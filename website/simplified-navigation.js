(function () {
  var playerInitiated = false;

  function activateView (name) {
    var next = document.querySelector('[data-view="' + name + '"]');
    if (next) {
      var current = document.querySelector('[data-view][data-active]');
      delete current.dataset.active;
      next.dataset.active = 'true';
      if (name == 'sp-show-interview' && !playerInitiated) {
        window.initPlayer(document.querySelector('audio'), document.querySelector('.audio-player'));
        playerInitiated = true;
      }
    }

    if (window.innerWidth <= 600) {
      window.scrollTo({ top: 0, left: 0 });
    }
  }

  window.addEventListener('load', function () {
    var links = document.querySelectorAll('[data-target-view]');

    for (var i = 0; i < links.length; i++) {
      links[i].addEventListener('click', function (e) {
        e.preventDefault();
        activateView(this.dataset.targetView);
      });
    }

    var initialView = this.document.querySelector('[data-view][data-active]');
    activateView(initialView.dataset.view);
  });
})();

