from pelican import signals
from bs4 import BeautifulSoup
from bs4.element import Tag
from copy import copy
import re

# find reference, by id:

# #fnref:1

# <sup id="fnref:1"><a class="footnote-ref" href="#fn:1">1</a></sup>

# <div class="footnote">
#   <hr>
#   <ol>
#     <li id="fn:1">
#       <p>
#         Guido Vanderhulst e.a., <em>3000 Brusselse Haarden, dat wordt niet op
#         één dag gebouwd</em>, Les dossiers de la Fonderie, n°2, oktober 1997,
#         2-talig&nbsp;
#         <a class="footnote-backref" href="#fnref:1" title="Jump back to footnote 1 in the text">↩</a>
#       </p>
#     </li>
#   </ol>
# </div>

# find footnote container
# loop through all the footnotes in it
# soup.select('.footnote ol li')

def insert_footnotes_in_content (contentObj):
  if contentObj._content:
    soup = BeautifulSoup(contentObj._content, "html.parser")
    num = 1

    for note in soup.select('.footnote ol li'):
      # per footnote find it's reference
      # print(note)
      backrefURL = note.select('.footnote-backref')[0]['href']
      backrefId = backrefURL[1:]
      backref = soup.find('sup', id=backrefId)
      backref['class'] = ['footnote-backref-sup']
      
      # change tagname of the li to section, add a className?
      newFootnote = soup.new_tag('section', attrs={
        'class': 'footnote inline-footnote',
        'id': note['id'],
        'data-num': num
      })

      # insert before the parent element of the reference
      backref.parent.insert_before(newFootnote)#.wrap(newFootnoteWrapper)
      # backref.insert_after(newFootnote)

      for child in note.contents:
        newFootnote.append(copy(child))

      num += 1 

    contentObj._content = str(soup)

def register():
  signals.content_object_init.connect(insert_footnotes_in_content)