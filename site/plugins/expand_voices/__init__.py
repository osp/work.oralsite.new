from pelican import signals
import re


def extract_name_initials (line):
  m = re.match(r'(.+)\s\((\w+)\)', line)

  if m:
    return (m.group(1), m.group(2))

  return None

def expand_voices (contentObj):
  voices = []
  if 'voices' in contentObj.metadata:
    if type(contentObj.metadata['voices']) is list:
      for line in contentObj.metadata['voices']:
        try:
          name, initials = extract_name_initials(line.strip())
          voices.append((name, initials))
          contentObj._content, _ = re.subn(r'(?<=<p>){}:'.format(initials), '<span class="label--voice">{}</span>'.format(name), contentObj._content, flags=re.IGNORECASE)
        except Exception as e:
          print(e)
          continue
    else:
      try:
        name, initials = extract_name_initials(contentObj.metadata['voices'].strip())
        voices = [(name, initials)]
        contentObj._content, _ = re.subn(r'(?<=<p>){}:'.format(initials), '<span class="label--voice">{}</span>'.format(name), contentObj._content, flags=re.IGNORECASE)
      except Exception as e:
        print(e)

    contentObj.metadata['voices'] = voices
    contentObj.voices = voices

def register():
  signals.content_object_init.connect(expand_voices)