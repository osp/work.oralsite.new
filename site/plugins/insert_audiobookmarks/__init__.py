from pelican import signals
import re

## -- section: name -- [02:40]
bookmark_patt = re.compile(r'<p>(-{2,})\s+section:\s+(?P<label>.+)\s+\1\s+\[(?P<timecode>[\d\:]+)\]</p>', re.I)
## ([name][02:40])
inline_bookmark_patt = re.compile(r'\(\[(?P<label>[^\]]+)\]\[(?P<timecode>[\d\:]+)\]\)')

def inSeconds(hours = 0, minutes = 0, seconds = 0):
  return max(0, seconds) + max(0, minutes * 60) + max(0, hours * 3600)

def makeTimelabel(hours=0, minutes=0, seconds=0):
  return ('{}:'.format(hours) if hours else '') + '{:0>2}:{:0>2}'.format(str(minutes), str(seconds))

def replace_bookmark (m):
  if m.group('label') and m.group('timecode'):
    label = m.group('label')
    timeparts = list(map(int, m.group('timecode').split(':')))
    timeparts.reverse()
    hours = timeparts[2] if len(timeparts) > 2 else 0
    minutes = timeparts[1] if len(timeparts) > 1 else 0
    seconds = timeparts[0] if len(timeparts) > 0 else 0
    time = inSeconds(hours, minutes, seconds)
    timelabel = makeTimelabel(hours, minutes, seconds)

    return '<section class="audiobookmark" data-time="{time}"><section class="audiobookmark--background-wrapper"><h4><span class="bookmark-label">{label}</span><span class="bookmark-timecode">{timelabel}</span></h4></section></section>'.format(time=time, timelabel=timelabel, label=label)
  else:
    return m.group(0)

def replace_inline_bookmark (m):
  if m.group('label') and m.group('timecode'):
    label = m.group('label')
    timeparts = list(map(int, m.group('timecode').split(':')))
    timeparts.reverse()
    hours = timeparts[2] if len(timeparts) > 2 else 0
    minutes = timeparts[1] if len(timeparts) > 1 else 0
    seconds = timeparts[0] if len(timeparts) > 0 else 0
    time = inSeconds(hours, minutes, seconds)
    timelabel = makeTimelabel(hours, minutes, seconds)

    return '<span class="audiobookmark inline" data-time="{time}"><span class="bookmark-label">{label}</span><span class="bookmark-timecode">{timelabel}</span></span>'.format(time=time, timelabel=timelabel, label=label)
  else:
    return m.group(0)

def insert_audiobookmarks (contentObj):
  if contentObj._content:
    contentObj._content, _ = bookmark_patt.subn(replace_bookmark, contentObj._content)

def insert_inline_bookmarks (contentObj):
  if contentObj._content:
    contentObj._content, _ = inline_bookmark_patt.subn(replace_inline_bookmark, contentObj._content)

def register():
  signals.content_object_init.connect(insert_audiobookmarks)
  signals.content_object_init.connect(insert_inline_bookmarks)