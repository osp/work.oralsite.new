from pelican import signals
import os.path
from mutagen import File
import math

# import re

def getFormattedLength (length):
  length = float(length)
  remaining = length
  hours = math.floor(remaining / 3600)
  remaining -= hours * 3600
  minutes = math.floor(remaining / 60)
  remaining -= minutes * 60
  seconds = round(remaining)
  return hours, minutes, seconds

def makeTimelabel (hours=0, minutes=0, seconds=0):
  return ('{}h '.format(hours) if hours else '') + '{:0>2}\' {:0>2}"'.format(str(minutes), str(seconds))

def get_audio_length (path):
  if os.path.exists(path):
    audioObj = File(path)
    if audioObj:
      return audioObj.info.length

  return None

def get_audio_mime_type (path):

  audioObj = File(path)
  if audioObj:
    return audioObj.mime[0]

  return None

def process (contentObj):
  if 'audio' in contentObj.metadata:
    audio = contentObj.metadata['audio']
    basepath = contentObj.settings['PATH']

    if not isinstance(audio, list):
      audio = [audio]

    audio = [(audiopath, get_audio_mime_type(os.path.join(basepath, audiopath))) for audiopath in audio]
    length = get_audio_length(os.path.join(basepath, audio[0][0]))

    contentObj.audio = audio
    contentObj.metadata['audio'] = audio

    if length is not None:
      hours, minutes, seconds = getFormattedLength(length)
      length = {
        'raw': int(length),
        'formatted': makeTimelabel(hours, minutes, seconds),
        'factored': (hours, minutes, seconds)
      }

      contentObj.metadata['audiolength'] = length
      contentObj.audiolength = length

def register():
  signals.content_object_init.connect(process)