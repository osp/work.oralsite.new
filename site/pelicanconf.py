#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = ''
SITENAME = 'Conversations in Vermont'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

DIRECT_TEMPLATES = []

THEME = 'themes/conversations-in-vermont'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PLUGIN_PATHS = ['plugins']
PLUGINS = [ 'get_audio_length', 'make_clusters', 'insert_footnotes_in_content', 'expand_voices', 'insert_audiobookmarks', 'elevate_img_class_to_figure' ]

STATIC_PATHS = ['images', 'audio']

ARTICLE_URL = '{chapter}/{slug}.html'
ARTICLE_SAVE_AS = '{chapter}/{slug}.html'

PAGE_URL = '{chapter}/{slug}.html'
PAGE_SAVE_AS = '{chapter}/{slug}.html'

FORMATTED_FIELDS = ['summary']

PATH_METADATA = r'(?P<chapter>[\w\d\-_]+)(?:/(?P<cluster>[\w\d\-_]+)/)?'

DEFAULT_DATE_FORMAT = '%d-%m-%Y'

import glob 
import os.path
glob_basepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), PATH)
def strip_basepath (p):
  return p[len(glob_basepath) + 1:]
chapter_files = list(map(strip_basepath, glob.glob(os.path.join(glob_basepath, '*/chapter.md'))))
credit_files = list(map(strip_basepath, glob.glob(os.path.join(glob_basepath, '*/credits.md'))))
cluster_files = list(map(strip_basepath, glob.glob(os.path.join(glob_basepath, '*/*/cluster.md'))))

PAGE_PATHS = ['pages', 'cluster.md', 'chapter.md']
PAGE_PATHS.extend(chapter_files)
PAGE_PATHS.extend(cluster_files)
PAGE_PATHS.extend(credit_files)

SUMMARY_MAX_LENGTH = 0

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'mdx_figcaption': {}
    },
    'output_format': 'html5'
}

STATIC_CHECK_IF_MODIFIED = True