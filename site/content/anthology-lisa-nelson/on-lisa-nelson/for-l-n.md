title: For L.N.
url: http://sarma.be/docs/3267
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1989-Svane_For_Lisa_Nelson%2b%2b.jpg
author: C. Svane
date: 1989
order: 2

Somewhere between the after-image and the present, between the humming of double-winged dragonflies and the diesel-belching roar of the earth-mover
