title: Dialogues on Blindness
url: http://sarma.be/docs/2978
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-dialogues-on-blindness%2b%2b.jpg
author: J. Peeters
date: 2005
order: 6

The word 'blindness' refers to a range of visual impairments that cause a severe to total reduction of the experience of sight. It is also charged as a metaphor, 'blindness' being a synonym of ignorance or not-knowing. Over the ages, both meanings got also mixed up, whether deliberately or out of convenience, blindness was romanticised and invested with misconceptions.
