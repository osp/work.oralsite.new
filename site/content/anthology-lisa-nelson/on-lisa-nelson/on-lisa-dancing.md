title: On Lisa Dancing
url: http://sarma.be/docs/3266
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1987-Svane-On-Lisa-Nelson-dancing%2b%2b.jpg
author: C. Svane
date: 1987
order: 1

First thing you notice is her eyes. She can spin out her gaze like line from a fly-fisher's reel, our focus the hook at its end.
