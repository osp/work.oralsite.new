title: Dance Between Performance and Image
url: http://sarma.be/docs/3286
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Coates-Dance_performance_Image%2b%2b.jpg
author: E. Carson Coates
date: 2014
order: 8

I'm lying on my back on a black floor, backstage and behind the scrim in the Brooklyn Academy of Music's Fishman Space, watching and listening to Bodycast: An Artist Lecture by Suzanne Bocanegra starring Frances McDormand.