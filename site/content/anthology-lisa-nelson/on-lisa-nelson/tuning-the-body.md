title: Tuning the Body
url: http://sarma.be/docs/3279
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Noe-Tuning-the-body%2b%2b.jpg
author: A. Noë  
date: 2006
order: 7

What do we see when we're looking at dance? What can art teach us about the mind? If art can help us understand the nature of the mind, then what does that fact tell us about art, or about some art? And what does it tell us about the mind?
