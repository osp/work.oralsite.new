title: Approche progressive d'une oeuvre
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-On-image_LNelson%2b%2b.jpg
author: P. Kuypers
date: 2001
order: 3

Somewhere between the after-image and the present, between the humming of double-winged dragonflies and the diesel-belching roar of the earth-mover
