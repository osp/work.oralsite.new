title: Danser, c'est regarder
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Nelson-excerpt-about-vision%2b%2b.jpg
author: E. Huynh
date: 2001
order: 4

Somewhere between the after-image and the present, between the humming of double-winged dragonflies and the diesel-belching roar of the earth-mover
