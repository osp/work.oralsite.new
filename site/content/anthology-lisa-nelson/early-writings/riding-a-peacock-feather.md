title: Riding a Peacock Feather
url: http://sarma.be/docs/3261
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1977-Peacock_Feather%2b%2b.jpg
author: L. Nelson 
date: 1977
order: 4

BASIC MATERIALS: Peacock feather (length can vary, 2 feet or more is good); as large an uncluttered, high ceilinged, undrafty space as you can find; yourself.
