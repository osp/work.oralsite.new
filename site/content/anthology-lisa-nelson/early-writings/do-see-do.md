title: DO-SEE-DO
url: http://sarma.be/docs/3259
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1977-Do_see_do%2b%2b.jpg
author: L. Nelson 
date: 1977
order: 6

The following Videologues are examples of an approach to the video medium based on the assumption that the experience of moving informs the way we see movement and conversely that learning to see movement in a new way influences our dancing.
