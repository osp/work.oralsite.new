title: Editor's Note
url: http://sarma.be/docs/3268
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1980-Nelson-StarkSmith-Cohen%2b%2b.jpg
author: L. Nelson - N. Stark Smith 
date: 1980
order: 10

On receiving these letters and other feedback (interestingly all from men) on David's articles, our first thought was to let David speak for himself in response.
