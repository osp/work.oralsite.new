title: A Short History
url: http://sarma.be/docs/3269
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1980-Stark_Smith-Round-Up%2b%2b.jpg
author: L. Nelson - N. Stark Smith 
date: 1980
order: 11

During a Grand Union residency at Oberlin College in January 1972, Steve Paxton made a work for 11 men in which they threw, caught, flung, collided and fell among one another continuously for 10 minutes.