title: Cleffed Pallette
url: http://sarma.be/docs/3291
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1979-Figurelli%2b%2b.jpg
author: L. Nelson - S. Figurelli
date: 1979
order: 8

In the interface between Italian and English we discovered some conceptual puzzles about the word, 'imaginario'.
