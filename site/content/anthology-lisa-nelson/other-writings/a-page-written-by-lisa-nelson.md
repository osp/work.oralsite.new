title: A Page Written By Lisa Nelson
url: http://sarma.be/docs/3295
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Nelson-A-page-Written-By%2b%2b.jpg
author: L. Nelson 
date: 2002
order: 13

I look at the senses as attentional tools.  Their physical organs are focussing instruments.  My body must move to tune each of them in to a desired channel.  I’m grateful that their simple instructions give me cause to move.
