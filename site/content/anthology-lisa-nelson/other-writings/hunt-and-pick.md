title: Hunt and Pick
url: http://sarma.be/docs/3278
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2006-Nelson_Hunt-%26-Pick%2b%2b.jpg
author: L. Nelson 
date: 2006
order: 15

For a time, magazine-making was an antidote to the 'now you see it, now you don't' of performing. Holding an object in my hands at the end of an intense collaborative effort gave a startling satisfaction.
