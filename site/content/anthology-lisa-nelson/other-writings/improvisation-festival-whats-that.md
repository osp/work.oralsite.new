title: Improvisation Festival, What's That?
url: http://sarma.be/docs/3275
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1995-Nelson-Improvisation_Festival%2b%2b.jpg
author: L. Nelson 
date: 1995
order: 12

Over the past year, improvisation festivals have cropped up on two North American coasts: The 2nd Annual Improvisation Festival/NY (New York City), Seattle Festival of Alternative Dance and Improvisation (WA), Improv Festival and Symposium (Vancouver, B.C.), Engaging the Imagination: A Festival of Improvisation (San Francisco, CA), and Taken by Surprise: Improvisation in Dance and Mind (Berkeley, CA).
