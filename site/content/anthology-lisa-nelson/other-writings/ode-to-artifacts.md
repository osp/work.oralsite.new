title: Ode To Artifacts
url: http://sarma.be/docs/3282
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2011-Nelson-Ode_artifacts%2b%2b.jpg
author: L. Nelson - J. Konjar
date: 2011
order: 16

Your process makes me wonder what turn my dancing would have taken if I’d had a video to study of the elderly musician who performed a five-minute dance in the Tibetan Folk Opera that visited a small town in southern Vermont in the early’70s.
