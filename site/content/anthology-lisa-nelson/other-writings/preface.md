url: http://sarma.be/docs/3283
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2013-Nelson_preface%2b%2b.jpg
author: L. Nelson
title: Preface
date: 2013
order: 17

This little book invites us to be a fly on the wall of an improvisational dancer's, by nature, self-invented research. We enter the dance studio during Susan Sgorbati's process of merging with a science called Emergence, which in itself, and perhaps by its own nature, is still emerging.
