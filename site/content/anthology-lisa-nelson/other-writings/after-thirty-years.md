title: After Thirty Years
url: http://sarma.be/docs/3277
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2005-Nelson-After-30-Years%2b%2b.jpg
author: L. Nelson 
date: 2005
order: 14

I strain to be concise. To not belabor the commonality of the struggle of keeping an indie dance publication alive with the struggle of improvisational dance to find a place in the dance market. After 30 years of both, I offer these thoughts
