title: [REPORT]
url: http://sarma.be/docs/3284
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2014-Nelson-report%2b%2b.jpg
author: L. Nelson
date: 2014
order: 18

The CQ chapbook of my imagination doesn't exist. It is, rather, another opportunity to not know. And to not know it within thirty-odd pages of bound paper. I like to think that, like the 'empty' space that invites a dance to materialize, the chapbook is already there, one need only make it visible.