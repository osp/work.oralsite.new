title: Before Your Eyes
url: http://sarma.be/docs/3247
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-before_your_eyes%2b.jpg
author: Lisa Nelson 
date: 2004
order: 2

I think of the eyes. Many moving parts. I think of seeing. There’s more to it than meets the eye. I think of vision and movement. One gives rise to the other. Dialogue comes to mind. That’s how I experience their wedding. And how I experience my dancing—within my body and in society with people, things, and space.