title: Fragment of a Tuning Run
url: http://sarma.be/docs/3285
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-materializing+.jpg
author: Lisa Nelson 
date: 2014
order: 4

Five people have organized themselves to stand in a casual curve, our backs a few feet from a stone wall. We face into a calm volume of space. The reach of our eyes is limited by tall walls of wood and glass, a floor of light satin wood, a peaked ceiling fancy with timbers.