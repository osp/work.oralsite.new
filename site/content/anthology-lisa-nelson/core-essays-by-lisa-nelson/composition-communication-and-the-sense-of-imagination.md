title: Composition, Communication, and the Sense of Imagination
url: http://sarma.be/docs/3280
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-LNelson-On-Gibson%2b%2b.jpg
author: Lisa Nelson 
date: 2006
order: 3

What do I teach? Laboratory: Tuning Scores-Composition, Communication, and the Sense of Imagination. This research focuses on the physical base of the imagination. As dance is the medium of my study, I will offer physical practices that put questions on the table.