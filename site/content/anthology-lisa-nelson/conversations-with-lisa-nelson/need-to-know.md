title: Need to Know
url: http://sarma.be/docs/3287
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Nelson-need-to-know%2b%2b.jpg
author: L. Nelson - M. Tompkins - D. Zambrano
date: 2000
L. Nelson - M. Tompkins - D. Zambrano
order: 8

A few years to back, I was surprised to notice a slew of improvised dance performances popping up in high visibility venues across Europe: in Holland, France, Germany, Switzerland, Austria, Portugal, Belgium. In my experience performing scored improvisation in Europe over the past 25 years, this apparent wave of mainstream popularity for improvised dance performance was a first.
