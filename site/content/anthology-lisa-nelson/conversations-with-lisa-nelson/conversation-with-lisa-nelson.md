title: Conversation with Lisa Nelson
url: http://sarma.be/docs/3276
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-wise-body%2b.jpg
author: L. Nelson - A. Benoit-Nader
date: 1997
L. Nelson - A. Benoit-Nader
order: 7

How did you come to improvise? I started dancing very young with a wonderful teacher in my neighborhood. When I was eleven I went to Juilliard1 to study ballet, Graham, music theory and composition. I studied with Pearl Lang, a Graham dancer and choreographer who told me that I should set the movements of my dances which shocked me because I felt that the dances I made were already set and repeatable.
