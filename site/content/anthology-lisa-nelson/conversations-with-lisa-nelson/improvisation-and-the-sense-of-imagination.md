title: Improvisation and the Sense of Imagination
url: http://sarma.be/docs/3272
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1992-Nelson_Lepkoff-improvisation-sense-of-imagination%2b%2b.jpg
author: L. Nelson - L. Kraus - D. Lepkoff  
date: 1992
order: 5

LISA KRAUS: My question comes from watching the commitment that never stopped no matter what the action at hand was. That action was completely full-bodied at every moment.
