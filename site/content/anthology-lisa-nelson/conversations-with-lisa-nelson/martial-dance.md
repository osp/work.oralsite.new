title: Martial Dance
url: http://sarma.be/docs/3273
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1993-Nelson-Krauss_Martial-Dance%2b%2b.jpg
author: L. Nelson - H. Krauss  
date: 1993
order: 6

This interview took place in August 1992, after a ten year period of development of a form I have called Martial Dance.
