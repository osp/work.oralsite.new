title: On Stillness
url: http://sarma.be/docs/3296
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2004-Nelson-On_Stillness%2b%2b.jpg
author: L. Nelson - D. Lepkoff - M. Van Imschoot
date: 2004
order: 9

In the fall of 1998, I embarked on a research project on postwar (WWII) improvisation in dance performance with a fellowship from the Belgian Fund for Scientific Research. For a subject that is as sparsely documented as improvisation, it was an obvious choice to turn to the interview as a way to retrieve information directly from practitioners in the field.
