title: The theatre, the music, the dance
url: http://sarma.be/docs/3270
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1985-Nelson_Vacca_Wolf%2b%2b.jpg
author: L. Nelson - T. Vacca - T. Wolf  
date: 1985
order: 3

ony Vacca and Tim Wolf share the distinction of being musicians who not only have collaborated closely with dancers and theater performers for many years, but who have themselves investigated physical performance and discipline-Tony as a gymnast, Tim as a contact improvisor and performer with an improvisational dance ensemble in Hartford, Connecticut.
