title: Whose Dance Are You Doing
url: http://sarma.be/docs/3294
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1982-Nelson_Paxton_Topf%2b%2b.jpg
author: L. Nelson - A. Hougée - S. Paxton - N. Topf
date: 2009
order: 11

LN / I'm here from the States to teach at the Theaterschool. I give voice training and I'm working with some compositional forms with the students.
