title: Making Sound
url: http://sarma.be/docs/3264
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1985-Making_Sound%2b.jpg
author: L. Nelson - D. Lepkoff - T. Pearson - A. Warshaw  
date: 1985
order: 2

Danny: A fundamental idea in the work with sound and movement was to just observe what movements the body had to make to produce the different sounds; to observe the different musculatures and attacks and sustaining postures rather than trying to dance and sing at the same time.
