title: Lisa Nelson in conversation with Nita Little
url: http://sarma.be/docs/3281
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Nelson-In-Conversation-Nita-Little%2b%2b.jpg
author: L. Nelson - N. Little
date: 2006
order: 10

We take the opportunity of Lisa Nelson's upcoming Tuning Scores workshop to publish a public interview with her conducted by Nita Little during the Side Step Festival of 2005 in Finland. Lisa's own remarks on Tuning Scores is also published. 
