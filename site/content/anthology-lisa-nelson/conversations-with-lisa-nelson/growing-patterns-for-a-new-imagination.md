title: Growing Patterns for a New Imagination
url: http://sarma.be/docs/3297
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-wise-body%2b.jpg
author: L. Nelson - J. Lansley
date: 2011
order: 12

Jacky - What is your earliest memory of dance? Earliest positive memory? Lisa - I think when I was about 3 I went to a dance class - which is so strange - to think that a dance class is probably my earliest memory of dance. The 'dance' was prancing around mimicking the story of the ugly duckling to a record. It's a vivid memory. I remember the dance studio in which it happened. I remember that it was something that was social because it was a group of people, so that value was part of what dancing was. I know I enjoyed it.