Title: Meeting Steve Paxton in the 1970s
Date: 20-11-2001
Location: 
Order: 9
Cluster: In relation to…
audio: audio/lisa-nelson/9-2001_11_20-Nelson-Lisa-On-relation-with-Steve-Paxton-stereo-one-second.ogg
       audio/lisa-nelson/9-2001_11_20-Nelson-Lisa-On-relation-with-Steve-Paxton-stereo-one-second.m4a
       audio/lisa-nelson/9-2001_11_20-Nelson-Lisa-On-relation-with-Steve-Paxton-stereo-one-second.mp3
Voices: Lisa Nelson (LN)
        Myriam van Imschoot (MVI)

It was during a two-hour long car drive along the dusty roads from Mad Brook Farm to Burlington, Vermont that I remember Lisa recalling an instance that she *could have first met* Steve Paxton. How we got to that topic is hard to say, but there was a certain appeal to speak about such virtual meetings before we would exchange good-byes and I would return to Europe. The parallel world of 'what ifs' is the forge of speculation and perhaps history too.
{: .introduction}

So, the occasion that 'Lisa could have first met Steve Paxton' was when friends flagged her down on the streets of New York to come see 'Contact Improvisations' at the John Weber Gallery in summer 1972, later coined as the birth of Contact Improvisation (the ideas moved outside the halo of the single event to become a worldwide practice for people who may now not even recall the name of the originator). As her friends waved her to come, Lisa stepped in her car to wind her way to a life elsewhere, following her appetite to spend time on the land, in the mountains of West Virginia for a while.
{: .introduction}

Lisa and Steve soon after met for real – as colleagues in the fall of 1972 at Bennington College where they slipped into each other's classes and work as part of a community of dancers, musicians and artists that lived on and off campus. One could easily write “and the rest is history”. However, it's only *in retrospect* that lifelong affinities reveal themselves in their fullest amplitude. All history is storytelling after the fact. In the moment itself, encounters, events, situations, etc. do not yet know their bearing and are far more prosaic. Like [PA  RT](https://vimeo.com/103767442). How casually and factually Lisa talks about the beginning of this duet of theirs that they performed from 1978 for more than two decades. When I saw *PA  RT* in the improvisation festival On the Edge, curated by Mark Tompkins in Paris in 1998, I could see why it had been mesmerizing audiences for all of that time. It's a hypnotic 'Spiel', a 'crime detective' before the crime has happened, a dance full of intrigue that happens in both virtual and actual worlds, and where non-meeting is as significant as meeting within ever-moving orbits and their occasioned alignments. But there was something else too that I hadn't felt that acutely before in performance: that the accumulation of histories is palpable, that we can indeed sense the prism of time and all its refractions. It's weird: one can touch sedimentation and it is not clogged or stuffed, but has resilience, like walking on bouncy sneakers.
{: .introduction}

What strikes me now, reading it again, is how this interview feels oddly floating in time. It was conducted two months after the Al Qaeda planes pierced the WTC towers and the bubble of the West burst.
{: .introduction}

LN: [Steve Paxton and I ] met at Bennington [College in Vermont] when Steve was teaching at Bennington, and I came in in the middle of the term to teach at Bennington. So, he had already started, and I ( …) It was the fall of ‘72. And that, well, we met there ( …)

MVI: As colleagues? 

LN: Yeah, we didn’t know each other, but I sat in on some of his classes, and he sat in on some of mine. I already told you that story about how he kicked me out of his class the first time I had asked him if I could come into his class [and] try it out.

MVI: Yeah.

LN: But it was the middle of the term. I mean, it wasn’t at the beginning, anyway. He invited me to leave after about a half an hour, and so, that was more or less one of our first exchanges. He said, 'If you can’t do what everybody else is doing, please leave.' Or, 'Leave'. I don’t remember whether he said 'please'.

MVI: What were they doing? 

LN: They were doing a warm-up. It seemed like they were doing kind of yoga postures and stretches, and his class at that time seemed to be a combination of yoga cleansing exercises, where we would snuffle salt water in the class and clean our sinuses, and some aikido walks if I remember. It was more or less a series of different kinds of exercises. It was a technique… it was called a 'technique class'. 

MVI: Yeah.

LN: And yeah, that’s what it was, and I can’t remember whether it was at that time or maybe the next term [that] he taught a standing class in the mornings, so it was a half hour, I think, of just standing, which I did go to as many of those as I could. And I was teaching technique, supposedly teaching 'technique', and I was working with beginning to try and understand teaching. I was making teaching scores where everybody taught each other things, so that was my idea about technique at that time.

MVI: What was very much on your mind in the teaching?

LN: That’s where it started because I didn’t really have a technique that I felt like I knew anything about. And so I had the freedom to do that at Bennington. And I think by the second term, my chronology is a little mixed up… in the spring of ‘73 … I can’t remember whether it was the spring of ‘73 or the spring of ‘74 because I taught technique at Bennington just for a year and a half, Steve invited me to come be a guest with the Grand Union at their 14th Street Gallery [performances in New York].

MVI: Yep.

LN: That was in [‘74]. And we played a lot. We didn’t have any particular projects. That’s my memory: we played a lot in the studios, and there was a lot of improvising going on, everywhere, all over the place. And we met on that territory and had a very playful connection. He was doing Contact at that time. I think, also, he started giving community classes in Contact during the spring of that year—spring of ‘73.

MVI: What does that mean - a community class?

LN: It wasn’t in the curriculum of the college. It was open to people from anywhere, and I was there. I was working with a group of people - Cathy Weis was one of them. Susan Sgorbati was another one of them. Yeah, I was working with a cellist, Michael Finkel, at that time. We were touring around the countryside doing cello and dance performances. The group of people included a bluegrass band, and we all came to Steve’s community class in Contact Improvisation. And I remember it was a very authoritative class—many exercises in working with weight and explorations—but Steve was very…

MVI: Yeah.

LN: …authoritative and didn’t want any playing around, and so…and they were probably short classes, I don’t know, maybe an hour and a half or a couple of hours at the most, and it was a big mix of people that weren’t dancers and some dancers … So, that was in ‘73, and -

MVI: But there was so much improvisation going on everywhere. Was there something that was particularly striking in this way of improvising or this way of …

LN: I’m trying to remember. Well, for me, because I had been there years before [and] had been working with musicians, and there wasn’t as much of that activity going on at that moment.

MVI: With musicians?

LN: Yeah, when I came back to Bennington to teach. I might be wrong. I’m trying to remember. Judith Dunn was there!

MVI: Yes.

LN: And her company was working, and that was a very particular way of working that she was doing with [black musician] Bill Dixon…

MVI: Yes.

LN: …and his musicians. Steve was doing the ([Contact Improvisation.][00:07:10]) I was working in a much more open field because I had been working with musicians for the years before in a very different way than Judith Dunn. ( …) And I was working much more into the collaboration. And then I had already stopped working with Daniel Nagrin, so I had a lot of that interactive…

MVI: Discourse?

LN: Well, yeah. I wouldn’t put names to the kinds of improvisation. It was just that people were making different kinds of work… There was no sense of comparison. Somebody wasn’t comparing Contact Improvisation to Judith Dunn’s work. They were coexisting. People were flowing in and out of all of them… Each of them. 

MVI: And when were you noticing… that’s why I ask it again, I repeat it here… the physiological base of that work, of Steve['s] as something that was…

LN: Oh, of Contact?

MVI: New in…

LN: It was completely new to me. I mean, in all of the work. It had a physical focus. The focus of the improvisation was physical. It wasn’t compositional. It wasn’t interactive in the sense of a human interaction. It was really the first time I even conceived of - I’ve already talked about this in another context - creating images to work from. Like, for me, the way I dealt with my physical score, my inner score, often was in limiting the body, and like, saying…“my hand is on my knee, and for the next half hour, I’m not gonna take it off my knee”. Or imagining a force moving towards me that I would be moving … My images I made for myself were very physical, but they didn’t have a physical score. Like I wasn’t *following* something. 

MVI: Like, you were following an internal or…

LN: Weight. Like I wasn’t following a physical sensation where it might organically go. I had no concept of 'following'. And that was what was most completely different about Contact Improvisation… I mean, it was a completely different way of looking at provoking movement in the body. And because I was familiar with a lot of the dancers who were working with him, I could see how it changed their action. It wasn’t as if the psychological or interactive material went away, but it was in the background rather than in the middle even…, that it wasn’t necessarily foreground. It’s just sort of where you’re working from, and I saw more difference in the way people moved when they were doing Contact Improvisation, these dancers, than I did watching them do anything else … Yeah, I was very busy doing my own work at that time [muffled conversation] making large theater pieces. In the spring of ‘73, I was collaborating with Cathy Weis and Susan Sgorbati making site-specific work in a large, old building…

MVI: Christie Svane refers to that in her article ['On Lisa Nelson Dancing. Profile of an improviser, Contact Quarterly Vol. 12 No 2 (Spring/Summer): 30], and that’s something you never mention.

LN: Oh, oh, yeah! 

MVI: Yeah! Can you say…

LN: Yeah, well, it was almost like an orchestration, inviting people who had certain skills and ways of working into a space and inviting them to make something specific for this space, and then creating linking features—interwoven features—of each person’s work, and creating a score for the audience to move through, so…. 

MVI: Interesting.

LN: And I remember, we made ([masks for the audience to wear.][00:12:44]) Different rooms had different masks. They were mouth masks, and they just had an expression of the mouth, and so in one room, for example, all of the masks were smiling. In another room, all the masks were shocked. [laughter] I mean, all the mouths were shocked.

MVI: You’re orchestrating audience response!

LN: Yeah! Right. And then the audience, of course, would go into these rooms. If they looked around, because you couldn’t not [look around], the rooms weren’t that big…They were as interested to look at each other with these partial masks, and so…. 

MVI: Sounds great.

LN: It was fabulous. I still have some of the…; they were *beautiful* masks we made out of paper mache with felt backs, and they were comfortable to put on and take off. And we also had made a jigsaw puzzle on glass…on mirrors! On pieces of mirror that were about one foot by one foot or rectangles of mirror on which somebody had made a painting.

MVI: On the mirror?

LN: On the mirror. Made a painting on the mirror, but, of course, with the mirror showing through it… And the audience assembled the jigsaw puzzle. It was huge. It was like a banquet table-size jigsaw puzzle, and so, people would take a piece and create and make and try to put it together to try and make the painting.

MVI: Oh, like, on the floor or - 

LN: Right. It might have been on a big table. I can’t remember, because it was in the dining room of this house, whether we left the table in or we made it on the floor, but it was the size of a very large banquet table. 

MVI: It seems like a lot of audience.

LN: It was all audience interactive, and we went in groups. There was like a 'butler' who would take different groups in a different order through all the events in the building, and then there would be repetitive random events that they might pass in the hallways as they travelled from place to place, so it was like a large orchestration of many different [actions].

MVI: The jigsaw puzzle might have been like in the same performance as the one with the masks?

LN:. ( …) Well, just some little pieces of one piece that was called *The Green Dream*.… When the last piece of the puzzle was put in, this music would play, it was like a big celebration. I think that was the end of your travel…whichever audience group. At the end, they had put the last piece of the puzzle in, and there would be fantastic music.

MVI: Sounds great.

LN: And there would be celebration [laughs]. So …

MVI: Why didn’t you ever talk about it yourself to me like because we never really touched upon that part. 

LN: Well, I didn’t continue. And, also, it wasn’t the last piece like that. But also, it was a long time ago, and I didn’t continue working that way, so I don’t think of it. It’s so long ago. But my interest in places persists…I mean, about building spaces. Yeah, I’d done another one the year before I left Bennington [as a student in '71]. I had done another one very similar …working with moving the audience through different parts of…it was in a completely different building… And used lots of ways of offering them things to look through. There would be things on their seats: frames they could look through like *pinse nez*…. or monocles, but they were objects that…they were nice objects, and you could use them to watch with. That was before doing any video, so the interest in looking at things had been already an appetite for that …

MVI: When did you take up dancing as part of yourself…?

LN: When did it come back? 

MVI: When did it come back?

LN: I remember ([being at Dartington][00:18:09]) [College in Totnes, UK] with Steve, when he was teaching there, and he asked me to teach some classes. And he was doing some performances. I guess it was ‘75 - maybe winter of ‘75. … I was working with video then, doing more video workshop type things, and I can’t remember what year it was, but it was in England that Steve was trying to do some Contact Improvisation performances in various places. Well, it happened very gradually that I was getting back into dancing. I was making performance pieces with video. … I made my first moving television piece in ‘75 to be part of a piece, a much larger piece, that Christie [Svane] was …

MVI: …engineering.

LN: Yeah, engineering… I wish I could remember the name of that. It’s a great piece. And I was working much more the support systems: doing lights and making music and making video components, and I was doing a lot of documenting of dance as well. And that’s when I…in ‘75, I went on a tour, a West Coast tour, with [the Contact group] Reunion. ( …) I was not dancing at all at that time, so that was ‘75… It might have been the winter of ‘75, and I *know* I wasn’t dancing. I was really having a very hard period of *missing* dancing, so I was kind of *yearning* to start again, but I wasn’t ready. But maybe it was the next year. I started doing some …it might have been in England when Steve needed another body for a performance, and I just agreed to try ‘cause contact really wasn’t my *thing*. But…I was working in the realm of invitations at that point. Anyway, invitations were drawing me back into dancing is what was happening, and various things arose. ( …) Oh! So, that was ‘77? Yeah, that was ‘77, then. By then, we were doing duet performances together - Steve and I. 

MVI: But that wasn’t like the first coming back to dance…?

LN: It was pretty close.

MVI: It was pretty close.

LN: I had been doing some pickup contact performances. I went on a tour with Reunion …in ‘77, as a guest. And Steve and I had started to do these duet performances together, which were a mix of solo dancing and contact improvisations, and often I would make sound tapes and sing. ( …) It was improvised from beginning to end. We had no format at that point. We just were doing an hour or so show and we would go back and forth between solos and doing duets. It wasn’t…as far as I remember, it wasn’t scored in any way. 

MVI: Yeah, it wasn’t so clearly like - [phone still ringing] Sorry, I just have to [picks up the phone] …hello? ( …)

[After the phone call]

MVI: Do you call it a format? Something you did for awhile?

LN: I guess, [in] ‘77 [and '78], we did a whole bunch. ( …)

MVI: For the rest of that work, did you start from similar premises? You’ve been working on scores…?

LN: If I remember, we didn’t talk about anything.

MVI: Anything!? Wow. You’re talkers! I could imagine -

LN: No, but we’re doers.

MVI: You’re doers.

LN: ( …) You perform together, and you start to find material that’s coming from your interaction. And then you start to have a dialogue about [it]…it’s a different way. Because it was possible to perform, we performed that area of the… There were never any rehearsals for anything, but we were ([both improvisational performers,][00:23:34]) so it wasn’t even something that had to be discussed. We were familiar with each other’s backgrounds. The material that came into the space when we were working together didn’t include everything we knew. It just included the finding out where we were meeting. ( …) Because it got more defined what I was interested in. And, for Steve and I, we came to the format of *PA RT*. And at that time, Steve was going to do a solo to the second half of *Private Parts* [opera by Robert Ashley], but someone asked us to do a duet. We said, “Oh, well, what should we do?” And I remember saying, “Well, why don’t I do a solo to the man’s half”, and then, we saw that we could do some duets and some solos, so it was a kind of a process of elimination of what was extra. And that could have been, just as I said, *PA RT* could have been a one-off. It could have just happened that one time. ( …) Of those first concerts of [improvisational works]…where doing concerts of that kind was…it wasn’t a big question. It's just, you get an invitation. “Oh, what should we do? Okay. Let’s do this.” And it was really learning about…it was sort of rehearsing in public. And I can’t really [explain]…if I think about it, why the climate allowed that, but they weren’t big, fancy venues, you know. It was really in smaller, sort of subvenues. I don’t think the first time we did it at Dartington, we called it *PA RT*, but I think a few weeks later, when we did it at this festival in London, we just named it. Yeah, I can’t remember, but it came very soon. It’s like, what do you have in your pocket at the time when you’re invited to perform, and…

MVI: **Oh! You have each other in your pockets.**

LN: Yeah, and we
([had Ashley's music in our pockets,][00:25:44]) and it was interesting enough to do it again, and see what it was. And it really was from the audience’s feedback that kept inviting it. So then the next time we went on a tour, I guess to Italy, Steve was invited, of course. He was always being invited places, so he had asked me if I would want to do that tour with him and that’s what we would do. So, it kind of just followed its nose into perpetuity.

MVI: **What would be like the earliest recording of *PA RT*? Just to see how it - where it developed from?**

LN: '79 at Western Front [in Vancouver].

**M Would you have any other concerts together? [What about] costume decisions, and the glasses…?**

LN: No, that was directly from the Ashley—about the man and the woman— and the clothes I wore were my clothes. It wasn’t like we went out and got costumes. Steve wore his clothes, and I…these were my clothes, and they seemed like they looked like a little man. It was sort of hermaphroditic. So it was what was right there at that time. And in the other performances, we basically just wore work clothes, and the only really things brought in, if I remember, were some of the music tapes that would go on and off occasionally.

MVI: **Did you bring them in? Because you seem to be more like the music person?**

LN: I brought them in. Well, that’s what I was very involved with, working with music—mostly world music at that time. ( …)

MVI: **What is funny, I mean, in terms to see the two of you together, is that in interviews, if I don’t really direct it, Steve would hardly really mention things about composition or [he would not be] like stressing the compositional mind. Even the word 'composition' is hardly ever used. While if I don’t push you any direction, it [comes] as a natural part of dance making in your ideas … So I wonder how that goes together [when] the two of you perform together. Seems like there’s different interests.** 

LN: Yeah, but also, I mean, Steve, he composes in space, so it’s… And he’s made incredible visual pieces. Like, you see **Ave nue**, and he’s used film way long ago, juxtaposition of imagery…but I think that is…

MVI: **But that’s his pieces, like, then he will talk about [it], indeed, he will use the word compositional.** 

LN: Right, so I think there’s two tracks for him.

MVI: **And in his talks on improvisation, he hardly ever…**

LN: Well, he’s working with ([a whole other microcosm][00:29:10]) of consciousness and unconsciousness. That’s what I hear mostly when Steve talks. He’s really looking at the mind. He’s exploring the mind of movement in the sense of consciousness and unconsciousness… Except over the last ten years, where he’s been working with these micro-relationships with the spine, so it’s very anatomical. And that’s, I think, that’s where he *throbs* with curiosity and research, and I… I don’t…I mean, that’s a subject that doesn’t … Yeah, I’m always amazed at how much juice there is in there. Because it’s very abstract for me: that. 

MVI: **That mind thing?**

LN: Yeah, consciousness-unconsciousness, and so where my material comes from is really much more immediate, [a] response to the immediate physical circumstance, and the movement of the senses. And I think maybe because 'weight' was Steve’s first metaphor in the body, and the skeleton, the bones, it has a certain logic to it… Well, the bones are very structural, but for me …this …

MVI: **That’s a physical environment.**

LN: Yeah, my most…the highest sensory feedback I get is from my eyes and from my guts—my organs—so it’s very interesting that maybe I create… The world is like an exoskeleton to me. It’s as if I don’t have an inner skeleton. Like, for me, what supports my sensorial world is what’s outside of my body. It’s what shapes me. It’s where I get my instructions. My body is like a *mirror* to the external world, and so, I feel like, yeah, well this is…I’ve never thought of it this way that, yeah, space is like an exoskeleton, which is like…. Exoskeleton is where the skeleton of the organism is on the outside of the body—like a turtle. Or certain insects. And then, and we have an endo…it’s not an endo …endoskeleton? But mammals have an internal skeleton, and … Oh, that’s interesting. 

MVI: **Say that again. Now that you have - **

**L: Yeah, explain what the exoskeleton is like …**

MVI: **Yeah.**

**L: **And Steve was looking at the skeletal structure, of course, as the inside of the body, and the forces that act on that on that - on the structure, internal structure of the body - 

MVI: **And the mind thing, so it’s like -**

LN: And well, the mind is around that. …And so I don’t relate to my inner skeleton. I relate to…it’s as if what gives me structure is what’s outside of me—the floor, the walls, that support my human form, and the eyes, of course, as this major balancing function, very much create a kind of exoskeleton to the body. It’s an amazing image.

MVI: **That’s an amazing image!**

LN: I’ve never thought of it before. I have to think about it some more. 

MVI: **I’ll transcribe it for you. You can - **

LN: Yeah, because - just to look at - It’s simplifying, in a way, two people’s paradigms. And then I could start to think of others like that, but it’s a different way we relate and read space. And with this outside relationship, outside-inside relationship, for me, the anatomy is more…the anatomy of *that* relationship; and Steve can look deeply into the micro-anatomy of the physical structure, ya know, the body, and I’m kind of on the outside. It’s sort of opposite viewpoints. And maybe that’s what keeps our dialogue rich. And, yeah, because it’s not hard to invert. I mean, I can understand to a certain degree what he means when he says and does, and he can read and, of course, also because you just invert your paradigm. It is sort of two sides of the same coin. 

MVI: **Yeah. When did you…**

LN: Wow.

MVI: **Yeah. Wow!** 

LN: Thanks, Myriam. 
