Title: Contact Improvisation. Flow, Tone, Cut,
Date: 02-05-2001
Location: 
Order: 8
Cluster: In relation to…
audio: audio/lisa-nelson/8-Relation-to-contact-impro-stereo-with-a-second.ogg
       audio/lisa-nelson/8-Relation-to-contact-impro-stereo-with-a-second.m4a
       audio/lisa-nelson/8-Relation-to-contact-impro-stereo-with-a-second.mp3
Voices: Lisa Nelson (LN)
        Myriam van Imschoot (MVI)

A powerful idea and an exhilarating practice, the dance form Contact Improvisation went soon after its inception by Steve Paxton in 1972 'viral', to use a term from the internet-technology that was invented in that same year. An expanding community of dancers started to make conjunctions with other ideas and fields too, espousing forms as varied as theater, sports, healing, martial arts, etc.
{: .introduction}

In the interview I probe Lisa to speak about her relation to contact. Lisa is not a contact dancer but as an editor of Contact Quarterly (together with Nancy Stark Smith since 1975) she has been engaged with the scene. I suggest to her that her work is like a 'negative space' in which principles key to contact were operating with significant difference and reversal. But now I feel sorry to have been crushing a singular artist under the thumb-rules of comparison. Yet wonderful thoughts arise here: about flow, cut and muscle tone. [Jump Cut](https://vimeo.com/177118938)  (1979), the referred to video work, is a must see. Time will give *Jump Cut* deserved place in the history of great dance videos. A study of flow with a cutting edge.
{: .introduction}

LN: I guess I started doing a little Contact in 1975. Steve [Paxton] would need a partner and I would do a little Contact with him and with whomever else was there. I would go to jams as I mentioned before. And I would use the studio space as open space to just dance and see what was happening in my body. Then Steve and I started doing these solo and Contact performances together. So I was getting back into my dancing. Those were the times when I was still very aware of the movement patterning, of what was still lingering of my old movement patterns, and I was getting into noticing the “reversing” pattern. In 1977, I started working with Bonnie [Bainbridge Cohen]. It was like coming back slowly, just dancing by myself, doing contact with various people. Christie [Svane] and I had been continuing to work together. Actually I would have to look at the chronology to be precise. But at the big Contact conference in 1977, the Windham College [in Putney, Vermont], I was pretty much just doing video. I was in all of the Contact concerts because everybody was. It was very eclectic and open, and there wasn’t any skill hierarchy. People were really still exploring the interactions. I was a little too wild; I remember Steve being not very happy. He thought I was too dangerous. I took that very seriously that this wasn't the context in which I could do what I was interested in with Contact. It was not the right thing for what Steve wanted to be showing. It was a peripheral activity to do Contact performances, but it was a place where I could practice dancing. 

MVI: When you said “reversing”, it's kind of erasing, isn't it?

LN: Yes. As if it didn't happen!

MVI: It's funny because performance is a realm where you can't erase.

LN: Well, that’s the whole point. Everything is so visible. I was so into the fact that everything was visible – having been doing video for some years then, it's so obvious that everything is visible. And I was interested in a lot of the ([invisible activities][00:04:10]). The movements seemed less interesting without the connective tissue of what happens between them. And it was the same for being interested in that moment before an action. What happens between one movement and the next, which gives it taste, flavor, personality, when you see the human being inside the forms. That was what was so beautiful about early Contact, in ’72, ’73: the explorations of it. Everything was so in front of you: the human being and how they survived the circumstances. That demanded great reflexes. And the personalities of human beings: you really saw what a human being was on a very raw level while doing that activity. What I was valuing out of those things was pretty clear. And so seeing people who did Contact with their universe of their life experience and all their movement patterns was somehow more interesting than just the purity of being able to do it with the right intention and have the right body tone … It wasn't as interesting without the flickers of personality and history embodied in the person. 

MVI: When did Contact become something that was also threatening to your own work? Threatening is too strong a word, but in the sense that it took over so much. We talked about this so often, that for too many people there is an equation between Contact improvisation and improvisation. People would hardly see improvisation beyond it.

LN: Pretty much from the start. I remember when Steve and I were doing PA RT, the critics were waiting for Contact Improvisation and since we did not do it, they criticized it. They did not see what they expected to see and they saw nothing. That was the first inkling. They were not going to like it anyway – the critics weren't going to like contact improvisation even – but they knew it was supposed to be rolling and flying and jumping and leaping and that sort of thing. The population of dancers who were getting fired up about it, they also seemed to like to see the other work. But … Because it was Steve [moving], they didn't know it was something else. That was right at the start. In my history, my interaction with it was very short. I did not have any conflict about being in or out of it. It didn't need to be for me. There was other work to do. But in terms of my own association, i.e. my name's association, it got cemented in there, and then doubly cemented by being the editor of CQ [Contact Quarterly]. I was very sensitized to that dominance of there being only one kind of improvisation and that was called “Contact.” Anything I did was associated with it, as if it's one thing. Even my video performance work, it was just ridiculous, which was not improvised at all. Maybe the other performance work was improvisational but it was not Contact. But even the video work got called Contact. In many ways it did not matter, because the people saw the work and they liked it. I was invited to do that; I wasn't invited to do Contact, because they knew it wasn't Contact.

MVI: Did this encourage you to stress more and more the difference?

LN: Only in the classes. If I was teaching a Contact class or workshop, sometimes contingent with the other work, I was very clear what the differences were. If I was interviewed, I could make it clear. But I didn't have to increase the differences. It was clear what it was. ( …) [A] marvelous thing about Contact is that you can make ([analogies with life activities:][00:10:26]) interpersonal support (support is so basic to Contact, the shifting of support: metaphorically that is so huge), and working with weight (what it is to give weight, to support weight, to take weight). The language had so much space in it for metaphor, and experientially it had so many spaces for metaphor. If you talk to someone who had been studying Contact, then anything else they were doing, they would apply it analogically to their study of Contact. Sometimes that was very useful and sometimes … It was fine, they could do whatever they wanted. But in terms of a group looking at some other aspect of interaction in dance, it was hard to pull those people in another focus. Because the stress is a lot on the organic in terms of the Contact, the weight flows, and the physics of it… the experiential nature, that very organic nature of physics. And so it was very applicable. It always seems to me that it depends on what your history is [that gives you] your dominant template. You apply your new experiences to your old ones, and sometimes a new one becomes a template that lasts for a long time. For me the Contact template was applied to what came before and it didn't become a bigger template. I still would apply it to what I learned about verticality and reflexes. Reflexes are happening all day long and not only when you are in a state of emergency. Bonnie [Bainbridge Cohen]’s work on reflexes as the underlying pattern of all movement, everything, was just the most extremely fantastic articulation, and something observable in human movement. It is brilliant because it is so accessible to observe.

MVI: I was always tempted to see in your work a 'negative image' ['negative' like in 'negative' space] of many Contact principles or even keywords. For example this huge notion of flow. If Contact was using that as a crucial keyword, then the negative image would be “cut”. If there was “going with the flow”, then there would be “resistance'” If there would be peripheral vision, then there would be other ways of using vision.

LN: No, it was not in any way a reaction to the language.

MVI: I know, but a negative image doesn't imply that it emerged by reaction, but it is interesting to see those two paradigms next to each other. Even in such a close vicinity if you just think about the people socially. I was tempted to see and to like that tension even.

LN: They surely are not the same. They are totally not the same. 

MVI: What marks the style and makes it prototypical of that style, because for every of those keywords I would find another organizing …

LN: In the early days of Contact, all the dynamics were there. It didn't go into any direction. This is just a funny thing, but I remember when the idea of flow came into Contact. 

MVI: Oh, if you could historicize that!

LN: It was with Curt Sidall (in the Reunion group), who was a football player who had a lot of mass. He was a big guy, and he had a wonderful weightiness and an incredible flow in his movement. Dancing with him was this ongoing ([feeling of flow][00:16:28]). When I saw him at the gathering, a few years ago, with the Contact performing 25th anniversary, he said to me: “I remember that dancing with you was the first time that I felt flow and the flow machine.” He identified it with dancing with me, which was very funny to me because, shit, I don’t think that was so. But anyway, we met easily in the dance. The thing about flow is that it is a basic physical truth. The dynamics within flow are what always appeals to me in dancing. I am interested in dynamics. My own dancing always had tons of firings, and dynamics. It is a mystery how certain aspects of Contact got valued more than others amongst this core group. And that is why we made [the video documentary] “Fall after Newton”, because we wanted to see each person’s strand. What got developed in Nancy [Stark Smith]’s strand, and in Curt’s, Nita [Little]’s, people who had been doing it from the beginning. There were the keywords coming into usage. But it was more about developing the experience in the body through practice. You would recognize in your own moving when flow would stop. Momentum has a flow, but to me there is also tonic flow, which is the tone of the muscles. Coming from martial arts and from a release technique that Steve brought into it, it had a anti-muscular approach: to let the muscles soften, to let the bones do the moving, to let the weight flow down the bones. This is the technique of it, and it would build for you a wiring to be able to access that technique. But when you are dancing or improvising, and not just doing the basic form of it, then safety was the concern. It always seemed to be Steve’s concern. How to do everything without hurting yourself. How to be in this highly adrenalized state and be able to navigate through reflexes without clenching. Hands were discouraged because they caused accidents in the beginning, but after a while they were supposed to be allowed to come back in because you would just know not to grab. You know how to grab but also to ungrab. You can feel where the movement is going, and you could really be grabbing, but you could modulate out of the grab. There were gradations of learning the technique. There were things you had to erase from your movement vocabulary to learn the technique. And how you brought the technique in action would be invented by the two people doing it. Since Steve would say to me “you did Contact great” and I would say “oh no, I could never do it, I could never do any of that stuff”, because I didn't want to cut out that stuff [that] I had no interest in removing at all. Tonic shifts (tension and muscles) was part of what dancing was to me. Without it, I was just a bunch of bones moving. I had nothing, I had no source, no motivation to move. I could do it for a minute but after that I would go off. I found that I got bored in the Contact duets. 

At first it was restful, because it had no creativity to it, it was just following. Every movement was caused by your partner, so it was a great game of how to compose yourself, to be the consequences of someone’s movement. This was delightful. But that was just a little piece to use. That also fed directly in relationship: the automaticity with which people would get into rolling moves, and lifting moves. They were practicing those moves, admittedly, but I did not like to go up in the air. I’d find ways of navigating out of that, not going up. But that was not “not following” the movement, but rather redirecting its direction in space. Support is crucial in my dancing. I use the ([environment as support,][00:23:21]) so that analogy can be made, from using somebody else as support to using the space as support. This is what I took from the analysis of Bonnie's about the distal support, which is when you stabilize the ends of your body and you move from that stability, rather than stabilizing the center of the body and moving the limbs, which is what ballet does: you are always stable on the vertical line and you move the limbs around it, and that is “proximal” support. Mine is almost all distal support when you imagine a wall and you move away from that support in the space. Contact was a place to practice distal support all the time because you were distally supporting constantly, in all relationships to gravity. I think you see that in my dancing more than anything else, the use of distal support without a partner, using the space. I anchor all the time parts of my body in space and then move around it and that makes unusual movement results. I see it now with a lot of people. It seems to have come in into the general style. But that was what I really loved and it made me do movements I could not conceive of.

MVI: Could the difference be that in Contact the point of support is a moving point (the support isn't there for the stability of it, not necessarily; it would be always be part of the whole center moving), but [your] exercise where we have to give resistance (two people are giving resistance it creates stability, and you can arrive in a composition that is still, even though there is so much force going on). It's interesting to see that that exercise which deals with support, balance, counterpoint … causes a composition of stillness within this force. While Contact does not have that aim anyway.

LN: Yes, the play of [Contact] is is to keep things moving. 

MVI: Again, I would see that there is a mutual interest, but that it is taken into different reactions. I don't want to put it in a reactive sense, but it's a way to analyze. With the flow too. I am surprised that flow is so important to you.

LN: It is, experientially. When the motor is running, it is a flow. It is like when the motor stops, it stops. It is not so much that the outer body is making visible flow but that the mind is moving and the attention is moving. When that gets interrupted it has a certain result that often isn't … There is a value put on being … I could call it “being on a roll”. There is a continuity of attention. And loss of attention would be stoppage of flow. Even when my body is not in motion, what’s moving is the attention and the readiness to move. 

MVI: Then we are speaking about attentional flow, while the body in that attention can be actually very bound.

LN: Absolutely. 

MVI: It can go from a bound position or movement to another one, to another one.

LN: Yes. 

MVI: Your body is very bound in many of the movements.

LN: I don’t think of it as “bound”, but it's just the use of the words of course. 

MVI: I hesitate to call it bound, because sometimes bound leads to an aesthetics where everything is colored in and filled into it, while there is so much in your bound shape that approximates it, and… it is a bit imprecise. You can't call these little things bound movements.

LN: Bound usually describes tone as tense. This is moving in bound flow (demonstrates it). This would not be bound flow (demonstrates). I am using tone in the muscles but it is not bound. In Laban it is slightly different. I would call it high tone. Because bound is stiff. 

MVI: Let’s call it then (['high toned',][00:30:50]) Probably it is a question of degree. Bound would be the extreme, while the complete relaxed and fluid would be the other extreme. And the toning is a modulation, from high to mid to low.

LN: Yes, low tone would be kind of flaccid, with not so much energy going through the muscles. In a lot of ways, when you leave the muscles behind, this is a big problem. There is a wonderful article in the [Contact] Quarterly coming from Bonnie [Bainbridge Cohen]’s work, about compression and muscle. I was just so delighted, cause it is my same complaint all the time. These released bodies just don’t have any “organs” in them. So, trying to energize and enervate the muscles, and when you then move that composition, through lock-in, resistance and force … Intentionally, you could let it flow; you can find a balance point where you are completely asymmetric in the muscles but not tense and you tune in where you are not tense, but you are still putting all your force into it, and then when that moves, it whoosshh. That's the next … When that moves, you modulate the tone. You might modulate it to be able to carry more of my weight. If you pushed into me and instead of going into low tone, which would make you fall on me, I could start to modulate. If you keep pressing with the exact same [force] … you'd start to move through space in another tonic, instead of automatically going into this fluid flow tone. The whole body would come with it. It is more about looking at modulation of tone, to get into the muscles. 

MVI: I am happy that I read this article, cause there is so much of this fluid. It is associated with so many things. Many of which exist in the Contact realm too.

LN: Well, that’s the whole point. 

MVI: So, there I saw a counter-image, or negative image, of someone using high tone, and working with that, instead of not addressing or not integrating it.

L: Yes, it is a continuum of language and concepts that is very closely aligned. 

MVI: So, there is shared interest, otherwise you wouldn't also be doing the magazine.

LN: There are totally shared interests in the development of that language. 

MVI: Like the cutting is such an interesting tool which not that many contacters use.
 
LN: Yeah, they don't “cut”. It has nothing to do with it. 

MVI: Two of the video pieces that you gave me are an analysis of flow.
 
LN: Absolutely, that is exactly what it was. And the “ear” ones: it's ([editing by ear][00:35:17]) instead of by image. We also were looking for cuts: how to create flow out of discontinuities. That’s the premise of making movies, so it is like the very central question of that medium. When I was learning how to edit: I was doing straight edits for all kinds of things (eg: making documentaries for doctors) and I didn't know any of the rules of editing. I had so many jump cuts. Unbelievable! Jump cuts everywhere! A jump cut is what you call it when you make a cut that is not good. It is the description of a bad cut, where the continuity is fucked up. That’s why you cannot do talking heads…you can't do a flow of talking head, because if the guy says something stupid and you want to cut it out, you have to insert something or – that’s why you use two cameras – you can dissolve to the other camera even when it is so many seconds later. I have seen some fabulous solutions to jump cuts in films and on television. Sometimes they insert black; they dissolve to black and the sound keeps going, but when you come back into the image, it doesn't look like a jump cut.
 
MVI: So there are many techniques to disguise and suture the cut. 

LN: Exactly. 

Myriam: Why? Because people don't like to be confronted with the cut? People want to be in the flow of the visual? In experimental film, filmmakers would stop suturing the cuts. 

LN: Oh, absolutely, that’s one of the places of creativity in the medium. For me the cuts were all I could use, because I did not have any fancy machines. I just had a single camera. How to create continuity then? And it's not just talking heads. The dominance of looking at a frame is that people tend to look at the right hand corner. If you have someone in red going out the right edge of the frame, you have a lot of decisions to make about what your next cut will be. You know the eye is there. Will you have a close-up of something emerging from where their eye already is? Or will you fuck them up and know that they won't be able to see when it's cut and the important things coming from the right. You learn that very quickly by doing it wrong. I used to say 'you learn why the conventions evolve only by doing it “wrong”'. You see why they are meaningful and then you decide what you want to play with. Those studies came out of what the medium or the tool could offer. For example: when I am looking at dancing, how could I imagine flow by constructing cuts, create the illusion of flow through cuts, not through seamless dissolves, through the cut as an insult to the continuity. In dancing, you could do stuff with lights. That’s another way you can do cuts on stage: you use light to keep shifting the attention. You do cuts, you do dissolves, you shift the stage space around through light. That's your framing tool. 


MVI: The interesting thing with [your video] “Jump Cut” is that you see both the attempt of …

LN: It was another thing that video offered, namely to make myself do things that I couldn't do. To use myself like a plastic. A wish-fulfillment. How could I float? I would like to float above a space. And so I could make that happen. I could use myself as the image. The magic of video was a tasty thing. How can I make myself experience floating? would be the next one. In this one it was floating suspended in the air. So I was working with a tool of cutting, and I was trying to extract that millisecond, or less … When Baryshnikov was jumping it would be much easier; he would stay up there for much longer. Particularly since I was timing it by eye and finger in the [editing] technology, where machines could back up 2 seconds and had seconds-counters. I couldn't do it by the counter, but by guessing and by eye. Or by [hand] reeling it back: one more full revolution, maybe I'll get it then.

MVI: What makes the other scores interesting is that you can see the difference between attempt and result. The Jump Cut video is a huge exercise in showing attempt and …

LN: …failure. Exercise and failure. 

Myriam: You are not even approximating. 

LN: Not even close! There is maybe half a second where twice two images would keep me up. 

MVI: Were you disappointed by the result? Maybe when someone is still too close to the attempt, they don't appreciate anything that derives from achieving the attempt.

LN: No! I was frustrated to death, but I thought it was hilarious, I really did. I had access to editing equipment in a library in Holyoke [Massachusetts], in this backroom where they had some editing equipment that was public access. I would be in there for eight hours, trying to do this stuff with nobody there to laugh with. I really thought it was hilarious. It was impossible! So frustrating. If I had kept on working in that way, as the technology developed, I might have eventually achieved … 

MVI: I am so glad that it did not exist then.

LN: It was definitely woman and machine and imagination all meeting. When I tried to do the jumps I was already frustrated because I knew I wasn’t up there long enough and I knew it was going to be impossible to edit it. 

MVI: Was there someone in the space? It felt like you were cooperating with someone.

LN: I don’t remember anyone being there with me, because I had to keep on going back to the camera, turning it on, going back, jumping…

MVI: Why would you want to do that? Why wouldn't you just let it run and edit it afterwards?

LN: Because I put the chair in and I had to see if it was in the good place. I had to see where my frame was in relation.

MVI: For a split second I thought the collaboration was that you were jumping and that someone else would voice and mark the aerial moments.

LN: No, I was making the mark. I was going ‘whooo’

MVI: Did you show this video as a real presentation? 

LN: Sometimes I did lectures. I'd show that one, I'd show “Weird Dance” [1979], I'd talk about the process, I'd show “Second Glances” [1979]. Sometimes I would do the “Postcard” pieces [1980s]. I did it in Boston Museum School of Art. I did it here in Bennington. I think that is the missing tape. Whenever I could, I showed them, but I did not enter them in any competition. “Second Glances” went around in some video festival type of things. I put the sound track on it in order to apply for a grant. I hate the sound track, but it doesn't matter. I hate the way I am describing it and speaking, I cannot stand it. It was very hastily done. But it does say what they are. I did it because of the grant. 

MVI: There is an interesting analogy for the Jump Cut in one of the posters for a Contact dance concert in Northampton. The jumping one. The poster was made by Stephen Petegorsky.

LN: Well, we all made it together. It was the idea to catch us in the air. I don’t remember, but it was around the same period for sure.

MVI: It is interesting, because you have a photographic analogy to what the video does.

LN: Yes. When I would do ([video workshops][00:48:18]), I would do them often … Actually I have a lot of these tapes setting up blind duets, blind trios, five to six live camera operators, having a photographer at the same station as the camera operators, and then clicking and looking for the moments. And then we would play back and still the moments of their choice, because we could hear the click and we could compare them with their shots. It was very clear. It was the same, because they were there. And then we could start to see what each photographer's sense of interest was in looking at flow of movement. That was wonderful. But they were all studies. But when I got rid of the cameras, it is the same work as the tuning scores: marking moments, measuring [for] moments of interest, [call]ing pause for those moments, arresting motion. Also, with video when you are looking for [a pause]… you had to turn the handle with your hand. In order to still an image you had to move, you had to make an action. It wasn't just a button press, but your whole weight…they were these long bars and you'd crank them sideways. 
