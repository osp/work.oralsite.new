text = """improvisation-festival-whats-that.md
title: Improvisation Festival, What's That?
url: http://sarma.be/docs/3275
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-1995-Nelson-Improvisation_Festival%2b%2b.jpg
author: L. Nelson 
date: 1995
order: 12

Over the past year, improvisation festivals have cropped up on two North American coasts: The 2nd Annual Improvisation Festival/NY (New York City), Seattle Festival of Alternative Dance and Improvisation (WA), Improv Festival and Symposium (Vancouver, B.C.), Engaging the Imagination: A Festival of Improvisation (San Francisco, CA), and Taken by Surprise: Improvisation in Dance and Mind (Berkeley, CA).

--

a-page-written-by-lisa-nelson.md
title: A Page Written By Lisa Nelson
url: http://sarma.be/docs/3295
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-Nelson-A-page-Written-By%2b%2b.jpg
author: L. Nelson 
date: 2002
order: 13

I look at the senses as attentional tools.  Their physical organs are focussing instruments.  My body must move to tune each of them in to a desired channel.  I’m grateful that their simple instructions give me cause to move.

--

after-thirty-years.md
title: After Thirty Years
url: http://sarma.be/docs/3277
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2005-Nelson-After-30-Years%2b%2b.jpg
author: L. Nelson 
date: 2005
order: 14

I strain to be concise. To not belabor the commonality of the struggle of keeping an indie dance publication alive with the struggle of improvisational dance to find a place in the dance market. After 30 years of both, I offer these thoughts

--

hunt-and-pick.md
title: Hunt and Pick
url: http://sarma.be/docs/3278
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2006-Nelson_Hunt-%26-Pick%2b%2b.jpg
author: L. Nelson 
date: 2006
order: 15

For a time, magazine-making was an antidote to the 'now you see it, now you don't' of performing. Holding an object in my hands at the end of an intense collaborative effort gave a startling satisfaction.

--

ode-to-artifacts.md
title: Ode To Artifacts
url: http://sarma.be/docs/3282
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2011-Nelson-Ode_artifacts%2b%2b.jpg
author: L. Nelson - J. Konjar
date: 2011
order: 16

Your process makes me wonder what turn my dancing would have taken if I’d had a video to study of the elderly musician who performed a five-minute dance in the Tibetan Folk Opera that visited a small town in southern Vermont in the early’70s.

--

preface.md
url: http://sarma.be/docs/3283
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2013-Nelson_preface%2b%2b.jpg
author: L. Nelson
title: Preface
date: 2013
order: 17

This little book invites us to be a fly on the wall of an improvisational dancer's, by nature, self-invented research. We enter the dance studio during Susan Sgorbati's process of merging with a science called Emergence, which in itself, and perhaps by its own nature, is still emerging.

--

report.md
title: [REPORT]
url: http://sarma.be/docs/3284
image: http://repo.sarma.be/Interview%20Affairs/Lisa%20Nelson/Anth-2014-Nelson-report%2b%2b.jpg
author: L. Nelson
date: 2014
order: 18

The CQ chapbook of my imagination doesn't exist. It is, rather, another opportunity to not know. And to not know it within thirty-odd pages of bound paper. I like to think that, like the 'empty' space that invites a dance to materialize, the chapbook is already there, one need only make it visible."""


articles = text.split('\n--\n\n')

import os.path

print(len(articles))

base = os.path.dirname(os.path.abspath(__file__))
folder = 'other-writings'

for article in articles:
  name, text = article.split('\n', 1)
  
  with open(os.path.join(base, folder, name), 'w') as h:
    h.write(text)