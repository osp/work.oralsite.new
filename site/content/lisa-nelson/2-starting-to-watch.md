Title: Starting to watch
Date: 30-01-2001
Location: in an apartment in Brussels
Order: 2
Cluster: Dance Background
audio: audio/lisa-nelson/2-starting-to-watch-with-one-second.ogg
       audio/lisa-nelson/2-starting-to-watch-with-one-second.m4a
       audio/lisa-nelson/2-starting-to-watch-with-one-second.mp3
Voices: Lisa Nelson (LN)
        Myriam van Imschoot (MVI)

This is perhaps one of my favorite interviews with Lisa Nelson. We had just returned from a trip to Rotterdam, where we saw a show of Meg Stuart, *Highway 101*. Back in Brussels we sit in the apartment where Lisa is staying for the time that she is conducting a workshop upon invitation by Contredanse, a Brussels-based dance organization who also later would play a significant role in Lisa Nelson's life and work.
{: .introduction}

The interview revolves still around the topic of Nelson's dance background, which we started off in Part 1; this time her early experiences as a *spectator* are the angle. It brings us to discuss film, music, and interestingly, when speaking about 'spectatorship', politics too. A world opens onto the alternative beatnik-style American summer camps, the sit-ins for civil rights, and the protest against the Vietnam war that terrified so many of Lisa's generation who were drafted to join the American war machine. Political awareness has been pulsating in every aspect of Lisa's life and work, even though we might not speak as directly about it as we do here. The choice to go and live off the land in rural Vermont, for one thing, and the commitment to an outsider's position in respect to the commodified dance circuits and institutions, for another, may account for that. Not to mention the relentless commitment to the sensing body, where imagination is fueled by physical wonder. This is what becomes the main subject of the interviews that follow in the Keyword Sessions. The first being on 'Attention'.
{: .introduction}

MVI: From fifteen onwards you could go downtown [in New York] yourself by subway. Did that change things you’d see?

LN: As an older teenager I was going more to jazz concerts. I some fantastic …I saw Monks' solo concert. I went to a lot of jazz. And tons of movies. Tons, and tons, and tons. It was a dollar to see three silent movies or three foreign films, so I spent most of my last couple of years going to films. 

MVI: But mainly art-films?

LN: All foreign films and silents. ( …) One other thing that did happen in high school is that I went to one of Allan Kaprow’s Happenings. I remember that quite well. I went with a few friends. I think it was at the New School. I heard about Happenings, I knew they existed, but I did not really know what one was.

MVI: By then the word 'happening' must have settled into discourse.

LN: Not so much. It was a thing you knew. I don't know how I would have heard of it, to tell you the truth. The other activity in my life were demonstrations and politics, probably from fourteen on. And actually from twelve on, I was going to sit-ins for civil rights — this sort of started in 1960. And I was going to these ([communistic camps][00:02:02]) in my summertimes. I got sort of pulled into the world of protest. This social scene was very interesting, in that young people got together, not just by themselves, but we were brought together by older people who would set up meetings. They weren’t overtly communists. They were sort of left wing discussion groups. And I had gone to this commie-camp, summer camp …

MVI: From when on was that?

LN: I think it started when I was twelve, or thirteen, fourteen. Or I went when eleven, twelve and thirteen. I wound up there more or less by accident. It was run by the communist party, by Pete Seeger and various other people. For my life, this was a very incredible kind of new patterning, because at the summer camp there was no program. And so the kids would invent their own program. There were activities you could go to. You could go to art, to dance, or you could go to swimming, you could go to volleyball. You could go and work on the farm. But you didn't have planned activities. You made your own schedule. So, really at eleven I was cut loose to make my own schedule. And I found that very simple to do. I was one of the kids that was always busy. I always had something I wanted to do. And there was also a sense of just hanging out. People would be sitting all over the landscape singing union songs, and there was a lot of music. It seemed spontaneous, but it was happening everywhere. And I think the adults were having meetings a lot of the time, they were organizing or something. And in the mornings, to wake up, they would play Bob Dylan on the loudspeakers. His first album had came out. I don't know what year that was. 

MVI: Jesus! Waking up with Bob Dylan!

LN: I know. Folk music was like Action. It wasn’t just recreation. That world was just a miracle for me! I loved it. I loved it! And I kept that life throughout. I chose to go to Bennington [College] where they had no requirements, no grades. I just found it very simple to be self-directed. ( …) I got lined up with this kind of network of activities outside of my high school, and outside of my neighborhood actually. It was just projects in Queens. It was a very redundant environment of brick apartment buildings. It got me out of my neighborhood. And for someone who didn't talk, these things were a little scary all the time. I was scared all the time that someone would talk to me. Everybody was always talking and discussing and I never said a word. But the milieu felt important. People were together for a reason, they were really together for a reason. 

MVI: That's good glue.

LN: Yeah, and it felt like it was important to be together, although I really was a fly on the wall. At some of these parties people would be ([dancing to Ornette Coleman][00:06:28]) ( …) Everybody was dancing by themselves, without partnering. You would never see that in a high school dance…yet. But in these gatherings, they were also taking drugs. Starting when I was fourteen. People were smoking dope and starting to take acid, which was a complete unknown. I guess, I am talking about ’62, ’63. I was offered drugs a lot when I was younger, like 12, 13. I didn't really know what they were, and I didn't take them. By my senior year I was smoking dope. But it wasn't in the culture. This was the Beat Generation’s stuff, it was the trickle down out of the Beats…the Beatnik's stuff. 

MVI: How is gender in this Beatnik culture? What position did women have in it?

LN: I grew up with a single mother. So I had that model right in front of me: a single working mother. But I did feel that the social climate in the gatherings was very honoring of women. 

MVI: 'Honoring'?

LN: It honored women. I felt their place. There were very strong women in all of the gatherings that I was at.

MVI: Were there black people in the summer camps?

LN: Absolutely. And that was my most contact with black people, because my neighborhood was white. Although the neighborhood next door was black. But my school was mostly white. My giant factory public school was white. But starting at eleven, going to camp, there were black kids and white kids together. And that was very noticeable to me at that time. Because of course in NY there are black, white, Asian, everybody is there, so you are passing through each other’s life, but as a child, your life is circumscribed by your family and your school.

MVI: But even the mixture was very unusual.

LN: People were separated even in neighborhoods. There were the Polish, there were the Irish … everybody was separated. You learn that very early. ( …) I was talking about these kinds of lives earlier: there was family, there was dance, there was school, and then there was this sort of social life that was a more political activist life. They were all separate populations. They did not really intersect, hardly at all. I think of this as very New York: that possibility of being in so many sub-cultures at once, and kind of just accepting that and taking that for granted. Because they were everywhere. And you were actually quite aware of those you weren't part of. 

MVI: For someone who never did a sit-in, could you describe it a little bit?

LN: Oh, let's say at the local five-and-dime — the local store — you'd go on a certain day and you'd sit on the stools [of the lunch counter]. You wouldn't order food; you'd sit. You’d sit-in. And because there would be also published literature saying this is a protest.

MVI: Against?

LN: Against segregation. It was a protest against segregation. It was mixed. It was black and white. If you were part of it, you were joining in the demonstration. Solidarity. And there was a lot of paper. I really don't know what the situation was in my neighborhood, whether they wouldn't hire black workers or whether they only hired black workers … It changed so much over the years that I don't remember exactly what the breakdown was. What I knew as a child was that there was segregation, and that blacks didn't have the same …it's not even rights, they were not accepted to do the same things. 

MVI: When did you have more of an organizational commitment?

LN: The civil rights thing turned into the Vietnam thing during my high school years. And once it was ([the Vietnam situation][00:12:14]) , it was absolutely completely personal. Because all of my friends in high school were getting drafted. So in the years ’64, ’65, ’66, when they were escalating the war in Vietnam and starting the draft …this was like MY life—the center of my life was my friends and my brother—everybody was going to get drafted. And so these demonstrations were internally motivated so deeply. I don't even know how often — whether it was every weekend — but there was action, most of it in Manhattan. It felt like it was happening all the time, but it probably wasn't. Every minute of the day we were worried. Is so and so going to get drafted? What's he going to do to try and get out? 

MVI: In comparison to many of your later artist peers, do you have the feeling that your political commitment was somehow rare? In retrospect, would you find many similarities with artists of that period. Someone like Simone [Forti] for example, she feels very uncomfortable with her book [*Handbook in Motion*] about the fact that it was published in '71 or '72 [note: in fact it was published in 1974] and it didn't ever, not once, mentioned the war or Vietnam.

LN: Well, my generation … You see, I was the generation that got drafted. She really was a different generation. She is 15 years older than I am. ( …) My generation was so instantly politicized and because we didn't already have a life, we were students just starting, we were able to just flow into being extraordinarily active in this movement. And the universities themselves were very active, they were totally filled with people with the same concern. So the breakdown in terms of Steve [Paxton]'s generation or my generation and Simone’s, it was a completely different stage of life and a different milieu. They were already in a subculture of artists and we were in a subculture of students. Right at the edge of moving along. A lot of my friends in this extracurricular world of politics were also artists. They were almost all artists. We were the nerds of the school.