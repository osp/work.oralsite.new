Title: Starting to dance
Date: 30-01-2001
Location: on the train from Brussels to Rotterdam
Order: 1
Cluster: Dance Background
audio: audio/lisa-nelson/1-lisa-nelson-dance-background-1-final-edit-(online-audio-converter.com).ogg
       audio/lisa-nelson/1-lisa-nelson-dance-background-1-final-edit-(online-audio-converter.com).m4a
       audio/lisa-nelson/1-lisa-nelson-dance-background-1-final-edit-(online-audio-converter.com).mp3
Voices: Lisa Nelson (LN)
        Myriam van Imschoot (MVI)

In this interview Lisa Nelson and I are scanning Lisa's early dance experiences in the first sixteen years of her life, before she went to Bennington College to continue her dance studies. The interview was conducted on the train from Brussels to Rotterdam - we talk casually yet focused while the wheels of the train clunk on the rail tracks and bring us closer to our destination. In Rotterdam we would see *Highway 101* by Meg Stuart.
{: .introduction}

There's a cut somewhere halfway in the interview, with a resulting jump in the conversation. Probably Lisa told me during that lapse about her father's untimely death when she was 11 years old and how this drastic event colored her teenage years with riddles of loss. 
The sounds of cigarette lighters during the interview bring to mind that back then, early 2001, one was allowed to smoke on the train.
{: .introduction}

Several years later, Lisa remarked that she had never studied a dance technique from scratch, always falling in the middle of something. It may explain why in her workshops and laboratories the exploration of the 'foundation' is so, well, fundamental. At any rate, that's how I experienced my first workshop with Lisa in the same month the interview was taking place. The workshop was organized by Contredanse in La Raffinerie, Brussels. From a note in my diary: 'To decompose' with so much fervor, analyze, break down the whole into experiential kernels and elements is hardcore, and feels uncompromisingly radical - radical, like in *radix*, Italian for 'root'. From that core spirals the future with momentum.
{: .introduction}



MVI: I'd like to go back to the first experiences you were conscience of. What brought you in touch with dance or anything relating to it? Because sometimes these recollections are strong and play a role later on.

LN: Well, they could. ( …) I started to go to dance class when I was six or seven, which was very normal in the U.S.. To have a neighborhood-dance teacher and send your kid to dance classes was very normal. So I did. But I was very lucky. Often you would study tap or acrobatics. Maybe a little ballet. But tap, acrobatics, and possibly ballet a little bit, would be a normal neighborhood dance teacher. But I was really very lucky. There was a woman in my neighborhood in Queens, who used to teach at the Actor's studio in NY where one of my aunts had studied with her. We found out she was teaching children, and she was a completely remarkable, idiosyncratic, wild Russian. She called herself ([Nadia Romanov][00:02:01]), but her name was Nadia Abeles. She was a very extreme character. She had a look about her of a kind of a Russian dance mistress. I think she was probably sixty at the time I first met her, but she might have been fifty. She had a very aristocratic bearing and she was very scary, but what she taught was very unusual: she taught ballet—terribly, really badly—and Graham, sort of. She is reported to have been in the 'Graham company' at some point. And the only story I remember her telling was how she didn’t have any underpants on and was in a performance of Graham’s; and so when she spread her legs, you saw her pubic hair…

MVI: …her beaver (laughs).

LN: Her beaver! She exposed her beaver in a Martha Graham performance! But actually it is pretty clear that she didn't know anything about technique. But she gave these classes, and she also taught—so, I must have been seven or eight when I was studying with her— the history of ballet and the history of Jazz. The history of ballet out of a book; and the history of Jazz out of a book and listening to the music and to recordings. So she kind of gave a really strong historic sense that you were studying something that wasn't just a physical exercise. That it had a whole surround and a culture. And she did creative work. Everybody made dances. So, it was really starting when I was eight. I joined her dance company, which was a small troupe. There were, I think, eight of us. Ranging in age from eight to eighteen. 

MVI: Eight to eighteen?

LN: Yeah…. And we made our own dances, and she choreographed dances that we learned. We each made our own dances, and our own costumes, and basically we made our own lights. She sort of introduced the whole picture in a very complete package. So, starting very young, that was what I thought dancing was. It was presented very simply and I didn't have any questions about it; and we toured the surrounding neighborhood, performing with the troupe at the Jewish centers and at the community centers. She had some concepts that were very interesting to me. She had a concept that she called: (['Today's music' and 'Yesterday's music'][00:05:50]). And I really don't remember what exactly the categories meant, but I got a sense that there was music that was real music, and music that was commercial music. That is what I remember. I don't know if that was her “today’s” and “yesterday’s”, but I had a very strong sense from her that there was commercial or pop music that wasn’t real music, and real music. Also, her forte was the piano. She taught piano. I also studied piano with her. She actually was a pianist.

MVI: Forte? You said her 'forte'?

LN: Yes 'forte', her 'strength'. She was actually much more a pianist than she ever was a dancer. I was more aware of that later. And there was another, there are a few incidents… 
she had a kind of reign of terror in her dance classes. The mothers often were there to watch the classes and she included them when she was teaching theory, ballet or … I remember she sent us home one day with an assignment to do a dance that you dance by yourself at home and then bring the dance and perform it as if nobody was there. Actually I think she was brilliant. I don't know how she got this across to children. I’ll never forget this. I went home and I had that thought in mind, and I always… Actually I just realized this yesterday when I was thinking about it… when we came in and showed our dances the next week, she ridiculed, she ridiculed us all.

MVI: **Everybody?**

LN: Yes, everybody, the little kids and the parents. “Because it is impossible,” she said, “to do a dance that you do alone in front of an audience.” But I begged to differ (laughing). I BEGGED TO DIFFER! With her, I never said a word, I was terrorized by her; but I thought about it at the time too and I realized that when I made my dances, which I always made in my living room, alone, without anybody there, I always thought of my dances as being seen by somebody. I always thought of them that way, as I was making them. There were no mirrors in my house, but I always imagined seeing my dances from the outside when I was making them. Since being a child. And so even though that idea was very stimulating when I went home to try it and brought…I can't remember what I brought in, but,I didn't feel like it was that different, actually. When I thought about it the other day, I hadn’t realized how much my later work was about this ([inner-outer view][00:10:06]). It was very natural for me to think about making dances to be seen. And I don't remember just dancing around by myself without making dances. I don't think I was the kind of kid who just danced her heart out in the backyard for just the joy of moving. Maybe I did, but I just had no awareness of it. But since I started making dances so young, I always imagined them being seen by others. So that really stands out to me, that one lesson she gave. She was very tricky with the parents in the room too and she would kind of fish around. She got them to provide words like “cliché.” I remember learning very young: What is a cliché? 

MVI: When she gave that assignment “as if no one is watching,” the trick is already…

LN: It was a trick. I am sure of it. But none of us had the slightest expectation. She was a terror. She really was a terror. You know, with little kids! 

MVI: So, did it get you… I can imagine that with so much terror involved, you might stop going?

LN: But I was terrified of speaking in school too. So it was not special. Also she liked me very much, I knew that, and she gave me tremendous encouragement. So, no, I never thought about not going. Sometimes I was scared to go if I knew we were going to be working on history because I was afraid she'd asked me a question.

MVI: A question you wouldn't know?

LN: Oh, whether I would know or not, I couldn't speak in public. I was so shy, it was awful. (…) Yeah, that was normal in my life. In school I was very shy. But I loved the dancing. 

MVI: If you were making those dances, there must have been many assignments of making?

LN: I don’t remember many assignments. I remember more that we would choose what we wanted. I don't remember assignments, except for that one. So I was with her from probably seven to… probably twelve or thirteen. But I started at ([Juilliard][00:14:11]) when I was eleven. It was the children’s… they called it the preparatory division. It was all day on Saturdays. It was just for children.

MVI: That’s a lot of dance. Once a week you go to this teacher in the neighborhood and once a week on Saturday you go to Juilliard.

LN: I was also once a week taking piano lessons and once a week taking guitar lessons. I was very busy. But I shifted away from her, including with the piano. There was a moment when I was eleven, I guess someone suggested I'd go to “real” dance classes, and I went to audition at the American School of Ballet. I remember that very well. It was most unlikely that I would go. My mother allowed me to go to an audition. I was not interested and I don't think they were that interested in me. Because I was too old. I was already eleven and I had no real previous training. Anyway I think they said I could come. But they were sceptical that I was starting so late. But I didn't want to go and that would be going into Manhattan more than once a week. Anyway, that was out of the question. But then I also auditioned for Juilliard and that was very possible just to go on Saturdays. And at Juilliard, I remember that at the audition they asked you what kind of music you wanted: fast or slow? Because they had fabulous musicians playing for dance at Juilliard. I can't remember if I said ‘fast’ and did a slow dance or said 'slow' and did a fast dance. But I did that. And I remember Pearl Lang, who taught composition there. Oooh …she remarked on that. I remember her remarking on that choice, whatever it was I did. So many patterns were made early for me, even before I went to Juilliard. I had already devised a way of making dances before I went to Juilliard. I generally would listen to music. I would dance to music in my living room around the furniture until something appealed to me, and then I would make some decisions. At that point I was listening to things like Ralph Vaughan Williams and Revueltas. My mother was always searching for music. She loved music and she’d bring back records all the time to listen to. World music and Jazz and Varèse. All kinds of music. And Miles Davis, naturally. Oh, absolutely. ‘Kind of Blue’ was out then. 

MVI: That's not music for kids …

LN: But she taught us the history of jazz. We went through everybody. Bix Beiderbecke, [Thelonious] Monk. That was 'living music', listening. The ballet history was really textbook stuff with pictures and photographs. But the jazz was living stuff. So that was very present. You know Edgar Varèse? Yes that would be in the more concrete area. Satie, it is sort of everybody… 

MVI: In other words, more contemporary classical music. And with Miles Davis, Thelonious Monk: lineages of Jazz. And the other names you referred to at the beginning?

LN: Early modern. Actually Silvestre Revueltas might be more contemporary at that time. We are talking about the fifties.

MVI: Improvisation was important to Jazz music. Did you have an understanding of that?

LN: No. Improvisation was not an issue. I don't remember anything. I don't think it was an issue.

MVI: Because you were just listening to the records and…

LN: Right. I probably just accepted the fact that they were improvising. But it wasn't an issue if something was composed in advance and read or whether it was composed spontaneously. Because actually we had not gone into John Coltrane and Ornette Coleman. Although I was listening to Coleman, for sure. But since the mind was so open to all kinds of music and sound, I was just responding very instinctively to what I heard. Same with the world music: African music, Balinese music … I just took it all in at that point.

MVI: Your first teacher, did she impose vocabularies from which to make the dances?

LN: She didn't really teach composition.

MVI: It is very structural then.

LN: She made really dumb dances. She taught us steps and we did them to music. But the dances that the children made were not like that. My dances before I went to Juilliard: I was always improvising, but I was always setting. I always knew what I was doing one thing after the next. I would get an idea from my body of something that I would take from my response to music. And I often didn't use the music that I made the dance with. I would play a different music once I got an idea of the shape of something. Sometimes I used the music. Often I think I made a kind of narrative in my mind. Not so much a story, but a narrative, like: “this comes after this comes after that.” I didn't change the order of things so much once it occurred to me. When I first went to Juilliard …

MVI: Can you explain the Saturday class: how was that?

LN: We did music theory for an hour and a half in the morning, with a marvellous man—I think he was a dancer: ([John Wilson][00:23:30]). I think he was one of the dancers that started Dance Theater Workshop. So he was a downtown independent choreographer. He was also a musician and he taught music theory at Juilliard to children. And he brought in a lot of the downtown world and his stories to us. We studied music theory and general things like we could try and listen to music and notate it and we'd learn simple components of musical composition: different time signatures, harmonics. And I guess he'd give us little exercises but I don’t remember them that well. I liked it very much. And also his stories. It turned out he talked about early Judson things, about Carolee Schneemann’s Meat Joy. I remember him describing it to us, and all these people rubbing their bodies in meat from the butcher. So I kind of had that world made out of stories from him. And then a class with Alfredo Corvino, one of the great masters of classical ballet. I guess he taught Cecchetti method. And he was a beautiful gentle teacher. Incredible. I remember how he would touch the body, when he touched the body of these little girls with a very gentle touch. And so he just taught ballet class. We did some work on point but very little. We obviously weren't on a ballet track. And then we had Graham class with Pearl Lang. And I think all of the classes were an hour and a half.

MVI: It would take the whole Saturday, then?

LN: Yes, it was a Saturday program. It went all day Saturday. It took an hour and a half to get uptown. It was up in Harlem, Juilliard, north of Columbia University. Probably it started at eleven or ten in the morning. And then after the Graham class was a composition class. So maybe I was there from ten to three thirty on Saturdays. And the composition class was small. It was just the people she would invite. That was a non-paid class. It was an extra. You didn't pay for that class. And the first assignment … Actually I was thrown right into the intermediate class of Graham, which I think was a terrible mistake. But that is what happened.

MVI: You sometimes say that your body “nearly got fucked up”?

LN: Oh, I got seriously fucked up by all of that Graham. It was awful.

MVI: So, Graham was not good for you?

LN: Maybe if I had gone to a beginning class it might've helped a lot. Because I could do things so easily. I appeared to be able to do things so easily. I think that's why I was just put into the intermediate class. I really hadn't taken a sensible technique class, ever, before I went to Juilliard. Nadia’s classes were ridiculously unsound. I'm sure the knees were going between the legs. Anatomically, the classes were really bad. But I was only eleven when I started at Juilliard and Corvino was very careful. I learned more about not letting my joints get fucked up with him. But the Graham was impossible. Also because I wasn't stretched. I wasn't stretchy at all. So the Graham was torture. However, I just did it. This was another realization I had after I left Juilliard: Pearl Lang was such an unbelievably beautiful dancer. What happens when you have an inspirational mover in front of you is very important as a student because you learn so much through your eyes. I mean, you take it in. You actually see that grace, you see that effortlessness, you see something delicious to look at, and your body strives to imitate it. And in many ways that was all that I was doing. I was striving to imitate what I thought was beautiful. And she was just effortless in everything she did. Effortless, powerful, fast. I guess I approximated it well enough to appear as if I was doing it. 

MVI: I can't even imagine an eleven year-old doing Graham.

LN: Oh, well gosh! They start so early! ( …) A year later I was in the “advanced” class. I was just pushed along. And my experiences in those classes was 'dancing to music', that’s all my experience was in the Graham classes.

MVI: Can you tell something about the composition class?*

LN: The first one I went to she gave us an ([assignment to do a poem.][00:30:41]) And I did Carl Sandburg's “Fog”. “The fog comes on little cat feet.” It is a very short poem. When I came in with that poem and showed that dance the very first day, she said: “Will you do it again?”. And I did it again. And she said: “Well, that was not again. That wasn't exactly the same. You have to set the movements. You have to know every movement.” And that was the biggest shock of my life at that moment. I really had a reaction to that. It scared me. But I tried it. And I pretty quickly devised a method to make my dances so I could remember them. It was a very simple method. Usually on Thursday or Friday night before Juilliard, I'd put on some music. I’d usually work with music and I would come up with one or two movements. So I made my dances as an accumulation and I'd show that much the next day. Maybe two or three movements, maybe I would've found a movement and changed the order a little bit.

MVI: That's a short dance.

LN: Well, it would take many weeks for me to complete a dance. This is what developed. Because I was there from eleven till sixteen. So I was there for six years. And this method evolved pretty quickly. Then we had moved to another apartment and I could see myself in the window in the living room. I remember being very interested in the shapes I was making at that point. But I couldn't see my whole body, I never could. But I would spend quite a while dancing until I found something that I liked and I'd try and see it in the window. This was always at night, so I could see a reflection. So I kind of did this measuring, a little bit, with what it looked like. But the windows were high and I was short. 

[recorder was stopped; a break]

LN: I didn't really start talking freely until after I quit college.

MVI: Lisa, I can't even imagine that.

LN: I was very quiet. But when I said something, people would tell me it was a kind of scary, because I would have been so quiet and then I'd say something just the way I speak now: a fully formed thought or opinion, with a beginning, middle and end. But to get to the point where I'd open my mouth … All these survival techniques became like all my material later. When I started looking at perception and survival, which got very interesting to me, and see how that interacted with my working process, like my observations of what a conversation is. Because there was no adult conversation in my home. (I think I might have told you this). And I really just didn't know what people talked about. And so I would listen with a kind of desperation to people in a social situation, trying to figure out why they spoke.

MVI: We started talking about dance. It seems implied that this was central to your life, but was it?

LN: Not at all! Actually I felt as if I had two lives: Saturday life which was completely different people, completely different place. I had three lives: my school-identity, my family-identity, and my dancing-identity, which was my Saturday life. And my Saturday life and the other two lives never intersected. Although in my last year of high school, my boyfriend came to see me perform at Juilliard and that was a total revelation. Nobody ever saw me do anything with dance outside of Juilliard. 

MVI: But for example, when you talked about Nadia, your teacher, you said she would involve mothers or families into the class. Does that mean that your mother was involved into your dance?

LN: She was involved with dance herself for a short period when I was young. She was very interested in dance. She, herself, she sculpted. And she was an athlete. And she had always wanted to be a dancer, but she never could do it for various reasons. She said she couldn't do it because she couldn't remember steps. She loved dancing. She took some classes with Helen Tamiris, I think, somewhere along the line. And she was interested in the history of dance. She fed my dance-life more by bringing me music. She was always shopping for music and always bringing me music. She saw my dances. She was very encouraging. She didn't really interact with it. She just was encouraging. She liked it. 

MVI: Was dance important to a teenager of that age?

LN: It was so important to me. My Saturday life was really important.

MVI: Dance was your core?

LN: Well, I was also really into classical guitar. I played guitar every day and piano. Dancing I didn't do every day. It was really interesting that I didn't, but I didn't. 

MVI: You think it is interesting because you needed these constraints?

LN: It is interesting because I never danced every day. I made these habits very early. I really did. I forged a lot of these habits very early and it really never changed. There were periods when I danced all day, all the time, later, like in my twenties, with boundless energy, I was always dancing. But that was just a short period. In actuality it was so in the fabric of my whole life at that time that it wasn't as if I was doing it every day. I was just living it. Whereas before, it would have had to be part of other things I needed to do, like go to school or something. When I went to Connecticut College, a summer festival, American Dance Festival, after I finished high school, before I went to Bennington, I took a summer at the American Dance Festival, where you'd dance all day every day. And I was AWOL most of the time. I cut classes all the time. AWOL: Away Without Leave. It just was not in my makeup to be in a studio all day long, doing dance classes. So, I experienced that. I had an inkling about that. I had an idea I might not be cut out for that and it was quite true. In there, at the summer course, I studied everything: ([Limón technique,][00:40:55]) Lukas Hoving. I got to try on different languages. I had never done any of that. I had only done Graham and ballet. 

MVI: Limón seems to be a nice technique. I don't know too much about it. I'm going to read more about it.

LN: It is very free. And it works with space totally oppositely than Graham. It is really free in space. It is the first time I experienced a Release class. I was studying with Betty Jones there. She had us lie on the floor in the “rest position,” and that was a miracle to me. That blew me away! I never heard of such a thing. But I was hopeless at Limón technique. Hopeless! I couldn't do it at all! ( …) I had no stretch. I was bound like a wrestler from the Graham technique. I was so bound. It looked nice and free, but it was really hard for me and it never appealed to me. I hadn't studied Cunningham yet. But there was another part of it which was going to see things. That was a really important part of the idea of dancing.

MVI: Here we can go back to the performance experiences.

LN: I found this very interesting too: what I was exposed to see? From very young I’d go to the ballet. 

MVI: The community ballet?

LN: No, I'd go to City Center every year, or maybe twice a year. That was regular. And I saw Balanchine ballets, but really the only thing that appealed to me were the male solos. ( …) In my childhood it was so clear that I was not formally interested in any way.

MVI: You were not identifying?

LN: I was looking at the movements.

MVI: Of course.

LN: So I'd go to the ballet and the only thing I liked, sometimes, was that: the male soloists moving through the air, with just full dancing. It felt like it was full dancing. I also went every year to see Graham, which never appealed to me: I didn't like the stories. The myths went right over my head. Some of the movement I thought was interesting. But not the movements that you learn in class. She does some interesting things, but I couldn't tell you then, because it mostly did not appeal to me. I wasn't that interested in narrative. 

MVI: Did she appeal to you as a performer?

LN: This kind of stark, haughty … I would say that Graham went right over my head. Pearl Lang's dances also. I just was not interested. The language didn't appeal to me, the movement. But I also went to see Nikolais every year. Although I wasn't that interested in the pyrotechnics. I liked the psychedelic stuff, but that wasn't what appealed to me. He always had solos in his dances, even with all the strange costumes and body distorting things. He always had solos without these costumes by his main dancers: Murray Lewis, Phyllis Lamhut, Bill Frank, and there was another blond woman. Although none of them appealed to me to look at, the movement fascinated me. It was very tiny. He did these tiny, tiny little articulations. Out of all of the quote “dance” that I was seeing — the “dance,” the “modern” dance — that's what appealed to me: these tiny little articulations and strange body dislocations. And I recognized at the time that I really wasn't interested in them as performers, none of them, but I was fascinated by the movement. That is what I saw from probably the age of seven, all the way to … Maybe not so much in my later teenage years. I did not keep going. I may have been thirteen or fourteen. That is what I got to see. Basically that was the food I got visually for dance performance.

MVI: You really had a privileged cultural background. In comparison to …

LN: Oh yeah, I was taken to see these things every year. It was amazing. It was because my mother was interested. But she didn't shop very hard. Maybe if we had lived in Manhattan I would have seen more, but we didn't, we lived in Queens. She had a full time job. She worked all the time. These were more rare events. If we had lived in Manhattan I would've seen a lot more. The other things that she took me to were some theatre things. Like at La MaMa [Theater]…

MVI: Would that have been in that period already?

LN: In the sixties? Sure. I was eleven.

MVI: When were you born?

LN: In 1949. When I was eleven, it was '60. So when Judson was happening, I was young. The first few years of the 60s I was quite young: 11, 12, 13.

MVI: You were a baby.

LN: I would not go into Manhattan alone by myself at night until I was fifteen. But anyway, these theatre things also imprinted on me very strongly. These experimental theatre works that weren't so text-based but had a really strong physical presence. 

MVI: You refer to “La MaMa”, and also to …

LN: Yeah, stuff at ([La MaMa][00:49:15]). I don't even remember what I saw there. Or a production of Genet's The Blacks, not at La MaMa, but at an off-Broadway tiny little theatre, about as big as these four seats. I'll never forget. It was done with masks. I also saw Marcel Marceau every year, probably from eight on. And I liked a lot of it. It is funny, I wasn't that literal. I wasn't so interested in narrative even then. That wasn't what I liked about Marceau. Certain ways in which he moved just knocked my socks off. And then in these theatre productions, since I couldn't follow the narratives that well— I was never that text alert, I'd lose the thread if it was language—it was the movement that fascinated me. And the atmosphere. These off-Broadway places had very strong atmospheres. The place is very intimate. Incredibly intimate. And the dance was not so intimate. It was always on stages. But the theatre things were so close! So that made a big impact. That was my patterning of interest. Also in terms of live performance, every year I'd go hear Segovia, and I'd go hear Sabicas, the performing flamenco guitarist, who was coming to NY a lot. 

MVI: Maybe a five-minutes-off-the-track-question: with everything happening in the fifties and sixties, you can read a lot about Merce Cunningham etc., but what surprises me is that there seems to be a taboo on ([Nikolais references][00:51:15]). Because it is a world apart. Is it because they had nothing in common? Why is it?

LN: I've seen some marvellous documentaries on Nikolais. And I read some about him more recently. I think he was such an iconoclast. He crossed all the categories. He really made total theatre. He made his own electronic music. 

MVI: He was ground-breaking in electronic music, synthesizer music. He was abstract before … (…)

LN: I share your puzzlement. He made improvisation systems that were very well known. Everybody knew that that was what he was up to on Henry Street. I wonder if I'd ever gone there what I would have found. In a way I'm glad, because he had a system. I was glad not to know any systems. And he was very structural in compositional and in improvisational formats, but also very free. I know that from people who did study there when they were younger. There are some very nice documentaries on him. 