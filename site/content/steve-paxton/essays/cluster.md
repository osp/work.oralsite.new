Title: Responses
Order: 5
HasPage: true
Template: cluster
Image: images/steve-paxton/menu/essays.jpg
color: hsl(353, 100%, 98%);

### Texts, essays and impressions

Responses contains a growing collection of texts, essays, and impressions that are propelled by the vast collection of interviews in *Conversations in Vermont: Steve Paxton*. An archive, at its best, is not just a mere collection of artifacts, their descriptions and inventories, but a generator of stories, narrations, insights, of history, past, present and future. The first two responses are contributions by American choreographer Moriah Evans and French scholar and curator Lou Forster. Evans speculates in a personal manner about the impact of Paxton’s work on her own artistic practice, the lineage and reception history of Judson Dance Theatre, improvisation as a hyper-dynamic practice, and what that means in Trump's America. 
Forster, on the other hand, uses his historical magnifying glass to approach the photo score, its genesis and its entanglement with art history via Suzi Gablik, René Magritte and procedures of seriality. Both responses are entangled with the interviews, produce new landscapes, and push them beyond their own horizon. A third response comes from Myriam Van Imschoot and was read out loud during the sneak preview of *Conversations in Vermont: Steve Paxton* at Dansehallerne, Copenhagen, on March 3rd, 2020. 
