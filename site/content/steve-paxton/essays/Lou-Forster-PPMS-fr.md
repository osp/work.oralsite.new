Title: PPMS (fr)
Author: Lou Forster
Date: 01-01-01
Template: essay
color: hsl(353, 100%, 93%);

Les *Conversations in Vermont* de Steve Paxton, Myriam Van Imschoot et
Tom Engels peuvent être abordées comme un document historique produit
d’une double conjonction. D’un côté l’objet principal du récit : le
développement de ce second moment de la danse moderne aux États-Unis qui
s’ouvre à la fin des années 1950, et dont Paxton est l’un des
acteur·ice·s privilégié·e·s ; de l’autre, celui des années 2000, en
Europe occidentale, marqué par un intérêt renouvelé pour le travail des
chorégraphes du Judson Dance Theater. Dans cet article, je ne vais pas
essayer de rendre compte de la variété des sujets et des questions
abordées par Paxton, Van Imschoot et Engels qui couvrent la complexité
d’une vie à l’œuvre dans la danse. Lire uniquement le chapitre « *Route
and Routines *» m’a pris environ quatre jours, un temps que chacun·e
devrait pouvoir accorder à ce texte qui couvrent la formation du
chorégraphe, sa transformation (technique, sociale et culturelle) en
danseur à New-York à la fin des années 1950 alors qu’il est interprète
pour la Merce Cunningham Company ; sa participation à l’invention de ce
qu’il est convenu d’appeler la danse « postmoderne », un qualificatif
qu’il rejette ; le développement des techniques d’improvisations pour
lesquelles il est reconnu aujourd’hui ; son positionnement critique
vis-à-vis de la réception du Judson et du travail de ses autres membres.

Mon propos va se concentrer sur une question ou un problème, le recours
au images photographiques dans le processus de création chorégraphique,
sur lequel la discussion de Paxton et Van Imschoot roule à plusieurs
reprises. En 1961, Steve Paxton est à la fois membre de la compagnie
Merce Cunningham et participe à l’atelier donné par Robert Ellis Dunn
dont émerge le Judson Dance Theater. La réalisation de sa deuxième pièce
chorégraphique, *Proxy* (1961), le conduit à produire ce qu’il nomme des
« *people photo scores* » ou « *people photo mouvement score* » (PPMS),
qu’il utilise par la suite pour trois autres pièces *Flat* (1964),
*English* (1963) et *Jag vill gärna telefonera* \[Je voudrais passer un
coup de téléphone\] (1964). Ces partitions constituent, ainsi, l’une des
méthodes de création chorégraphique qui caractérisent le travail de
Paxton dans les années 1960[^1].

## Une partition parmi d’autres

L’invention de la première partition de photos de personnes en mouvement
avait jusque-là été commentée par Steve Paxton dans un entretien à
*Avalanche* en 1975 et par l’historienne de la danse Sally Banes.
« Steve Paxton, nous apprend-elle, a réalisé *Proxy* alors qu’il était
en tourné avec la Merce Cunningham Company*.* Ce trio était "une danse
lente composée en quatre sections : les sections deux et trois
utilisaient les partitions de personnes en mouvement, les sections une
et quatre des instructions”. La danse impliquait beaucoup de marche ; se
tenir debout dans une bassine pleine de billes ; prendre la pose avec
les photos pour modèle ; boire un verre d’eau ; manger une poire. Paxton
parle de cette danse comme d’une réponse à l’atelier de Dunn autour des
partitions de John Cage »[^2]. Le chorégraphe précise les enjeux qui
l’ont mené à réaliser cette partition :

> « J’avais le sentiment qu’un pas supplémentaire était nécessaire dans
> le recours à l’aléatoire pour arriver au mouvement grâce à de
> l’aléatoire. Le fait que le mouvement implique un choix me posait, en
> quelque sorte, un problème logique. Si l’on utilise l’aléatoire alors
> pourquoi ne pas l’utiliser jusqu’au bout »[^3].

La réalisation de la première PPMS s’encre, ainsi, dans l’une des
grandes controverses esthétiques des années 1950-1960, à
l’entrecroisement entre danse et musique, et dont le compagnie Merce
Cunningham est le laboratoire. Depuis 1951-1952, John Cage à recours
pour la composition de ses pièces musicales à de l’aléatoire (*chance
operations*)*.* Les divers sons joués par les instruments sont organisés
dans un tableau puis tirés à pile ou face avec plusieurs pièces de
monnaie en suivant le modèle d’une ancienne méthode de divination
chinoise, le Yi Jing[^4]*.* Utilisée pour la première fois pour le
*Concerto for Prepared Piano* (1951)*,* cette technique graphique est,
ensuite, adoptée par Merce Cunningham pour *Sixteen Dances for Soloist
and Company of Three* (1951). La composition des gammes de mouvements,
des durées et des directions dans l’espace de chaque danse y sont tirées
au hasard.

Steve Paxton affirme que les PPMS déplacent l’aléatoire de la
composition du mouvement à celui du mouvement lui-même. Les
représentations photographiques des personnes en mouvement offrent au
chorégraphe un vocabulaire de substitution dont la mise en danse
constitue ensuite l’un des enjeux du processus de création de *Proxy*.
Le titre de la pièce décrit, d’ailleurs, cette dimension puisqu’elle est
chorégraphiée « par procuration » (*proxy*). Steve Paxton en donne la
description suivante :

> « J’ai fait la partition puis l’ait donné aux interprètes qui
> pouvaient la lire de manière linéaire ou circulaire. Vous pouviez
> commencer à n’importe quel endroit et suivre ensuite plusieurs sens de
> lecture. En revanche, la durée des pauses et ce que vous faisiez entre
> elles n’était pas précisé. C’était une grande zone de choix qui
> n’était pas influencé par le chorégraphe. Durant les répétitions nous
> avons passés les matériaux et discuté du détail des postures. Nous
> avons regardé la danse et discuté du fait de savoir si les photos de
> la partition étaient précisément reproduites et nous avons travaillé
> pour qu’elles le soient plus précisément. Nous avons également parlé
> de l’interprétation de la partition parce qu’il y avait une confusion
> : Quand vous regardez une partition de photos vous pouvez interpréter
> les photos de manière correcte pour vous ou pour le public, de la même
> manière que durant un cours le professeur pointe le pied gauche et que
> vous êtes supposé·e·s pointer automatiquement le pied droit. Cette
> convention a été questionnée. Dans le processus nous avons traversé
> les différents points qui permettent aux personnes de se sentir
> confiantes. Et puis, il·elle·s ont dansé en confiance. La performance
> était détendue et avait sa propre autorité »[^5].

La partition de *Proxy* transforme la manière d’aborder la création
chorégraphique en permettant à Steve Paxton d’introduire plusieurs
facteurs d’indétermination. Les interprètes sont libres de commencer à
danser à partir de n’importe quelle photo de la partition, de circuler
de manière linéaire ou circulaire dans la mosaïque d’image, de négocier
le temps de pose et la manière de passer d’une image à l’autre.
L’indétermination (*indeterminacy*) est, l’autre principe, que les
compositeurs Morton Feldman, Earle Brown et Christian Wolff, notamment,
débattent activement à la fin des années 1950. Comme l’explique
l’historien James Pritchett, alors que pour Cage « “l’aléatoire”
\[*chance*\] désigne le recours à une procédure aléatoire \[*random*\]
dans l’acte de composer*,* “l’indétermination”, désigne, quant à elle,
la possibilité qu’une pièce soit performée de manière substantiellement
différente, c’est à dire qu’elle prenne une forme telle qu’il soit donné
à l’interprète une variété de manière unique de la jouer »[^6]. À cet
égard, l’indétermination n’est pas le propre des formes contemporaines
de composition musicale. Dans sa conférence sur le sujet,
« Indétermination » (1958), Cage analyse les formes qu’elle prend dans
*L’Art de la fugue* de Joahann Sebastian Bach ou *Klavierstück XI* de
Karlheinz Stockausen[^7]. Les *Conversations in Vermont* explicitent la
position actuelle de Steve Paxton sur cette question :

« \[L’aléatoire\] était, en quelque sorte, l’une des prémisses de mon
travail. Je ne souhaitais pas tant avoir un mécanisme objectif qui
prenne des décisions à ma place que de créer une situation dans laquelle
les interprètes puissent faire des choix avec les mécanismes
qu’eux·elles mêmes avaient développés, et qu’il·elle·s opèrent, ainsi, à
un certain niveau de la création de la danse. Je me suis, donc, ouvert à
l’indétermination, ce terme que Cage utilisait pour désigner les options
que les performeur·euse·s pouvait choisir dans la structure,
mais… »[^8]

À ce moment là, le chorégraphe hésite. Quel mot pourrait adéquatement
décrire adéquatement la relation des danseur·euse·s et de la partition
avec laquelle il·elle·s travaillent. Si le terme “improvisation” est,
évidemment, anachronique[^9], celui d’ “indétermination” est trop ancré
dans l’esthétique cagienne. Entre ces deux champs, Paxton conclue
provisoirement qu’il « cherchait à voir ce que cette structure
créait »[^10]. Si la dimension expérimentale et pragramatique du travail
de Paxton avec les partitions doit être reconnu, une explication plus
générale semble pouvoir être trouvé dans une théorie générale de l’image
qui guide Paxton dans son exploration des relations entre les pauses et
leur mise en action.

Avant d’aborder ce sujet et d’abandonner la terminologique cagienne, on
peut toutefois se demander dans quelle mesure l’aléatoire constitue-t-il
une "prémisse" des PPMS. Formulé autrement: *Proxy* et les pièces
suivantes peuvent-elles être considéré comme étant *à la fois*
indéterminées *et* composées de manière aléatoire ? Si, comme on vient
de le voir, l’indétermination tient un rôle privilégié dans le travail
des interprètes avec la partition, auquel Steve Paxton accorde une
dimension politique, la manière dont le choix des photographies a été
effectuées et leur organisation graphique sur la page n’est pas décrite
directement par Paxton. Pour lever le doute sur ce point, Sally Banes
site une interview qu’elle a réalisé en 1980 avec Robert Ellis Dunn dans
laquelle il explique que « Paxton avait découpé les images, les avait
laissées tomber, les avait laissées flotter, puis les avait collées.
L’une d’entre elle était une photo d’un joueur de baseball qui glissait
pour atteindre la base. Ces instantanés étaient vraiment très
beaux »[^11] . Banes en conclut que la méthode utilisée pour réaliser la
partition « était plus proche de celle de Duchamp que de celle de
Cage »[^12]. Ce dernier témoignage et la conclusion à laquelle arrive
Banes exclut en partie l’aléatoire mais s’avère toutefois questionnable
à la fois par son caractère indirect et vague.

Si l’on confronte, maintenant, ce témoignage à la seule PPMS conservée,
celle de *Jag vill gärna telefonera*, on constate que l’organisation des
photographies sur la page y obéit à une organisation iconographique et
gestuelle précise. Composée de trois feuilles de papier
d’approximativement 40 x 60 cm, elle rassemble sur le niveau supérieur
des photos de sauts ; sur celui du milieu, des variations sur la marche
(en haut à gauche), la course à pied (au centre) et la chute (à
droite) ; enfin, le troisième panneau explore différentes formes de
relations à deux, depuis la contiguïté jusqu’au contact. Des gommettes
de couleurs et des flèches indiquent plusieurs sens de lecture et
dessinent des itinéraires relativement cohérents comme, par exemple,
lorsqu’on regarde les différentes photos de droite, de haut en bas.
Saut, chute, saisie et porté s’y succèdent offrant une sorte de
découpage chronophotographique d’une même action. Il semble, donc, exclu
que cette partition ait été composé grâce à de l’aléatoire*,* quant à la
forme de suspension du jugement inspiré par Marcel Duchamp, elle
demanderait, au-moins, à être précisée. Steve Paxton au cours de sa
discussion avec Myriam Van Imschoot, donne un indice important sur cette
question :

« Faire des partitions était simplement \[une tentative\] de réfléchir
au mouvement. Évidemment, sur scène je présente une danse, mais lorsque
je fait la partition, je présente à mon esprit \[*mind eyes*\] toutes
sortes de transitions entre une chose et une autre. Travailler avec les
photos devient quelque chose comme une danse mentale, et l’on devient
très intime avec ces photos [^13]».

Le témoignage de Paxton diverge, radicalement, de celui de Robert Ellis
Dunn et de la conclusion à laquelle arrive Banes. C’est en regardant
attentivement les tableaux qu’a réalisé Merce Cunningham pour soumettre
le mouvement à l’aléatoire que l’on peut démêler cette question. Comme
Paxton, Cunningham faisait des partitions pour « réfléchir au
mouvement » et non, uniquement, pour le composer. Ces tableaux
rassemblaient des gammes de mouvement comme différentes sortes de pas,
de sauts ou de chutes[^14]. Le recours à une technologie intellectuelle
comme la grille afin de cataloguer des mouvements était, en fin de
compte, aussi important que de tirer à pile ou face l’ordre de leur
enchaînement. De la même manière, la PPMS de *Jag vill gärna telefonera*
rassemble et organise des gammes de mouvements offertes par des
magazines de sport. Abandonnant le "mécanisme objectif" des *chances
operations,* Paxton conserve le recours à une littéracie[^15] qui lui
permet de développer son imagination kinesthésique.

**Image double **

« Myriam Van Imschoot: Comment avais-tu accès à Magritte ? À travers
des livres ?

Steve Paxton: Des livres et des posters au début, je pense. Puis, je
me suis rendu compte que Jasper et Rauschenberg étaient, également,
intéressés par lui, et Cage, aussi. \[…\] Et puis, il y avait Suzi que
je connaissais bien… »[^16].

Née en 1934 à New York, Suzi Gablik est une historienne de l’art et une
artiste. Elle étudie à Black Mountain College puis à Hunter College avec
Robert Motherwell au début des années 1950. En 1969, elle codirige avec
John Russel l’important catalogue *Pop Art Redifined* [^17]*.* À la fin
des années 1970, elle arrête sa pratique artistique et se consacre
entièrement à la critique d’art. Elle devient la chroniqueuse d’*Art in
America* à Londres. Elle est, aussi, l’auteure d’un des textes de
référence sur René Magritte[^18] chez qui elle habite, ponctuellement,
au cours des années 1960 pour écrire sa monographie. Un élément moins
connu de son parcours est son amitié avec Steve Paxton et sa
participation à une pièce du chorégraphe, *Ann Arbor Deposit* en
1966[^19]. Les *Conversations in Vermont* éclairent l’importance jouée
par la critique dans la réflexion de Paxton sur l’image et sa
fréquentation quotidienne des peintures de Magritte ou de leur
reproduction au début des années 1960[^20].

Dans son livre sur Magritte, la critique d’art insiste sur l’importance
de ce qu’elle nomme les « images doubles ». Au cours des années 1950,
l’artiste belge se détourne de l’approche surréaliste de l’objet trouvé
procédant de « la rencontre *fortuite* sur une table de dissection d’une
machine à coudre et d’un parapluie » [^21] pour explorer les « affinités
cachées entre les objets, \[comme\] la relation du soulier avec le pied,
du paysage avec le tableau ou du visage de la femme avec le corps
féminin. \[…\] Une image conceptuelle unique n’exprime \[alors\] la
synthèse de deux ou plusieurs images conceptuelles que si elle naît à
l’intersection d’un paradoxe »[^22] . Pour cerner ces « intersections »,
Magritte développe une pratique du dessin où se réfléchit le
dédoublement des images. Plus ponctuellement, il utilise la photographie
pour arranger ses compositions dans lesquelles il pose, parfois,
lui-même. Dans les descriptions qu’il donne des PPMS, Steve Paxton
insiste sur la succession des images car elle constitue l’essentiel du
travail de studio. L’instruction donnée aux interprètes consiste à
passer d’une image à l’autre par le mouvement. On peut toutefois avancer
qu’une des fonctions de ces partitions consiste à produire des images
doubles comme c’est le cas, en particulier, dans *Flat* (1964). Pour ce
solo, Steve Paxton porte un costume. Assis dans le public, il se lève et
rejoint la scène une chaise à la main. La pièce alterne, alors, des
parcours circulaires autour de la chaise et des poses durant lesquels
Paxton enlève à chaque fois un vêtement. Arrivé au sous-vêtement, il se
rhabille méthodiquement en suivant le même principe. Durant les poses,
les vêtements sont pendus aux crochets qu’il a scotché directement sur
son bras gauche et sur le haut de son dos et il active les photos de la
PPMS. Si certaines photographies sont performées de manière quotidienne
et avec une lenteur démonstrative, enlever ses chaussettes, par exemple,
d’autres peuvent faire l’objet de pantomimes ou de mouvements virtuoses
et rapides selon le registre de l’image : Faire pipi, regarder au-dessus
d’un mur, pêcher, embrasser l’espace de ses bras comme une statut
Classique. En 1990, Steve Paxton décrit *Flat* de la manière suivante :

> « Une série de petites surprises : le costume, se déshabiller, les
> pauses et les crochets sur le corps. Le déplacement ironique du
> familier rappel les peintures de Magritte »[^23].

Il semble, donc, que ce solo active les images de deux manières
distinctes. La première consiste en une succession des poses en suivant
la PPMS, l’autre est la fabrication d’une image double dans laquelle,
Paxton devient, à la fois, une figure masculine à la Magritte (à
laquelle il ne manque qu’un chapeau melon)[^24] et un porte manteau. On
pourrait avancer l’hypothèse que le tableau qui lui sert de référence
est *Un peu de l’âme des bandits* (1960) dans lequel le visage d’un
violoniste est remplacé par son instrument. Les dessins préparatoires
pour ce tableau dont le titre original était *Les lettres persanes*
appartenaient depuis 1960 au collectionneur new-yorkais grand ami de
René Magritte, Harry Torczyner, que Suzi Gablik ne pouvait pas ne pas
connaître. Dans ces dessins on voit, notamment, le violon accroché au
dos du violoniste (feuillet III et IV) là où Paxton place l’un de ses
crochets. On peut, également, trouver une continuité entre le trait fin
et presque hésitant des croquis de Magritte et celui de Paxton lorsqu’il
tente de figurer, à son tour, l’intersection de ces deux images dans le
catalogue *Body Space Image : Notes Towards Improvisation and
Performance* en 1990[^25].

## Succession et sérialité

Il me semble que le livre de Suzi Gablik invite à renouveler un deuxième
lieu commun du recours aux images dans les pièces de Steve Paxton. Les
historien·enne·s de la danse et le chorégraphe lui-même insistent sur la
prégnance des études chronophotographiques d’Edward Muybridge pour
penser le rapport des photographies au mouvement dans les années 1960.
On retrouve cette influence dans la partition de *Jag vill gärna
telefonera,* où la succession des prises de vues est organisée de sorte
qu’elle invite à une lecture linéaire. À l’action de sauter, succède la
chute, le fait de se relever, etc. Toutefois, les poses appartiennent à
des séries différentes (athlétisme, football, box), et Steve Paxton
reconstitue, donc, une succession à partir de provenances hétérogènes.
Finalement, la série n’est pas le résultat d’une action organique, comme
dans les chronophotographies, mais d’un montage qui créé l’apparence
d’une continuité. La référence à Muybridge s’avère, donc, plus complexe
qu’il n’y paraît au premier abord, la PPMS jouant sur la disjonction
entre la mise en série des images et leur succession.

Dans la troisième section de *Proxy*, Steve Paxton met, cette fois-ci,
directement en scène un dispositif à la Muybridge: Un rideau noir autour
duquel les danseur·euse·s tournent, apparaissant et disparaissant aux
yeux des spectateur·ice·s à sept reprises. L’interprétation qu’en donne
Sally Banes et Carrie Lambert-Beatty à sa suite, c’est que Steve Paxton
s’intéresse dès le début des années 1960 à la manière dont la
technologie façonne nos perceptions. En mettant en scène ce dispositif,
il explorerait ce que Walter Benjamin appel « l’inconscient optique » —
la dimension du monde que révèlent le cinéma ou la photographie
lorsqu’un gros plan ou un ralentie nous donne accès à des phénomènes
invisible à l’œil nu. L’utilisation des poses opèrerait « presque comme
si les chorégraphes voulaient s’approprier la faculté qu’ont les
réalisateurs de ralentir un film ou de le regarder image par image
»[^26]. Il me semble que l’on peut douter du fait que cette section de
*Proxy* vise à reconstituer littéralement l’expérience de Muybridge pour
placer les spectateur·ice·s dans la position d’analyser le mouvement. Le
recours au ralenti et au défilement image par image dévoile plutôt une
dimension proprement inconsciente de la vision que l’expression de
Benjamin souligne. En cela, l’approche du mouvement de Paxton possède
des affinités avec la représentation non-narrative du quotidien chez
Magritte[^27].

Dans la production picturale de René Magritte, on trouve plusieurs
tableaux représentant des images multiples comme *L’homme au journal*
(1927). Une pièce y est figurée à quatre reprises mais dans une seule
d’entre elles, un homme se trouve effectivement assis lisant son
journal. Cette image compartimentée introduit un récit banal dont
l’absence de liens de causalité entre les figures qui la compose ne
permet, toutefois, pas de restituer la succession. Cette sortie du récit
construit, alors, le tableau en énigme. Il me semble assez probant de
rapprocher la troisième section de *Proxy* de cette disjonction
magritienne entre la série et le récit. La succession des images que
propose le dispositif de Paxton confronte non-seulement les
spectateur·ice·s à leur inconscient optique, mais encore à
l’impossibilité de fondre des apparitions discontinues dans une
structure narrative. Leur nombre, sept, a, par ailleurs, un but précis.
Comme pour les *White Paintings* de Robert Rauschenberg, il s’agit du
« plus \[petit\] nombre dont vous ayez besoin \[…\] pour suggérer
l’infini »[^28]. La section construite à partir de la PPMS nous plonge,
donc, dans un univers à la fois quotidien, le registre des images
utilisées, et étrange — la causalité et le temps n’y gouverne plus la
succession.

Cette partie de la pièce entretient une relation étroite avec les
tâches, manger une poire et boire de l’eau, performées précédemment. Si
la différence principale réside dans la durée réelle des tâches, elles
partagent un même sujet — le quotidien — et un dispositif de monstration
— les tâches sont performées dans un espace lui-même compartimenté, un
carré marqué au sol par du scotch jaune. Dans cette pièce, les tâches
n’ont probablement pas pour fonction de produire un effet de réel et
l’absence de péripétie qui les caractérisent se rapproche, plutôt, des
tableaux de Magritte. Elles produisent une étrangeté comparable aux
premiers films d’Andy Warhol. Dans *Sleep* (1964), John Giorno, l’aman
de Warhol à l’époque, était filmé durant 5 heures et 20 minutes,
dormant. En étendant la durée d’une tâche au-delà de la durée de sa
représentation habituelle, Warhol insistait sur l’étrangeté fondamentale
d’une véritable intimité partagée. Plus qu’une recherche moderniste sur
les fondements biomécaniques du mouvement dont les expérimentations de
Muybridge seraient le modèle[^29], *Proxy* tend, plutôt, à troubler avec
de nouveaux outils chorégraphiques, les tâches et les PPMS, les normes
de la représentation. Décrit comme une série de « révélations »[^30],
*Proxy* donne accè*s*, par des ellipses ou des durées, à la réalité de
la vie du chorégraphe qui était, à l’époque, probablement
irreprésentable et indéniablement camp.

Je n’avais jamais trouvé cette dimension du travail de Steve Paxton
aussi clairement explicité que dans les *Conversations in Vermont.* Les
PPMS apparaissent au cœur de problèmes théoriques et intimes comme celui
de la durée réelle des tâches et du temps narratif des images, dont elle
bouscule le découpage. Alors que les pièces du chorégraphe ont souvent
été décrites comme *cool*[^31] ou « jésuitique »[^32], elles s’avèrent,
en réalité, bien plus ambivalentes. L’ouverture vers une durée infinie
ou des images doubles le rapproche, indéniablement, de chorégraphes
comme Lucinda Childs, David Gordon ou Fred Herko auquel on a voulu
l’opposer[^33]. À la différence des visions rétrospectives critiques
comme celles que peut porter Yvonne Rainer sur les années 1960[^34], il
me semble que le dialogue de Paxton, Van Imschoot et Engels invite à
reconsidérer la complexité d’un moment historique qui ne se laisse pas
saisir dans des oppositions. La vie à l’œuvre dans la danse qu’est celle
de Paxton se nourrit de contradictions à partir desquels il
chorégraphie. Au-delà des pièces des années 1960 que nous avons
commentées ici, le travail de Steve Paxton continue à développer des
espaces et des durées imaginaires qui de *Part* (1979), à *Bound*
(1982), *Night Stand* (2004) ou *Quicksand* (2019) se combinent aux
techniques qu’il a développées par la suite.
  
  
 **Lou Forster**, historien de l’art et commissaire d’exposition, mène une thèse de doctorat à l’École des hautes études en sciences sociales (EHESS) sous la direction de Béatrice Fraenkel et la co-direction de Carrie Lambert-Beatty (Harvard University). Après un master consacré à la mise en exposition de la performance, ses recherches se sont portées sur l’œuvre de Lucinda Childs dont il étudie les pratiques graphiques et chorégraphiques. En 2016, il a organisé la première exposition rétrospective de l’artiste au Centre national de la danse et à la galerie Thaddaeus Ropac dans le cadre du Festival d’Automne à Paris. Il a, également, participé à la documenta 14 entant que curatorial assistant. En 2009, il a fondé avec Lenio Kaklea, abd, une plateforme développant des projets chorégraphiques et curatoriaux à l’intersection entre danse, recherche et théorie critique. Ils sont, notamment, les auteurs d’Encyclopédie pratique, Portrait d’Aubervilliers, publié en 2018 par les Laboratoires d’Aubervilliers.

[^1]: Au côté de l’utilisation des environnements et du plastique dont
    il parle dans Paxton Steve, Van Imschoot Myriam, Engels Tom,
    *Conversation in Vermont*, Mot-Clé, “The Inflatables in the Age of
    Plastic”.

[^2]: Banes Sally, *Democracy’s Body: Judson Dance Theater, 1962-1964*, Durham, Duke University Press, 1993, p.58.

[^3]: Paxton Steve et Béar Liza, « Like the Famous Tree », in *Avalanche*, no. 11, 1975.

[^4]: Pour une description détaillée de la mise en œuvre de l’aléatoire
    par John Cage au début des années 1950 voir Pritchett James, « From
    Choice to Chance: John Cage’s Concerto for Prepared Piano », dans
    *Perspectives of New Music,* no. 1, vol. 26, 1988, p. 50‑81.

[^5]: S. Paxton et L. Béar, « Like the Famous Tree », art cit.

[^6]: Pritchett James, *The music of John Cage*, Cambridge, MIT Press, p.109.

[^7]: Voir, Cage John, *Silence: Lectures and Writings*, Middletown, Wesleyan University Press, 1961, p.35‑41.

[^8]: S. Paxton, M. Van Imschoot, T. Engels, *op. cit.,* Cluster 2, Partie 4, “Score and Indeterminacy”.

[^9]: Steve Paxton aborde cette question seulement à la fin des années 1960.

[^10]: S. Paxton, M. Van Imschoot, T. Engels, *Ibid.*

[^11]: S. Banes, *Democracy’s Body: Judson Dance Theater, 1962-1964*, *op. cit.*, p. 59.

[^12]: *Ibid.*, p. 57.

[^13]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont,
    op. cit.,* Mot-Clé, “Score”.

[^14]: Ces tabeaux sont reproduits dans Cunningham Merce et Starr
    Frances, *Changes: Notes on Choreography,* New York, Something Else
    Press, 1968, n.p. Un travail récent a été dédié aux archives Merce
    Cunningham et revient sur ces questions, voir Noland, Carrie, *Merce
    After the Arbitrary,* Chicago, University of Chicago Press, 2019.

[^15]: Le terme de littéracie désigne à la fois une connaissance
    formelle de l’écriture et de lecture (alphabétisation) et une
    culture de l’écrit et de la lecture replacé dans des contextes
    singuliers et des usages sociaux. Voir, notamment, à ce sujet,
    Fraenkel, Béatrice, et Mbodj-Pouye, Aïssatou, « Introduction. Les
    New Literacy studies, jalons historiques et perspectives
    actuelles », *Langage et société*, vol. 133, no. 3, 2010, pp. 7-24.

[^16]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont,
    op. cit.* Cluster 2, Partie 2, “Mystical Leanning, Surrealism and
    Magritte’s Puzzles.”

[^17]: Gablik Suzi et Russel John (éd.), *Pop art
    redefined*, Londres, Thames&Hudson, 1969.

[^18]: Gablik Suzi, *Magritte*, traduit par Evelyne De Knop-Kornelis, Bruxelles, Cosmos
    monographies, 1978.

[^19]: La pièce est présentée dans le cadre du Bridge Concert à Ann
    Arbor, Michigan. On en trouve une courte description dans
    *Conversations in Vermont,* Mot-Clé, “The Inflatables in the Age of
    Plastic”.

[^20]: Voir en particulier *Conversations in Vermont,* Partie 2,
    « Mystical Leanings, Surrealism and Magritte’s Puzzles ».

[^21]: Pour reprendre la fameuse phrase du Conte de Lautréamont dans
    *Les chants de Maldoror* (1869) qui sert de mot d’ordre aux
    surréalistes. Je souligne.

[^22]: S. Gablik, *Magritte*, *op. cit.*

[^23]: Tufnell Miranda et Crickmay Chris (éd.), *Body
    Space Image* *: Notes Towards Improvisation and Performance*,
    Londres, Dance Books Ltd, 1990.

[^24]: “J’ai l’impression que *Flat* est plus surréaliste que
    minimaliste. C’est cette figure qui tourne en rond avec un chapeau
    melon dans les peintures de Magritte”, Steve Paxton dans
    *Conversations in Vermont, op. cit.,*“*Mystical Leanning, Surrealism
    and Magritte’s Puzzles*”.

[^25]: M. Tufnell and C. Crickmay (ed.), *Body
    Space Image*, *op. cit*.

[^26]: S. Banes, *Democracy’s Body: Judson
    Dance Theater, 1962-1964*, *op. cit.*, p. 54.

[^27]: L’un des moments les plus marquant des *Conversations in Vermont*
    est, probablement, “La cérémonie du thé japonaise” dans laquelle
    Paxton commente l’un des aspects essentiel de sa pratique
    chorégraphique, « enquêter sur le quotidien ». Le point de départ en
    est la cérémonie du thé et les paysages de Magritte où « l’acte de
    regarder à travers une fenêtre est subverti, ou déplacé à un autre
    niveau, pour faire apparaître un aspect différent de la réalité ».
    Steve Paxton dans *Discussion in Vermont, op. cit.*,Cluster 2,
    Partie 3, “The Japanese Tea Ceremony”.

[^28]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont,
    op. cit.,* *Cluster 2,* Partie 2, « White paintings ».

[^29]: Au cours des années 1940 et 1950, une approche appliquée de la
    chronophotographie à l’étude du mouvement dansé se développe, mais
    dans un contexte tout à fait distinct de celui dans lequel travaille
    Steve Paxton. Il s’agit de la choreology de Joan et Rudolf Benesh au
    Royal Ballet de Londres.

[^30]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont,
    op. cit.,* *Cluster 2,* Partie 2, « Proxy and General Art ».

[^31]: De très nombreuses publications s’intéressent à cette attitude
    esthétique depuis les travaux fondateurs de Stearns Peter, *American
    Cool: Constructing a Twentieth-Century Emotional Style*, New York,
    New York University Books, 1994.

[^32]: Lucinda Childs citée par S. Banes,
    *Democracy’s Body: Judson Dance Theater, 1962-1964*, *op. cit.*,
    p. 98.

[^33]: Voir, en particulier, le chapitre “Allegories of the ordinary and
    particular” dans Burt, Ramsay, *The Judson
    Dance Theater: Performative Traces*, Londres New-York, Routledge,
    2006, p.88‑116.

[^34]: Voir Rainer Yvonne, *Feelings are Facts: A Life*, Cambridge, MIT Press, 2006 et Rainer Yvonne, *A Woman
    Who…: Essays, Interviews, Scripts*, Baltimore, The Hopkins
    University Press, 1999, p.27‑46.
