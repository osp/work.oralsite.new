Title: PPMS (en)
Author: Lou Forster
Date: 01-01-01
Template: essay
color: hsl(62, 82%, 85%);


One can approach Steve Paxton, Myriam Van Imschoot and Tom Engels’
*Conversations in Vermont* as a historic document produced at a double
conjunction. On the one hand, the principal object of the narrative:
modern dance’s second moment of development in the United States, which
opens at the end of the 1950s and of which Steve Paxton is a privileged
player; on the other, that of the 2000s in Western Europe, marked by a
renewed interest in the work of choreographers from the Judson Dance
Theater. In this article I do not endeavour to account for the variety
of subjects and questions that Paxton, Van Imschoot and Engels
undertake, which cover the complexity of a life at work in dance. To
read the chapter “Of Routes and Routines” took me nearly four days,
an amount of time that anyone should grant to this text, which covers,
among other, the choreographer’s formation; his transformation
(technical, social, and cultural) as a dancer in New York at the end of
the 1950s, specifically in the Merce Cunningham Company; his part in
inventing what would come to be known as “postmodern dance”, a term that
he rejected; the development of improvisation techniques for which he is
famous today; his critical positioning vis-à-vis the reception of Judson
Dance Theater and the work of the group’s other members.

My point is to focus on a question or problem, i.e. the use of
photographic images in choreographic processes, on which Paxton and Van
Imschoot’s discussion repeatedly revolves. In 1961, Steve Paxton was at
once a member of Merce Cunningham’s company and participating in Robert
Ellis Dunn’s workshop, from which Judson Dance Theater emerged. The
realising of his second choreographic piece, *Proxy* (1961), drove him
to produce what he named “people photo score” or “people photo movement
score” (PPMS), which he used subsequently for three other pieces: *Flat*
(1964), *English* (1963), and *Jag vill gärna telefonera (I would like
to make a phone call)* (1964). As such these scores constitute one of
the methods of choreographic creation that caracterise Paxton’s work in
the 1960s[^1]*.*

## A score among scores

Steve Paxton had previously commented on the invention of the first PPMS
in an interview in *Avalanche* in 1975, as had dance historian Sally
Banes. Steve Paxton, she informs us, “had made *Proxy* while on tour
with the Cunningham company in 1961. A trio, it was a ‘slowpaced dance
in four sections, with two photo movement scores for \[sections\] two
and three; instruction for \[sections\] one and four.’ The dance
involved a great deal of walking; standing in a basin full of ball
bearings; getting into poses taken from photographs; drinking a glass of
water; and eating a pear. Paxton speaks of the dance as a response to
work in Dunn’s class with John Cage’s scores”[^2]. Steve Paxton
specifies the challenges that lead him to realise this score:

> “My feeling about making movement and subjecting it to chance
> processes was that one further step was needed, which was to arrive at
> movement by chance. That final choice, of making movement, always
> bothered my logic somehow. If you had the chance process, why couldn’t
> it be chance all the way”[^3].

The realisation of PPMS anchored in one of the great aesthetic
controversies of the 1950-60s at the intersection of dance and music,
and for which the Merce Cunningham Company served as laboratory. From
1951-52, John Cage took recourse to chance operations for the
composition of his musical pieces. The various sounds played by the
instruments were organised on a chart and then thrown, heads or tails
with various coins, following the model of an ancient method of Chinese
divination, the I Ching[^4]. Used for the first time for *Concerto for
Prepared Piano* (1951), this graphic technique was subsequently adopted
by Merce Cunningham in *Sixteen Dances for Soloist and Company of Three*
(1951). For each dance, gamuts of movement, length of time and
directions in space were composed at random.

Steve Paxton claimed that PPMS’s displace the stakes of chance
operations from movement’s composition to movement itself. Photographic
representations of persons in motion offered a found vocabulary, whose
rendering in choreography became one of the creative challenges of
making *Proxy*. The piece’s title describes this dimension, as it is
choreographed “by proxy” (by procuration). Steve Paxton offers the
following description:

> “I made the score and then handed it over to the performers, and they
> could take a linear or circular path through the score. You could
> start any place where indicated, and you went back and forth as
> indicated. But how long it took and what you did in between postures
> was not set at all. It was one big area of choice not at all
> influenced by the choreographer. The only thing I did in rehearsing
> the work was to go over it with them and talk about the details of
> postures. We looked at the dance and discussed whether they’d
> accurately done the picture scores or not and worked on getting it
> more accurate. We talked about the possibilities of how to interpret
> the scores, because there’s a confusion: When you’re looking at a
> picture score you can interpret the picture, in the same way in which
> when you’re in class and the teacher sticks out the left foot, you’re
> supposed to automatically stick out your right foot. The convention
> was questioned. We went through the various points in the process to
> see what would make people feel secure. And then they gave a secure
> performance. It was relaxed and it had its own authority“[^5].

The score of *Proxy* transformed the approach to choreographic creation
by allowing Steve Paxton to introduce various factors of indeterminacy.
The performers are free to begin dancing from any photo in the score, to
circulate in a linear or circular manner through a mosaic of images, to
negotiate exposure time as well as the manner of moving from one image
to the other.oo Indeterminacy was the other principle that was the
subject of active debate for composers at the end of the 1950s, notably
Morton Feldman, Earle Brown, and Christian Wolff. As historian James
Pritchett explains, “whereas for Cage ‘chance’ refers to the use of some
sort of random procedure in the act of composing, ‘indeterminacy’, on
the other hand, refers to the ability of a piece to be performed in
substantially different ways — that is, the work exists in such a form
that the performer is given a variety of unique ways to play it”[^6]. In
this regard, indeterminacy is not unique to contemporary forms of
musical composition. In a lecture on the subject, *Indeterminacy*
(1958), Cage analyses the forms that it takes in Johann Sebastian Bach’s
*Art for Fugue* and Karlheinz Stockausen *Klavierstück XI*[^7]. *The*
*Conversations in Vermont* offers the current view of Paxton on this
question:

“I was working from \[chance operation as a\] premise to some degree.
But I was not interested in having an objective mechanism that would
make decisions so much as I was interested in creating a situation where
the performer themselves had - with whatever mechanism they had
developed in themselves - had to make choices and to operate on the
level of creation within a certain line through the dance. So, I was
opening it up not to indeterminacy, which is the Cageian word for giving
performers options in the structure, 
but…”[^8].

At this point, the choreographer hesitates. Which word could accurately
describe the relation of dancers working with his scores? If
“improvisation” is obviously anachronic, the term “indeterminacy” is too
rooted in cagean aesthetics. In between this two realms, Paxton
concludes provisionally, “I was trying to see what does this kind of
structure create”[^9]. If the experimental and pragmatic dimension of
Paxton’s work with the score should be acknowledged, a more general
explanation seems to lie in a theory of image which guided Steve Paxton
in exploring the relation of poses and their being set into motion.

Before tackling this issue and abandoning the cagean terminology, one
can still wonder in which respect chance operations constituted the
“premise” of the PPMS. Differently formulated, could *Proxy* and the
subsequent pieces be considered *at once* indeterminate *and* based on
chance operations? If, as we have just seen, the realm of
“indeterminacy” organises the relation of the performers with the score,
neither the choice of photographs nor their graphic organisation on the
page have been commentedd yet. To clear doubt on this point, Sally Banes
cites an interview that she conducted in 1980 with Robert Ellis Dunn,
where he explains that, “Paxton cut the images out, dropped them on
various places, let them float down, then glued them in place. One of
them was a photo of a baseball player sliding into base. Those stop
photos are very beautiful "[^10]. Banes concludes that the method
undertaken to realise the score " was close to Marcel Duchamp’ work,
rather than Cage’s "[^11]. This testimony and Banes’s conclusion exclude
chance operations in part but prove to be questionable nevertheless by
their indirect and vague characters.

Consulting today the only conserved PPMS, *Jag vill
gärna telefonera (I Would Like to Make a Phone Call)* (1964), one notes
at the contrary that the organisation of photographs on the page obeys
both an iconographic and gestural organisation. Composed of three sheets
of paper of approximatively 40 x 60 cm, the poses of its upper level
gather jumps. The second offers multiple variations of walks (at upper
left), running (centre), falling (at right). Finally, the third panel
explores different relational forms from contiguity to contact. Coloured
stickers and arrows indicate various directions for reading and draw
relatively coherent itineraries as when, for example, one views the
different photos on the right side from top to bottom. Jump, fall,
grasp, and lift follow one another and offer a sort of
chronophotographic cutting of the same movement. Therefore, it seems
unlikely that this score would be composed by chance operations. If
suspending judgement strategies inspired by Marcel Duchamp were
employed, it would demand, at least, to be specified. Steve Paxton in
his discussion with Myriam Van Imschoot gives an important clue to this
question:

“ Making scores \[was an attempt\] to just find a way to think about
movement. I mean, I’m proposing on the stage definitely a dance, but I’m
proposing in my mind’s eye as I’m creating the score all kinds of my own
transitions between one thing and another and it’s like a mental dancing
to work with the photos. One gets very intimate with the photos ”[^12].

Paxton’s testimony radically diverges from that of
Robert Ellis Dunn and the conclusion reached by Banes. A closer look at
the charts developed by Merce Cunningham in order to subject movement to
chance give a hinge into this complex question[^13]. As Paxton,
Cunningham made scores in order to “think about movement” and not only
to compose. Specific charts gathered gamut of movements such as jumps,
leaps or falls. The recourse to intellectual technologies of movement
cataloguing, such as a grid, was as important as tossing coins at the
end in order to choose at random the succession of movement. In a
similar fashion the PPMS of *Jag vill gärna telefonera (I Would Like to
Make a Phone Call)* collects and organises the gamut of movement
provided by sport magazines. Abandoning the “objective mechanism”,
Paxton kept from chance operations the recourse to a visual literacy
which allowed him to develop his kinesthetic imagination.

## Double image

“Myriam Van Imschoot:How would you have access to Magritte?
Through books? Steve Paxton: Books and posters at first, I think. Then, I found out
that Jasper and Rauschenberg were interested in him and Cage […] Anyway, and then Suzi, whom I also knew…”[^14]

Born in New York in 1934, Suzi Gablik is an art historian and artist.
She studied at Black Mountain College then at Hunter College with Robert
Motherwell at the beginning of the 1950s. In 1969, she co-directed the
important catalogue *Pop Art Redefined* with John Russel*.*[^15] At the
end of the 1970s, she concluded her artistic practice and dedicated
herself entirely to art criticism. She became a columnist for *Art in
America* in London. She is additionally author of a major reference text
on René Magritte who hosted her occasionally over the 1960s when she was
completing her monograph[^16]. A less known aspect of her trajectory is
her friendship with Steve Paxton and her participation in a piece of the
choreographer’s *Ann Arbor Deposit* in 1966.[^17] The *Conversations in
Vermont* illuminate the importance that the art critic played in
Paxton’s reflections on image, and in his regular frequenting in the
1960s of paintings by Magritte or of their reproductions.[^18]

In her book on Magritte, the art critic insists on the importance of
what she terms “double images.” Over the 1950s, the Belgian artist
retreated from the surrealist approach towards found objects, which had
proceeded from “the *chance* *encounter* of a sewing machine and an
umbrella on a dissecting table,”[^19] to explore the “hidden affinities
between objects, \[such as\] the relation of shoe to foot, of landscape
to painting, or of a woman’s face to a woman’s body. \[…\] A singular
conceptual image \[then\] expresses the synthesis of two or more
conceptual images only if it is born at the intersection of a
paradox.”[^20] To identify these “intersections,” Magritte developed a
practice of doodling that reflects on this duplication of images. More
punctually, he used photography to arrange compositions in which he,
occasionally, posed. In his descriptions of the PPMS, Steve Paxton
insists on the succession of images since it was essential to studio
work. The instructions given to the performers consisted of passing from
one image to the other by means of movement. It can be additionally
proposed that one of the functions of these scores is to produce double
images, as is the case in *Flat* (1964). For this solo, Steve Paxton
wears a suit. Seated amongst the audience, he stands up and enters the
stage with a chair in his hand. Then, the performer makes circles around
the chair and poses repetitively, each time, removing an article of
clothing. Ultimately stripped to his undergarments, he methodically
dresses again following the same principle. During the poses, the
clothes are hung on hooks which he has taped directly on his left arm
and on the top of his back, and he activates the photos of the PPMS.
While certain photographs are performed in a quotidian manner and with a
demonstrative slowness — removing his socks for example — others become
an object of pantomime or of virtuosic and rapid movements according to
the register of the image: going to pee, looking over a wall, fishing,
embracing the space with his arms as a classical statue. In 1990, Steve
Paxton describes *Flat* in the following manner:

> “ A series of small surprises — the suit, the undressing, the pauses,
> and the hooks on the body. The ironic displacements of the familiar
> are reminiscent of a Magritte painting.”[^21]

It seems then that this solo activates images in two distinct ways. The
first is the succession of poses following the PPMS, the other is the
production of a double image in which Paxton becomes, at once a
masculine figure à la Magritte (which merely lacks a bowler hat)[^22]
and a coat rack. One can hypothesise that the painting of reference is
*Un peu de l’âme des bandits (A little of the outlaw’s souls*) (1960),
in which a violinist’s face is replaced by his instrument. In 1960 the
preparatory drawings for this painting, whose original title is *Les
lettres persanes* (*The Persian Letters*), enter into the collection of
Harry Torczyner, a significant New York collector that Suzi Gablik most
certainly had to have known. In the drawings one sees, notably, the
violin hanging on the violinist’s back (sheet III and IV) where Paxton
would place one of his own hooks. One can additionally relate the thin
and almost hesitant line of Magritte’s doodles with Paxton’s when in
1990 the choreographer represented the intersection of this two images
on a sketch for the catalogue *Body Space Image: Notes Towards
Improvisation and Performance.*[^23]

**Succession and Seriality**

Suzi Gablik’s book invites reevaluation of a last commonplace in Steve
Paxton’s use of images in his works. Dance historians and the
choreographer himself insist on the pertinence of Edward Muybridge’s
chronophotographic studies for considering the relation of photography
to movement in the 1960s. One notes this influence in the score of *Jag
vill gärna telefonera (I Would Like to Make a Phone Call)*, where the
succession of shots is organised in such a way that they invite a linear
reading. From the action of jumping succeeds fall, rising, etc. However,
the poses belong to different series (athleticism, football, boxing).
Steve Paxton reconstructs a succession of heterogeneous provenances. The
series is not the result of an organic action, as in the
chronophotographs, but in a montage that creates the appearance of
continuity.

In the third section of *Proxy*, Steve Paxton places a muybridgian
device directly on stage: a black curtain around which the dancers turn,
appearing and disappearing before spectators' eyes at seven intervals.
Sally Banes and Carrie Lambert-Beatty[^24] insist that in the early
1960s Steve Paxton was interested in the manners by which technology
shapes our perceptions. By placing this device on stage, he would
explore what Walter Benjamin calls “the optical unconscious”— the
worldly dimensions revealed by cinema or photography when a close-up or
a slow motion gives us access to phenomena invisible to the naked eye.
The use of poses would operate then “almost as if the choreographers
wished to appropriate the filmmaker’s ability to slow down a film and
watch it frame by frame.”[^25] It strikes me that one can doubt this
section of *Proxy* as aiming to literally reconstitute Muybridge’s
experiment in order to place spectators in the position of analysing
movement. Instead, the recourse to slow-motion and frame-by-frame
appears as a portal to a dimension of movement genuinely inconscient
with Benjamin’s expression. In this respect, Paxton’s movement approach
shows affinities with the magritian approach to the non-narrative
representation of the oridinary[^26].

Within René Magritte’s pictorial inventory, there are several paintings
representing multiple images, as in *L’homme au journal* (*Man with a
Newspaper*) (1927). A room is figured at four reprises, but effectively
in only one of them is a man seated reading a newspaper. This
compartmentalised image introduces a banal story line but the absence of
causal links between each picture cannot render the succession. So,
exiting the narrative structure, turns the painting into an enigma. It
seems compelling to think that in the third section of *Proxy*, Steve
Paxton explores this magritian gap between seriality and succession. The
accumulation of poses performed in slow motion not only confront
spectators with their optical unconsciousness but also with the
impossibility of reconstructing a narrative from discontinuous
appearances. That they appear seven times also has also a specific
purpose: like the *White Paintings* of Robert Rauchenberg, it bears on
the “\[smallest\] number you need \[…\] to suggest infinity.”[^27] With
this section we enter in a world that is simultaneously quotidian — the
register of the images — and uncanny and where time and causality no
longer govern succession.

This part of the piece maintains a paradoxical relation with the tasks,
eating a pear and drinking water, performed previously. While the tasks
are distinguished by their differing durations in time, they
nevertheless share a common subject (the everyday) and are staged in the
same, consistent space, marked out on the floor by yellow tape. In this
piece, tasks are likely not meant to produce an effect of reality,
instead their absence of peripeteia are reminiscent of Magritte’s
paintings. They produce an equal feeling of strangeness which one could
compare to the first films of Andy Warhol. In *Sleep* (1964) the poet
John Giorno, Warhol’s lover, is filmed during five hours and 20 minutes
sleeping. Extending a task beyond the duration of its usual
representation exposes the fundamental eeriness of a real, shared
intimacy. In a word, more than a modernist investigation into
biomecanics for which Muybridge’s experiments would stand as model[^28],
*Proxy* troubles with new choreographic tools (tasks and PPMS) the norms
of representation. Described as a series of “revelations”[^29], *Proxy*
give access, through ellipses or durations, to the choreographer’s
reality which was at likely unpresantable at the time, and indeed
camp.

I have never found this dimension of Steve Paxton’s work rendered as
explicit as in these *Conversations in Vermont*. The PPMS expose
problems of theoretical and intimate bearing, such as the real duration
of tasks and the narrative time of images, and their jostling.
Receptions of the choreographer’s work have insisted on its cool[^30] or
“jesuitical”[^31] manner, though it proves to be much more ambivalent.
This openness towards an infinite duration or to double images
unmistakingly associates him with such choreographers as Lucinda Childs,
David Gordon, or Fred Herko, against whom he has been previously
opposed.[^32] Unlike visions of critical retrospection such as those
that Yvonne Rainer bears on the 1960s,[^33] it seems to me that the
dialogue between Paxton and Van Imschoot invites us to consider the
complexity of a historical moment which cannot be caught up in
oppositions. Paxton's life at work in dance is fueled by contradictions
from which he choreographs. Beyond these pieces from the 1960s that
occupy our focus here, Steve Paxton's later work continues to develop
imaginary spaces and durations which, from *Part* (1979), to *Bound*
(1982), *Night Stand* (2004) or *Quicksand* (2019), combine with dance
techniques he subsequently developed.

*Translated from the French by Macklin Kowal*

**Lou Forster** (FR) holds a bachelor’s degree in Philosophy and Theater Studies and a master from the School for Advanced Studies in the Social Sciences (EHESS). In 2017, he started a PhD at the EHESS on the choreographic and graphic work of Lucinda Childs and is doctoral fellow of the National Institute of Art History (INHA). Since 2010 he works as an art critic and writes, among others, for A prior and Le journal des Laboratoires d’Aubervilliers and, from 2011 to 2013, he co-directed the art journal Art21. He published a number of articles on Walid Raad, Yvonne Rainer, Franck Leibovici, Claudia Triozzi, Juan Dominguez, Rabih Mroué, L’Encyclopédie de la Parole and others. In Fall 2016, he curated Lucinda Childs, Nothing personal (1963-1989), the first retrospective dedicated to the work of the American choreographer at the National Center for Dance (CND) and Thaddaeus Ropac Gallery, Pantin during the Festival d’Automne à Paris. Since 2010 he collaborates with Lenio Kaklea developing choreographic and curatorial projects that explore the intersection of dance, research and critical theory.

[^1]: Next to the use of environments and plastic commented in S. Paxton, M. Van Imschoot, T. Engels, *Conversation in Vermont*,
    Keyword, “The Inflatables in the Age of Plastic”.

[^2]: BanesSally, *Democracy’s Body: Judson Dance Theater, 1962-1964*, Durham, Duke
    University Press, 1993, p.58.

[^3]: Paxton Steve and Béar Liza, “Like the
    Famous Tree,” in *Avalanche*, n.11, 1975.

[^4]: For a detailed description of the implementing of chance
    operations by John Cage in the early 1950s, see Pritchett James, “From
    Choice to Chance: John Cage’s Concerto for Prepared Piano", in
    *Perspectives of New Music,* n.1, vol.26, 1988, p. 50-81.

[^5]: S. Paxton and L. Béar, “Like the Famous Tree”, art cit.

[^6]: Pritchett James,
    *The Music of John Cage*, Cambridge, MIT Press, p.109.

[^7]: See Cage  John,
    “Indeterminacy,” in *Silence: Lectures and Writings*, Middletown,
    Wesleyan University Press, 1961, p.35-41.

[^8]: S. Paxton, M. Van Imschoot, T. Engels, *op. cit.* Cluster 2, Part
    4, “Score and Indeterminacy”.

[^9]: S. Paxton, M. Van Imschoot, T. Engels, *Ibid.*

[^10]: S. Banes, *Democracy’s Body: Judson Dance Theater, 1962-1964*,
    *op. cit.*, p.59.

[^11]: *Ibid.*, p.59.

[^12]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont
    op. cit.,* Keyword, “Score”.

[^13]: The charts have been reproduced in
     Cunningham Merce and Starr Frances, *Changes: Notes on
    Choreography,* New York: Something Else Press, 1968, n.p. For a
    recent investigation into Merce Cunningham’s archives see Nolan,
    Carrie, *Merce After the Arbitrary,* Chicago: University of Chicago
    Press, 2019.

[^14]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont,
    op.cit.,* Cluster 2, Part 2, “Mystical Leanning, Surrealism and
    Magritte’s Puzzles.”

[^15]: Gablik Suzi and Russel John (ed.), *Pop Art Redefined*, Londres, Thames&Hudson, 1969.

[^16]: Gablik Suzi, *Magritte*, New York: New York Graphic Society, 1976.

[^17]: The piece was presented in the frame of the Bridge Concert at Ann
    Arbor, Michigan. For a short description of the piece see,
    *Conversations in Vermont,* Keywords, “The Inflatables in the Age of
    Plastic”.

[^18]: See in particular *Conversations in Vermont, op. cit.,*“Mystical
    Leanning, Surrealism and Magritte’s Puzzles.”

[^19]: As to make use of the famous line of the Count de Lautréamont in
    *Les chants de Maldoror* (1869) which served as a watchword for the
    Surrealists. My emphasis.

[^20]: S. Gablik, *Magritte*, *op. cit.*

[^21]: Tufnell Miranda and Crickmay Chris (ed.),
    *Body Space Image*, Londres, Dance Books Ltd, 1990.

[^22]: “I feel like *Flat* is more a surreal piece than it is a minimal
    piece. It's that figure that walks around in a bowler hat in
    Magritte paintings”, Steve Paxton dans *Conversations in Vermont,
    op. cit.,*“Mystical Leanning, Surrealism and Magritte’s Puzzles.”

[^23]: M. Tufnell and C. Crickmay (ed.), *Body Space Image*, *op. cit*.

[^24]: S. Banes, *Democracy’s Body: Judson Dance Theater, 1962-1964*,
    *op. cit.*, p. 54 and Lambert-Beatty Carrie,
    *Being Watched: Yvonne Rainer and the 1960’s*, Boston, MIT Press,
    2008, p.60-63.

[^25]: S. Banes, *Democracy’s Body: Judson Dance Theater, 1962-1964*,
    *op. cit.*, p. 54.

[^26]: One of the most striking moment of the *Conversations in Vermont*
    is the so called “Japanese Tea Ceremony” where Paxton discuss a
    foundational aspect of his dance practice, “inquiring into the
    routine”. The point of departure is the tea ceremony and Magritte’s
    landscapes painting where “the act of looking out a window is
    subverted, or taken to a different pitch, or you’re shown a
    different level of reality”, in. Steve Paxton in *Conversations in
    Vermont, op. cit.*, Cluster 2, Part 3, “The Japanese Tea Ceremony”.

[^27]: *Conversations in Vermont,* Cluster 2, Part 2, “White Paintings”.

[^28]: During the 1940’s and 1950’s, chronophotography was applied to
    movement analysis in a dance context in a very different
    perspective by Joan et Rudolf Benesh at the Royal Ballet in
    London.

[^29]: S. Paxton, M. Van Imschoot, T. Engels, *Conversations in Vermont
    op. cit.,* Cluster 2, Part 4, “Proxy”.

[^30]: *Conversations in Vermont,* Cluster 2, Part 2,“The flatness of
    flat". Numerous publications take interest in this aesthetic
    attitude, ever since the groundbreaking *American Cool: Constructing
    a Twentieth-Century Emotional Style.* Stearns Peter, New York, New
    York University Books, 1994.

[^31]: Lucinda Childs cited by. Banes, *Democracy’s Body: Judson Dance
    Theater, 1962-1964*, *op. cit.*, p.98.

[^32]: See in particular the chapter “Allegories of the ordinary and
    particular” in Burt Ramsay, *The Judson Dance Theater: Performative Traces*,
    Londres New-York, Routledge, 2006, p.88-116.

[^33]: See in particular Rainer Yvonne, *Feelings are
    Facts: A Life*, Cambridge, MIT Press, 2006 et Rainer Yvonne, *A Woman
    Who…: Essays, Interviews, Scripts*, Baltimore, The Hopkins
    University Press, 1999, p.27-46.
