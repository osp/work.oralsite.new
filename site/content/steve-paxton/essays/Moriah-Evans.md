Title: “Everybody's enacting their role in a moment.”
Author: Moriah Evans
Date: 01-01-01
Template: essay
color: hsl(342, 23%, 93%);
order: 1

Myriam: “Do you have the feeling of competing with your past?” Steve:
“Yes.”

History-making procedures of canonization and acclaim churn some people
into signs. Steve Paxton’s multi-decade practice is packed with
shareable knowledge-production. He advocated for individuals to dance
themselves, to find the small dance within, without choreography. He
gave the world Contact Improvisation, and he made it open source. He did
not create the Steve Paxton Dance Company.” But what does one really do
when they name something? “I went on to name Contact Improvisation. And
I think it was with the consciousness of this problem that I didn’t want
it to get stuck in whether it was dance or whether it was, you know,
what it was. I decided well, give it a name, and then, they can struggle
with the definition of what that name is.”

How do I, as an artist, reflect on “Steve Paxton”? How do I “struggle
with the definition of what *that name* \[emphasis added\] is”? Several
generations apart yet definitely in a lineage—I have absorbed the legacy
of Steve Paxton, inadvertently, through history. The aesthetic impact of
Judson Dance Theater, Grand Union, Contact Improvisation, Small Dance,
Goldberg Variations, and Material for the Spine is identifiable and
simultaneously invisible through its very absorption. I do not know
Steve Paxton. I have never met him, but I have experienced his work
live, recorded, re-made, and even approached as a readymade by other
artists. I have listened to him speak. I have heard others speak about
him. His voice—a disembodied body, mechanically reproduced on this
website—is present as I write and rewrite this text. There is an
intimacy to listening to the private conversations Steve had with Myriam
and Tom. Who do we have the occasion to listen to and what does that
listening do? Paxton was not one of the Judson luminaries on whom I
intellectually/artistically obsessed; I can only guess why—he wasn’t one
of the female art matriarchs emerging from the Judson moment. Due to my
investment in feminist discourse, I obsessed over Deborah Hay, Trisha
Brown, Yvonne Rainer, Simone Forti, and Lucinda Childs. Yet, when I
really consider my values and assumptions about what dance is and can
be, it is clear they have been informed by the same principles to which
Paxton ascribes, and to which he has committed so much of his brave
life. Maybe I didn’t obsess over him because he didn’t really proclaim
nor position himself as a person to obsess over—he is never master,
expert—rather he works to rupture such notions in favor of the
potentiality within the autonomy of the dancer and the dialogic.

Alongside these interviews, there is a chance to speculate how the
politics of Steve’s practice have held up over the past fifty years.
Maybe there are clues for figuring out another role for art and dance in
relation to the social field. Maybe in all this retelling, conversing
and listening, there is a chance not to repeat but to transform: to go
deeper, elsewhere and otherwise. Visiting this oral archive of Steve
Paxton in conversation is a series of rehearsals for the future. Steve
spent a decade just walking, again and again, to understand it. We can
also observe this in his endless doing of the *Goldberg Variations*,
again and again. On this website, we can listen to Steve rehearse his
ideas, as arrived at habits of forms of thought, in an effort to do and
be otherwise throughout the decades and throughout every temporal
fragment of every instance, insisting on a radical commitment to
presence.

I was supposed to write an essay, but there is not really an argument
here. Instead, this is a collection of impressions, a palimpsest of
notes and thoughts. To emphasize the dialogic, I insert a plethora of
quotes from Paxton as the “material for the spine” of this
text—something of a “combine” of my reflections interweaved with
excerpts from this extensive oral history. Steve, Myriam, and Tom—their
talking produces this text as I listen; is this a third space, an
emergent “small dance” between me and this archive? Talking, reflecting,
stuttering, stammering, writing: a set of multidirectional
interrelationships—starting from no fixed point, forever in motion and
in relation.

\*\*\*

Members of The Judson Dance Theater have debated many of the labels
ascribed to them and their work—particularly the words “neutral” and
“democratic”—“we were not allowed to title ourselves… Sally Banes’s
title has stuck to us, and it’s not terribly accurate, you know. I don’t
think,” says Steve. The aftermath of Judson Dance Theater and Grand
Union have been examined and absorbed by various artists as well as art
and dance establishments all over the globe. Amongst artists, there is
an ongoing tension between being informed by the aesthetic revolution of
the Judson era, and the rejection and transformation of the era’s
aesthetics and values. Does the Judson artists’ whiteness (exception:
Rudy Perez) have something to do with these descriptions of the
“neutral” and “democratic” body getting stuck on/with them or is this
adhesion more about their use of quotidian vocabulary—standing, sitting,
running, walking—archetypical human actions that claim perceiving one’s
own body is already a dance? Assumed correlations between race and dance
aesthetics have been refuted, deconstructed, reconstructed, and
fabulated otherwise in the New York scene and (inter)national dance
scene for decades, and certainly with increasing vigilance and urgency
in the era of Trump politics. Many artists repeatedly rethink and
re-inscribe the notion of “neutrality” and the “democratic body” in
dance aesthetics, practices, and languages; today such notions seem
increasingly impossible to fathom.

“Mr. Neutral” is a nickname Steve calls himself. This myth of the
neutral emerges *again*, returning like an element with a half-life, but
this time employed by Steve himself. Is there a reason for the rational,
almost scientific attitude? His projects tend to have an object of
inquiry and concern for something's functionality: “dance became a
testing ground, a laboratory,” and as such, dance might function as mind
training. Existence is already an improvised dance; thinking is also a
dance. Steve’s work configures a situation of thought; it produces
reflection, not representation of reflection, but a situation that
generates reflection—reflections concerning dance in general, and
arguably, expanded life: what it means to be human, have a body, relate,
connect, contact, interact, feel, be conscious or unconscious. There is
no singular sentence to describe his work, no statement for “this is
what Steve Paxton is." For instance, *Material for the Spine* is quite
dry, but this dryness is also a way to ensure that entry to the work is
not contaminated by metaphor, reference, empathy, or emotion. Instead of
telling the dancer, the spectator, or the other what to feel, he
proposes that they produce their own response (physical, social,
emotional or otherwise) in relation to the situation. His notion of
dance/dancing becomes political—as potentiality. Rather than being the
representation of an already established perspective, potentiality
starts where possibility ends: what is beyond the thinkable?

\*\*\*

It’s ironic that during my musing about the “inventor” of Contact
Improvisation, a global pandemic transpires in which no contact is
allowed. Our interdependent global society is forbidden to gather in
person—no touching—in order to mitigate a public health crisis. During
this state of suspension, I have experienced my dancing and thinking
body floating around in an abyss of uncertainty and absence—in
isolation, removed from literal physical relation to my extended
community in the dance studio, the theater, the street. Yes, there is
the virtual online space, but that zone is even more of a void. This
unknown context for what I am doing with my body-thinking has been
informative: there is only the abyss inside of my body, the
unknowingness of this infinite materiality composing the sensing self in
constellation to the stuff of the outside world. I listen to what Steve
says and does throughout these conversations—lighting cigarettes,
eating, sneezing—and I envy conversing with another person: the time
spent, the exchanging, the ruminating, the emergent structuring of
ideas. Clear, well-structured expediency is often demanded of written
texts. And I yes, oh, I struggle to argue anything about what art could
even be during a pandemic’s seconds, minutes, hours, days, weeks, months
of uncertain times. Risking a reductive formulation, I wonder about a
possible parallel: between the differences in labor, focus and effort
required by improvised dancing and choreographed dancing, and the
necessary labor, focus and effort of spontaneous conversing and
structured writing? One action happens now and in relation: the other in
the past for the future. I find myself in yet another rehearsal for a
search for new models, right now during a pandemic, listening to Steve
Paxton. Can the idea of the small dance offer up a third space so that
the line between improvisation and choreographed dancing be more nuanced
and emergent? And so that, yes you, dear reader, can perceive both a
structured argument and an improvised, momentary play as you read these
words?

In this suspended and uncertain time, when society—and certainly the art
form of dance— stands on a precipice: is there any necessity for
choreographed dancing, if it’s all going to be improvised regardless? If
improvised dancing is arrived at through rigor, agency, autonomy and
deep principles of artistic work, what is the use of written
choreography? The distinction between anarchic, emergent structures and
techniques that are more position-based and bound is of course also a
political question in addition to an aesthetic, formal one: how are
people enabled to do things with their bodies? In speaking about dance
technique, Steve explains “\[t\]here are arrived at habits, but we can
do other things.” Paxton criticizes the brainwashing necessary in most
dance techniques. Yet, he acknowledges the pleasure he took in
conforming to such models:

> Dance technique, and subsequent choreographic use of those dancers who
> have been technically trained is a kind of brainwashing of the dancer
> into being able to accept motivation from another person for their
> movement, for the use of their body. …Basic training kind of thing
> takes about six weeks to eight weeks to train people to \[…\] go from
> being students and accountants and storekeepers into killing machines
> \[…\] Brainwashing is not unusual, but it was unusual to think or to
> realize that I had been through such a process quite willingly. I
> enjoyed the whole thing of giving my body over to somebody that I
> thought was a great choreographer, but then realizing that it makes
> certain things happen, like, I didn't know what I danced like.

Despite Paxton’s critique of dance in relation to militaristic,
absolutist methods, these interviews reveal his genuine desire to
“invent” a new form of dance. He simultaneously adores and criticizes
his modernist predecessors:

> The real work that they had passed on was that you could create a
> movement style or technique, a new process, a new way of looking at
> the body, and that that was the, finally, the most precious gift that
> they had given. The repertory was proof and presentation of that
> movement work, but that it was important to create a vocabulary. It
> was important…like writers, you know, to find a voice within the
> body… So, it's a long processing of the question, what…how do I
> dance?

Thanks to Steve and the legacy of Judson, it’s important to note that
dance training and notions of power in dance have shifted a lot since
the time Paxton was a student. This military notion of basic training as
brainwashing is somewhat outdated as a strict formulation. Technical
training does not have to be disciplinary and abusive. Does virtuosic
dance training necessarily numb dancers and make us unable to take
decisions for ourselves? Does subordination feel good with consent? The
complexities of these power dynamics are plastered all over the field of
dance with an array of experiences. If dancers are trained in these
techniques, do we become non-subjects? Dancers use the knowledge of
“ballet” or “release” or “Cunningham” or “Graham” or whatever. Maybe
they do not necessarily master it, as in the perfection of a step. And
the idea of a standardized dance company that does morning class
together? Well maybe that is actually refreshing in the neoliberal
pandemic moment.

Throughout the decades, Paxton stays preoccupied with fundamental
questions—how to dance and also what is a dance? Steve thinks of himself
as “an example of somebody who could not be supported because my field
had not yet been invented, because I was of the sort that was going to
invent my own field.” Because of what is and had been felt through his
goals with dance, He needed to invent that which had not been invented
(or not yet named). Whether in the years of Judson Dance Theater and
Grand Union, Contact Improvisation, Small Dance, Goldberg Variations or
Material for the Spine, Steve’s practice leads with questions. “If
thinking is too slow, is an open state of mind useful?” “How does
gravity feel?” “What happens when bodies are thrown into the air?” “What
happens to the body when it is kept still?” “What tiny movements
constitute dance or performance?” “What happens when movement is left
undetermined?” “How does one bring unconsciousness to consciousness?”
Questions and curiosities have undergirded all his explorations, and
this is part of the legacy of experimental dance that much of
“contemporary dance” continues to be formed through today. Dance as a
research-based, speculative practice. What is dance? What can dance be?
What is a dance? What is a dancer? What is the difference between
choreography and dancing? How to dance?

A lot of these questions of exploration start with “what is,” which is
an essentializing question. What is useful about: What is dance? What
does it mean to have a body? Do such questions generate or consider a
politics? Is questioning politics? Such questions can suggest certain
mysticism, where only the elevated artist knows. I vividly recall asking
Deborah Hay questions about the very questions she uses as prompts for
perception and movement; she would answer one question with another,
look at me with what felt like a degrading gaze or deliver some
riddle—the questioner is made stupid, needing to reflect for years on
the implicit paradox. I consider the generational difference in the use
of the question as a prompt for action in dance and in art. Before 1970,
asking open questions could be productive and even political because
difference had value in a post WWII, deeply homogenized USA society.
People needed to be opened. Today Wall Street uses bodies without
organs; war strategists think through networks and indetermination.
Today, to ask an open question can seem a way of not standing for
anything. If we take Steve’s principles as a rehearsal for the future,
what are ways to ask these various questions about dance and
choreography so that the answers do not ever have the chance to become
one, static, understandable, but rather become a form of drama,
movement, transformation?

\*\*\*

Steve’s *Small Dance* grew out of the explosive dance *Magnesium*. Steve
would stand still for five minutes and in standing still, a small dance
emerges:

> But at the other extreme is what is the tiniest sensation that you can
> find in the body? What are the smallest elements of movement? So that
> the glamour of high activity, you know, which always gets everybody's
> attention is contrasted with an underlying minutia of sensation. And
> so you always have both things going on, and the minutia is not
> ignored. And the minutia is in every gesture of a hand, or twist of an
> arm, or a thrust, a little, little thrust of a leg, or giving way to
> weight, or all of these potential changes, as a kind of…well it's an
> orchestration, from the quietest to the loudest parts of a symphony,
> like how to keep the quietest part of the vocabulary, the most subtle,
> the most basic elements of time and energy.

With Paxton’s notion of the small dance, the present is a moment of
incessant eventfulness. By changing and/or bringing attention to the
notion of what might constitute an event in dance, Paxton also slowly
critiques the apparatus of the stage and the theater and the regimes of
what is interesting to look at, what is boring, what can be seen by
others versus what one perceives and senses within themselves. The pure
basic minutia of sensation, of being alive, is a sequence of incessant
events. Steve says: “I can theorize that watching someone stand still is
interesting… Is boredom good to provoke? Do we need to get bored?
Boredom is usually something we run from.” Steve asks us to attend to
and utilize what might be perceived as boring from an exterior vantage
point but something else entirely when understood from within. This
reorientation of what constitutes a dance event is not something “words”
can describe—what one feels—but Steve adheres to and shares this
knowledge.

Paxton advocates somatic exploration as a way of being in life, of
advocating for another way of perceiving, another way of living, and
most certainly another way of dancing. How is living a practice, and how
is improvising an ongoing practice for life? What Steve made was more
for life than for the theater: “Where I had gone wrong was to put my
work in the theater at all…” *Satisfyin’ Lover* aims to present natural
walking without comment. How is it possible to walk naturally,
purposefully and without affect, in a theater, while people look at you?
I find it worth repeating: “Where I had gone wrong was to put my work in
the theater at all…” The dance form that Steve crafted belongs inside
the body (with a performer who is not contaminated by a director); it
belongs to consciousness.

“Feeling the other person’s small dance…if you’re following each other’s
small dance, the third thing will arise.”

Steve advocated for another way that the body might adhere to a social
contract. “I mean, if we had open minds enough to try to work with new
structures and try to understand how new structures would impact on the
dance forms, why wouldn't we have been interested in all the new…all
the possible structures?” Mad Brook Farm in Vermont offered another way
to be in intensive investigation about the body and life—a way that one
could probably not do in NYC. “A modern city, you know, the fact that it
doesn't know what to do with its sewage, and its waste…is just such a
generator of pollution…I felt like being here was a way of avoiding the
worst of humanity's efforts to civilize itself.” At Mad Brook Farm,
there are short growing seasons and harsh winters. There must be a real
commitment to be there. What happens in this gestation period? Being
situated in nature, rather than in a city, informs how the body adjusts
to place, to site, to context. In leaving New York, and the
interrelationships of the dance ecology there, what type of “stepping
outside of invented constructs” was Steve actually doing as an artist?
This narrative moment in the life and career of an artist, when an
artist leaves the city that enabled them to create their name as value.
If an artist leaves the contextual machine that made their name
knowable, what type of participation in the scene (and the machine of
capital) do they have to continually re-inscribe in order to ensure
their relevance when they are no longer inside of an artistic ecology?

“And I would argue that it's the body, and the mechanics of the body is
how you're approaching ecology, is that we're of the body. Dance pursues
that.”

And yet, despite this humility required to submit to the materiality of
the body and to really pursue ecology through the mechanics of being a
body, having a body, dealing with one’s body in the world today—we still
recognize artistic contributions through certain expansionist,
extractionist modules: “there's always been an appetite for enlarging,
but there's also been this appetite for the marginal, you know, the
avant-garde arts.“ How many people know the work? Is it acclaimed? What
type of institutional pedigree is upholding it? Whether or not Paxton’s
ethical and political positions in the world could enable him to stake
modernistic claims as an “inventor,” a knowable choreographer is a point
of tension in the oral history on this site. I’ve listened to it all and
hear plenty of tension around this point within Steve’s talking and
thinking.

\*\*\*

“Because what artists do for each other primarily is permissions.”

The whole Judson era project is incredibly important and absolutely
foundational to what I do now as a dance artist. They ruptured
modernism’s forms and techniques with principles of the everyday body.
They experimented with alternative, often nonhierarchical, modes of
organizing. They made an improvised dance company—which “was not at all
a possible pursuit” but that became a “new reality included in the
premises of what dance and choreography could be.” They associated
improvising and playing as pursuits in which one invents one’s own
reality as a natural human ability—to control one’s perceptions in
relation to one’s environment. But I do wonder, through Steve’s words,
about: “…the keys to leaving the structure as the structure has
conformed itself…”

I want to think through this glorification of improvisation in the
lineage of American postmodern dance as well as in my writing here. The
kinds of freedom that improvisation addresses are also forms of freedom
that have authorized extractive capitalism, private property,
individualized subjects, and have deposited us inside a violent pandemic
emphasizing the injustice of oppressive social structures or a lack of
social protections. Is improvisation, especially from the perspective of
Trump America, good? Improvisation has been a liberating rupture for
dance and art, freeing and democratic. But, today, control and covert
forms of power are used in dance and life in stealth and abusive ways.
In current neo-liberal capitalism, improvisation is a primary mode of
organizing life, labor, and even social relations. Trump appears only to
improvise—a reality TV star playing president to disastrous
consequences, he who is self assuredly not responsible for anything.
Capitalism devoid of structure gives the one with the most money or the
most force the most power. When there is structure, or choreography, we
can question this structure on a fundamental level and navigate it
through alternative strategies. In 2020, improvisation is not about
keeping an open mind; improvisation is about being hyper-dynamic. The
populous can manage daily life while having four jobs, a family, no
money, and also still try to develop an entrepreneurial project—the
American way, the American dream. Neoliberal capitalism requires
improvisation so that individuals are always able to react to any
changes in work, economy, distribution of resources, etc. Nothing and no
one is held responsible in this version of late capitalism—aside from
the individual worker. An individual is always on their own and always
responsible for their decisions; decisions which are in fact not their
own decisions, but always in relation to financial markets—even while
the individual is conditioned to believe and feel otherwise. Do some of
the values in improvisation replicate free markets? There should be as
little choreographic action or intervention as possible. The government
provides roads, public transport systems, water, some parks, etc., but
it should stay away from everything else because the market will
regulate itself. However this market does not regulate itself. It makes
the rich richer, the poor poorer. In the dance, the market coagulates
power in regards to how one ought to dance and what type of dances
someone should be making. This notion of “freedom” and a “democratic”
body was revolutionary with Judson and Grand Union but today, what is
the byproduct of such values and principles if extracted to a larger
social scale?

So, Steve Paxton is part of a generation of artists privileged by their
historicity—postwar America, largely white, relatively privileged in
terms of class—some middle class, some more of the independently wealthy
type. Steve grew up in Arizona: his father, a farmer who then worked in
law enforcement and his mother, a secretary. Steve was a gymnast first,
a dancer later. He left Arizona State University to dance: “as a
privileged person in this culture, and one with an appetite for the
marginal, I could pretty much shake myself free…I think that was because
of privilege…I could just do whatever I wanted to and I was too
culturally impoverished to have any other goals other than the ones that
I found in dance.”

Even though dance is a form that is always changing regardless,
conceptions of art have to change. It is not 1968! The utopic project of
living off the land and going back to nature in alternative forms of
social organization largely failed against capital. This movement,
however co-opted by the neoliberal desire for health and wellness, has
not completely “failed.” Maybe a global pandemic like Coronavirus that
has forced economies to halt and that is busy re-inscribing and reifying
the cruelties of capitalism, will finally precipitate neoliberalism’s
collapse or the social fear of such a collapse will function as portal
towards another model of living together. In 2020, many artists work to
be socially responsible, transparent, and engaged in the fight for
equity. Perhaps the possibility to be playful, optimistic, and naive was
the loudest historic privilege of postwar American culture in the late
1960s-early 1970s. In the 2020s, there is so much cynicism amongst
artists and the populous; belief is suspended constantly so that we(as
populous) and as a culture (in general) are socially scripted to
continue to invest in an imaginary realm fueled by capitalism. Belief is
so frequently suspended that people are simultaneously too busy
commodifying themselves and monetizing their futures to believe in
anything. In this landscape, what does it mean to be a dance artist? How
to dance? How can this art form relate to the social field, to the
public? How do we live life together?

Despite these free markets controlling our every move and thought, Steve
has avoided the commodification of his work and himself. In contrast to
so many other Judson and post-Judson characters that succumb to market
pressures Steve denies capital access to the work by continually
insisting that dance is an art of the moment and not something that
gains value through repetition or reproduction. There has never been an
iconic “piece” or singular motif encapsulating his oeuvre. During a talk
at MoMA in 2019 Steve explains why he stopped teaching Contact
Improvisation, to something of the effect of: “I felt like I was stuck
in a perpetual kindergarten of my own making.” Repetition is necessary
for inscription, into bodies, ideas, and of course history. Steve
repeats himself a lot but never in entirely the same way—he is always
with the moments of the small dance and the third space. Steve has
always been interested in a dance that, although danced by an
individual, is always moving towards being public—public as in the
opposite to private capital. Steve generates forms of publicness for
bodies. His dance approaches publicness and the terrains of common
knowledge and shared thought.

\*\*\*

“The whole point was communication to another mind and how to do that?
In my case, my rationale was how to do that without dictating what was
done. So, in other words, to not influence or contaminate the
performer.”

Despite a general politics of refusal around the notion of the master
artist, maybe Steve is an undeniable master-guru of postmodern dance. As
far as the oral history of impressions goes—maybe otherwise referred to
as gossip amongst practitioners—Steve Paxton is widely revered for his
integrity and his ingenuous artistry. Only recently have certain
critiques manifested: how can he not credit African diasporic cultural
forms when he speaks about a polycentric and polyrhythmic dancing body;
when he references the ways in which social dance impacted him? Is it
because his generation was not conditioned to think through these
questions of aesthetic lineage because they had a privileged historicity
or something else? Steve insists:

> How does Contact Improvisation not address sexism, racism,
> nationalism, ableism, militarism, aestheticism, popular Darwinism
> (issues of daily politics), etc.? By providing an alternative
> proposition in which they are irrelevant. How does one arrive at this
> position? By being aware of the personal, social and national politics
> and asking, ‘what is possible besides divisiveness?’

In 2020, many artists argue there is no possible world or somatic
experience/subjectivity that is not informed through the oppressive
structures of racism, sexism, able-ism, etc. And maybe the privilege of
historicity that the Judson era artists were enabled by and through is
just this capacity to believe in a principles-based and physics-based
physical exploration in which issues of daily politics are irrelevant.
In 2020, with systemic failure and an ongoing, centuries-long co-option
of the vanguard in arts by capital, such claims are no longer popular
from the voices of artists, particularly white artists, as such claims
were never plausible for non-white artists in the first place. A body’s
physics are also informed by its politics—that cannot be made irrelevant
or temporarily suspended. Any notion of physics is culturally specific
and inside of language. Claims of alternative propositions to the
problems of daily politics are no longer treated as radical in
mainstream culture—everything is an alternative proposition—and race,
class, age, gender, sexuality, et al categorizations are already tools
in the neoliberal machinery as conditions and capacities that are
financialized and incentivized.

\*\*\*

“Words like gravity were missing from dance; physics was missing from
such a physical form.”

In improvisation, whatever the elusive concept and/or way of being in
the world and in dance actually is, whatever is done is forever
original—forever authored by the subjectivity of the mover, and forever
subjugated to a particular person’s body as well as the time-space and
contextual constructs that enables the happening, the dancing event. One
body cannot replace another: one body cannot step in for another—even in
the case of Vincent Dunoyer, an apparent Paxton look and dance alike. We
are absolutely not dance robots. Contact Improvisation interrupts any
Fordist assembly line production of dance in favor of sensation, and
organization of action through principle by a particular subject with
its particular subjectivities in a particular instance, its particular
experience of gravity in relation to the material and sensorial matters
of an instant. Bojana Cvejic’s essay “A Study in Anarchy: A Physical
Quest for Natural Rights” formulates this question about Steve’s oeuvre:

> is authority situated when what is obeyed isn’t a conventional,
> historically established instance of power? How can the givenness of
> experience act as an authority, providing an individual discovery of
> concomitant laws of physics and physicality, namely, a subjective and
> lived experience of gravity?… When two people improvise together, is
> the nature of their encounter contractual or a matter of contact? Can
> a relation between two or more people be based only on “natural laws”
> (gravity, friction, support, surface area of touch…), without
> reference to “positive laws”(social conventions, gender performances,
> political aims…)?

What cannot be turned into economy (at least not on the same level) is
natural law—gravity, an individual’s own experience of the movement of
their weight and the touch of their skin against another body. This
consciousness is in fact a dance in an expanded sense. How we understand
gravity may be cultural, but gravity itself is not. How we understand
how to cope with Coronavirus as a pandemic may be cultural, but what the
virus itself does to human bodies is not.

In this conceptual construction of dance, the performer stays
autonomous. The psycho-social environment necessary to ensure an
autonomous performer is challenging, maybe even impossible. But
impossibilities are the only things worth striving for and maybe even a
harbinger of the notion of what can become radical, and the potentiality
of dance and movement to “change the world.” Listen to these interviews.
With the insistence on “essence” or “magic” or an absolute conviction to
the perception of the microscopic frame of the present moment, there are
clues for figuring out another role for art, for dance in relation to
the social field. How can we live with the awareness of incessant
eventfulness not only within our bodies, consciousness and unconscious
selves, but also in the third space between self and other, between one
social faction and another? How might dancing activities move the body
politic away from social laws instrumentalized to create differences and
privileges? Steve does not address freedom as constituted by the State,
his notion of freedom is actual freedom: the difference between
individual freedom and to be a free individual. And yet, Steve dances
without “any” sense of privacy. He becomes public and in the end he
makes himself sovereign. His proposals don’t offer anything concrete.
They simply are, and in this there is a fundamental trust in and
fidelity to dance—where there is no solo or duo or trio, with or without
gravity, there is only dance. He proposes a form of autonomy based on
radical openness—to be open to everything—and not as is more common, to
keep things out. The conundrum: to really stay open, one must maintain
indifference—“Mr. Neutral”—being unconditionally porous.

“\[T\]hat was all about how to find out more, how to… open”

*May 2020*

*Thank you to mayfield brooks, Lizzie Feidelson, Irene Hultman, Lydia Okrent and Mårten Spångberg for their insights during the process of writing this text.*

**Moriah Evans** (US) works on and through forms of dance and performance. Her choreographies navigate utopic/dystopic potentials within choreography/dance/body, often approaching dance as a fleshy, matriarchal form slipping between minimalism-excess. She initiated “The Bureau for the Future of Choreography,” a collective apparatus, to create research processes and practices to investigate participatory performances and systems of choreography in 2011. Evans was an artist-in-residence at Movement Research, The New Museum, Lower Manhattan Cultural Council, Issue Project Room, Studio Series at Νew York Live Arts, ImPulsTanz, MoMA/PS1, MANA Contemporary. She is editor-in-chief of the Movement Research Performance Journal since 2013, curatorial advisor for the Tanzkongress 2019, co-artistic direction and editor of 2019.tanzkongress.de/salons, and curator of Dance and Process (The Kitchen). She received the Foundation for Contemporary Arts Award to Artists (2017) and a Bessie Award nomination for Emerging Choreographer (2015). Notable works: “BASTARDS” (NYU Skirball, Νew York, 2019); “Configure” (The Kitchen, Νew York, 2018); “Figuring” (SculptureCenter, Νew York, 2018); “Be my Muse” (Villa Empain, Brussels, 2016; FD13, Minneapolis, 2017; Hirshhorn Museum, Washington DC, 2018); “Social Dance 9-12: Encounter” (Danspace Project, Νew York, 2015); “Social Dance 1-8: Index” (ISSUE Project Room, Νew York, 2015); “Another Performance” (Danspace Project, Νew York, 2013); and “Out of and Into (8/8): STUFF” (Theatre de l’Usine, Geneva, 2012). Her choreographies have been commissioned throughout Νew York and internationally at Kampnagel (Hamburg); Theatre de l’Usine (Geneva); Villa Empain (Brussels); Atelier de Paris (Paris); and Rockbund Art Museum (Shanghai). She received her BA in Art History & English (honors), Wellesley College, and her MA in Art History, Theory, and Criticism (20th Century Art) from UCSD’s Visual Arts Department.
