Title: The Brussels Marathon<br>Brussels, 2001
Supertitle: Cluster 1
HasPage: True
template: cluster
Image: images/steve-paxton/cluster-1/cluster.jpg
Order: 1

### Introduction

This interview is sometimes just called the Brussels Marathon. Of all the interviews in this publication this is the only one that took place in Brussels and had an attention span of four hours, which strikes us as athletic and as a generous gesture of an artist giving his time so readily.

At the time, in January 2001, Steve Paxton had arrived in Brussels to attend and perform in a three-week improvisation festival in Kaaitheater. Every week, a different constellation of artists was programmed to first work together and then perform in public, representing an array of registers and approaches. Some artists were rather new to the form, tiptoeing into the land of improvisation, others were seasoned by life and had many years of practice. Paxton qualified as the latter by far and would perform with Boris Charmatz, Katie Duck and Alex Waterman, a week after the interview. 

The series of improvisations in Kaaitheater echoed the strong interest in improvisational dance in Europe that since the 90s had become manifest. The European improvisation “wave” is referred to by Paxton in the interview as a logical outcome of deeper currents of information spreading on a global scale that had been gaining critical mass. The proliferation of eastern meditational practices, yoga, aikido, as well as the impact of the somatic sciences underlying dance that developed new knowledge about the body, the breaking up of hierarchical structures in the wake of the post-war society reform, “the shock” of Merce Cunningham's structures, the artistic experimentation with indeterminacy, games, task-based dance and improvisation in the New York avant-garde all factor in - from the perspective of somebody who was in the midst of it all. The new dance schools in Arnhem and Amsterdam, in turn, functioned as gateways to Europe. 

It's against this wider horizon that the interview speaks of lineages, of a perpetual dynamic of transmission fueled by the art scene's tendencies to articulate and originate ideas that ripple and transform. 

The interview is a verbal improvisation in itself. Without an outspoken framework or agenda, there is however a tensegrity at work that holds separate elements strongly together with invisible lines. What is it to be part of an art tradition that values invention? It's a question that Paxton takes seriously, even though one could as easily reject it for its ideological repercussions and myths of progress, market-driven fascinations with the new, and the construction of artist subjectivities consonant with such aesthetic regime, etc. It's paired with another question: what is improvisation and how can it offer a critical alternative?  

In the many meanderings the talk takes on the allure of a philosophical journey, touching relationships between teachers and students, masters and followers, Darwinian species, fathers and sons, memes and genes, in search of permissions. After one of the excursions during this journey, Myriam Van Imschoot says, “Wow, that's a jump, what a leap.” “No,” answers Paxton, “it's a circle.”

*1: Doubt, democracy, improvisation* starts from a place of doubt about making work in an art world fixated on choreography, language, and rote systems of rehearsal and reproduction. Paxton has been seeking out alternatives from the early 1960s onwards, trying various non-hierarchical collaborative forms that democratize creativity and responsibility. He mentions practices from the east, somatics, new education initiatives and social and political reform as confluences. Throughout examples are given, like his early photo scores, Forti's task-based pieces, Grand Union and the Lisbon Group as well as other ad hoc group improvisation events in Europe in the second half of the '90s in which he sees an ideal come true. “I think we finally hit critical mass with improvisation.” 

*2: Parents and lineages* takes us back to 1967, to *The Tape Piece*, when Paxton decided to broach “the question of improvisation” full-on  (the original title of this solo made for a West Coast tour is lost and we “renamed” it after a tape recorder used in the performance). A line is made to the abstract expressionists Willem De Kooning and Jackson Pollock, who favored process over result and subverted ego control. Paxton reframes the critiques of younger artists like Rauschenberg on their aesthetics of gestural spontaneity or of his Judson Dance Theater peers on Merce Cunningham (who experimented unsuccessfully with bigger freedom in *Story*) as a generational dynamic, with artists looking for stature and survival in a thriving arts scene. 

*3: Influences and memes* goes deeper into the visual arts scene of New York and “a bunch of painters with an intellectual base.” “If you are young and uneducated as I was this has a very big effect on what you base your dreams on for your own work.” Paxton sees “permissions to explore areas” in the collages of Picasso, the graphics of Franz Kline, the multiplicity of Robert Rauschenberg, the notations of John Cage, etc. René Magritte, Eadweard Muybridge and Marcel Duchamp encourage him to pursue the illumination of everyday movement as a ready-made and study the operations of the subconscious with a bias for the impenetrable. The theme of lineage returns via segues into psychoanalysis (the Schreber-case that Freud used to explain paranoia and interested Paxton for a while), Darwinism, the quest for tribes and arts families, the impersonation work of Vincent Dunoyer and his own story with his father's death that became central in the piece *Ash*. 

<cite>Image left:</cite>    
<cite>- Fred Kihn, *Ouvrée (artistes en alpage)*, Col du Semnoz, 2000. Courtesy of Terrain/Boris Charmatz.</cite>    
<cite>Images right starting from the top:</cite>    
<cite>- Fred Kihn, *Ouvrée (artistes en alpage)*, Col du Semnoz, 2000. Courtesy of Terrain/Boris Charmatz.</cite>    
<cite>- Robert Rauschenberg, Merce Cunningham Dance Company rehearsal, 1964. ©2020, Robert Rauschenberg Foundation. Courtesy of the Robert Rauschenberg Foundation.</cite>    
<cite>- Eadweard Muybridge, *Walking Pig (0,67 seconds)*, 1887. Wikimedia Commons.</cite> 



