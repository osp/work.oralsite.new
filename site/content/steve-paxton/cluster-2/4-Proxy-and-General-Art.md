Title: 4: Proxy and General Art
Date: 03-10-2001
Location: Vermont
Image: images/steve-paxton/cluster-2/part-4.jpg
audio: audio/steve-paxton/cluster-2/part-4.ogg
       audio/steve-paxton/cluster-2/part-4.m4a
       audio/steve-paxton/cluster-2/part-4.mp3
Voices: Steve Paxton (SP)
    Myriam Van Imschoot (MVI)

--- SECTION: Proxy --- [00:00]

![]({attach}ProxyP1393_RRFA09.jpeg)
:   Rauschenberg and Lucinda Childs performing *Proxy* (1961) by Steve Paxton, unknown location. Photograph Collection. Robert Rauschenberg Foundation Archives, New York. Photo: Unattributed, 1966.

SP: We're discussing *Proxy* and the number of times that people walk at
the beginning of that dance. There's seven rotations.

MVI: Can you show the pattern? [gives Steve a piece of paper and pencil
to draw the pattern]

SP: Well, the pattern assumes that there is a passageway behind the
curtain, which forms the visual back of the stage for the audience. And
the people walk across in view of the audience, and then they exit, and
then they walk around the back of the curtain, and then they enter
again.

MVI: Like this… [draws the pattern on the paper].

SP: Yeah, so it's a circular sort of pattern, and they just do it, you
know, they just walk. They don’t…the distance that they walk behind the
curtain is the same length of time as the distance they walk in front of
the curtain, and they do it seven times. They come on seven times, and
they exit seven times.

MVI: There being three or four performers…?

SP: Three.

MVI: Three. Does that mean that there's always someone in view of the
audience?

SP: No.

MVI: How was the…?

SP: Well, wait a minute. I can’t remember. Oh, God, I wish I still had
the score. I can't remember. Is there one person doing it? I can't
remember. I think there's one person doing it, you know. I think there's
one person doing it, and that on the sixth time, they bring the plastic
basin out and put it down…

MVI: Yeah! Apparently, according to…

SP: …according to the authorities!

MVI: …Don McDonagh: “It began with a long walking section in which the
first performer circles whatever forms the backdrop of the stage seven
times in the same direction carrying a white basin.” So, you're already
carrying the white base while walking?

SP: I think the white basin appears on the sixth walk or the fifth walk.
It was put down on the sixth walk and then stepped into on the seventh
walk…something like that. Maybe they carried the whole thing. I could
ask the performers who have done it. I can't be sure.

MVI: Were you the first performer?

SP: I was in the first performance, which was at the Cunningham studio,
you know, the first showing, but afterwards, I found other performers
because I really wanted to see this thing and what it was. I must have
been in the first performance at Judson, too, because I have this
feeling that thereafter, I needed to find somebody to perform it, so I
could see it. The difference being very clear to me at that point. I
mean, why was this work booed? You know, why was it rejected on the
first performance, which was so full of stuff that could have been
rejected, you know. It seemed to me…something about what I had done was
rejected. So, I wanted to see why. I wanted to see if I agreed that it
should be rejected, I guess. What I saw was an amusing dance. I found it
quite wry in it’s…but, of course, I was filled with my own projections
about it.

MVI: Did you know when the boos came? Was it after the piece, or was it
during the piece?

SP: It was after the piece.

MVI: So, it was an expression of appreciation that didn't disrupt the
performance.

SP: No, they didn't disrupt the performance, but they didn’t…it was
cynics. It was an expression not of appreciation but of…

MVI: [laughing] …negative appreciation.

SP: Yeah, negative appraisal.

MVI: [laughing] Appreciation has a…now I see, also, what you mean with
breaking down the pace, or changing the pace, restoring pace as pace.

SP: Well, and relating pace to space, so that the offstage space is not
regarded as a…or is not used as a kind of repository of the unknown and
miraculous, but is instead quite an ordinary…

MVI: Because usually, dancers run…

SP: From place to place, yes.

MVI: In the wings, they…

SP: Yeah.

MVI: They run to make their next…

SP: …their next appointment. Yeah. Here, the appointments were much more
casual. [laughing]

MVI: So, here, you expand stage space to larger space.

SP: Well, I just acknowledge what it is.

MVI: That’s interesting.

SP: But also, they were walking across the front, so once again, there
was no accumulation of energy or dispersal of energy, or any of that
kind of thing. It was the same energy all the way across, and all the
way around, and all the way across.

MVI: That does not necessarily mean that you were also investigating
something sensorial within that walking.

SP: On the contrary.

MVI: Yeah. Like, how in every, you know, like…

SP: Also, in the construct. I mean, the seven walks, and they’re…the
fact that this woman keeps coming in from one side and keeps walking out
the other, I think, is quite surreal. That's what I said, I may have, I
found it an amusing dance. I felt like it accumulated sensorial material
that was very strange, and that you would not…I don't know where you
would see it in real life. We talk about “the pedestrian” as though it
were real life. You know, all these words get confused…

MVI: They very much do.

SP: But on the contrary, you know, the actual construct made it very
unrealistic.

MVI: Yeah. Because pedestrian movement has an efficiency.
It…you…ordinary movement is usually kind of designed to be efficient.
While, if you want to enter the space and you go off, but you want to
enter it again, you would do it from that side. You wouldn't do it from
the other side. That’s…

SP: You wouldn't show the mechanics of the same movement continuing
invisibly for the same amount of time. You wouldn't include that.

MVI: Did you hear the steps?

SP: No.

MVI: Like, for example, when it’s invisible, would it still be audibly…?

SP: No, no, it wasn't particularly audible. You really had to do it in
your mind, if you were going to bother with it at all.

MVI: That must’ve been really funny, actually, if you start…

SP: Well, I liked it. [laughing] To my relief, I mean, I didn't know
if I was going to like it or not, but then when this person who had done
the seven entrances stood in the basin, and eight…

MVI: And the basin was already in the rectangle?

SP: The basin was, at that point, in the rectangle. And so, there was
this gradual accumulation of images. There was the yellow thing on the
floor already. The yellow square was there as they walked. So, they were
walking past the yellow square for six…

MVI: Yeah, which causes anticipation or expectation.

SP: If you bothered…

MVI: Yeah, yeah.

SP: Then it might, yeah. What is the relationship between these two
things, the square and the performer walking? Then, when they put the
basin down, that seemed to answer any anticipation. And then, on top of
that, the performer stands in the basin.

MVI: And eats.

SP: So, now we have an accumulation of the available material, you know,
into one image, and then the eating, which…

MVI: A pear? Was it a pear?

SP: It was a pear. It was something…a ripe pear, I have to say. It was
something soft and easy to eat and easy to get down during performance.
I thought an apple was maybe too difficult to eat in performance, you
know. I mean, just having to choke it down, you know, if it was…and I
think I also used a glass of water. I can't remember. I fiddled around
with what would be the best thing for that moment.

MVI: Where would the glass of water come from?

SP: And then…hmm?

MVI: Where would the glass of water come from? Because a pear could be
in your…

SP: Well, you see, they put the basin down, and then they had one more
entrance. So, they could then pick up the pear and or the glass of
water, and do it. Now, I don't remember what they did with the glass of
water, you know, after it was drunk. What happened?

MVI: Yeah.

SP: Or with the core of the pear, what did they do with it?

MVI: If you threw it away!

SP: I assumed that, because the next…

MVI: They ate it all!

SP: The next thing that happened was that the contents of the basin were
revealed by another performer coming on, in from the same place and
holding out their arm like this, so that the first performer held on to
this arm, and then they were turned around a couple of times.

MVI: Not seven?

SP: As though they were…no, not seven…as though they were, yeah, as
though there was no friction, as though, you know, the basin didn't turn
underneath them. They…there wasn't any problem with them turning. So,
the ball bearings, which were in the basin, which I think one heard when
they walked, you know, there was something in the basin, and we didn't
know what it was. So, it was revealed. So, there was a gradual
accumulation and revelation of stuff in a very low-keyed way. So, you
know, in that low-keyedness, I found a pleasant humor, but other people
had found it difficult to accommodate to this lack of normal
accumulation and release of energy that we think of as being
choreographic material.

--- SECTION: Less is more --- [10:36]

MVI: Yeah. That was a piece that Robert Morris thought of as minimal.

SP: See, it just wasn't my focus that minimal was a possibility. It was
my focus that everything was evident, everything was obvious, that in
looking at this dance, you had to bring with it all your dance history
and look at it with all of that information. This was not something new.
This was an aspect of all previous material.

MVI: Why did you say yesterday…“I don't think minimalism in dance…that
minimalism doesn’t apply to dance,” or something?

SP: [laughing]

MVI: Yeah.

SP: I'm not sure minimalism applies to minimalism either. You know, it's
something to do with…using a term like that is trying to point out a
difference in construct and relationships, but if Albers, for instance,
was minimalistic, proto-minimalistic work, the actual richness of the
phenomena that he's looking at, and its implications, are anything but
minimal. I mean, it's all color is, and all experience of color is being
critiqued, and when you're dealing with the human body, this is the
proper study of mankind as somebody has said, and not just the human
body, but the humanness, you know, all of the implications: the
spiritual, the psychological, the physical, the past, the future, you
know, all of that is implicit, and I guess I was trying to look at that
generality, and so, it seemed that the less I did, the more I was
implying connections and by less, I simply mean leaving out the
traditional look and pace and texture and tension that has entertained
us in dance for so long, you know…has become quite codified and
rigidified in its way.

MVI: Expenditure, energy, and…but a typical saying is 'less is more',
so even minimalist art acknowledged that the reduction, kind of the…

SP: What does “less is more” actually mean?

MVI: Less, like, if even when you reduce something to nearly
nothingness, that it will reveal…it’s the white painting. That it will
reveal not nothingness but…

SP: So, it’s a kind of holistic view perhaps, where everything is
implied in any fragment of the image.

MVI: And that…

SP: As in a hologram, I mean, holistic in that sense.

MVI: Yeah, but also that you needn’t give much to generate much. So, the
‘more’ is maybe more in the viewer’s eye, and again in the perceptual…

SP: Well, it would seem that these early works, which contain so little
of traditional dance of…didn’t find an audience that was willing to look
in that, through that particular lens…

MVI: …in that way.

SP: …whereas, much as they were used to, was enough, and anything less
was…

MVI: Yeah, less.

SP: [laughing] Was less, not enough.

MVI: Less was less.

SP: Was starvation rations. I've noticed in my own perception of my own
body though, that very often, the greater the simplicity of what I'm
understanding, is the more important understanding, and so, in some
ways, less is more, for instance, all the movements of ballet that you
get in class are done without any real acknowledgement of gravity. You
know, your mind is not on gravity although you're working, of course,
with gravity and friction and all that stuff all the time. And so, that
was one reason that with contact improvisation, I started to focus on
those words. They were really missing from the dance world. There was no
physics in dance. There was aesthetics, and there was the unconscious
use of all those materials, but unless you slipped and fell, and said,
“Gee, the floor is slippery today,” you didn't have to acknowledge the
fact that friction was a component.

MVI: Yeah.

SP: You didn't have to acknowledge the quality of the friction. You
didn't have to acknowledge the direction, or the feeling of the
friction. You didn't have to acknowledge it consciously. And then, I
came to feel that dance is a matter of, or “art dance” anyway, and
choreographed dance relies a lot on bringing the unconsciousness to
consciousness, and thereby, being able to direct what you're already
unconsciously able to do, and maybe to, via exercise, increase the range
of limbs and things like that too, you know, the consciousness has a use
in dance.

And that therefore, it can be seen that most dance techniques are
simplifications of normal movement possibilities. And something like
ballet, for instance, you could reduce. It’s a reduction of movement
possibilities to a few of those possibilities, and then, what happens if
you concentrate on those few, like the projection of limbs into space,
the longer leverage, the, you know…we achieve things with those kinds of
simple thoughts that other forms have never achieved. The quality of the
high leg extension, you know, “développé” is not a useful martial art,
kind of strength, although you see that in China, they have this
possibility, but they don’t…you know, you see it happen. You see that
they can do it. But there was no concentration of it as an art form that
was…it was an aspect of kicking that they developed to a very high
level, but our concentration of these elements into a movement form,
which has no other use other than to display the human body, and somehow
make us feel an empathetic reaction in the audience, is quite an
invention. It's quite an extraordinary invention. On the other hand, it
has already been invented, so what is the avant garde supposed to do
with it? You know, in a way, it says, “Oh, we're stuck with these
inventions, and it is here that we have to create, but within these
already invented constructs.” So, I was trying to step a little bit
outside the invented constructs, and I found myself in Iceland, you
know, as opposed to New York City. I found myself really quite a
foreigner in the dance world.

MVI: Yeah, even though the dance world was, at that time, so into…

SP: Well, the dance world wasn’t. I mean, you’re focused on the…

MVI: I mean, in general…

SP: …you’re focused on the Judson and the downtown and the Cunningham
circle, you know.

MVI: Yeah, yeah, true.

SP: There, I wasn't such a foreigner, but I was still pretty much a
foreigner, because if you look at *Flat* as opposed to what Yvonne or
Trisha or David Gordon or even Simone…Simone was a bit of a foreigner,
too, you know, with her dance constructions, or movement constructions,
whatever she called them at that time. So, it was possible to be too far
away from even that center to be in good communication with what was
going on in overall movement of the form at that time…of the dance form
at that time.

MVI: Since Robert Morris, whether it’s legitimate or valid or not but,
appreciated your work so much, and since he had this inclination towards
theory, did he theorize about it? Did he talk to you about it?

SP: No, not much. What I got from him was what he gave to everybody in
the [Village] Voice or something.

MVI: In the Voice, yeah.

SP: In the newspaper.

MVI: It's so funny that the Judson workshop, so not only when Robert
Dunn’s classes were over, and you organized weekly showings, you would
think it’s weekly showings for weekly feedback.

SP: We didn't have a strong tradition of feedback. And when we got into
feedback, very often, it was on the level of “Oh, it made me feel…it
made me remember this.” It made, you know, personal connections to
imagery that was seen on stage. It didn't do much with what was
happening on stage, and I think there's good reason for that. I think
it's extremely difficult to render movement into language. So, to give
feedback about…how do you give feedback about somebody standing still? I
mean, normally what we do is evaluate things, “Oh, it was good
standing.” “Terrible standing,” you know. It was…

MVI: “It was a little bit not long enough.”

SP: “It was not long enough.” Yeah, that kind of thing, you know, like,
or,“It was too long.” I mean, you could say, you could say you…that’s
evaluation, again. That's evaluation, right? You would critique the
length of it, but in terms of the actual, what's going actually going
on, in something like standing, Where are the words? Where is the
vocabulary? What in English helps us to cope with these kinds of events?
It doesn't have an affect. It is simply standing. So, English let's us
say that much.

Having said that, and the fact that we don't have much language to go
further, means that it's a little bit difficult to think further, and
that's what I was trying to do…was to think further than the language
gave me easy access to assess these things. They obviously exist. You
obviously can do them. It's very clear that there’s…it's not against the
law. It's not obscene. It's not anything. You know, we don’t…it's like
that. It’s…

--- SECTION: Calling something dance --- [22:14]

MVI: Did you insist at that time on calling them dances?

SP: Mmhmm. [Yes.] Well, I didn't really insist. I just was stuck with
the obvious problem, you know. Is this dance or not? Well, I am a
dancer. I mean, one of the things that I was…my reality was that at the
same time that I was standing still, at Judson, I was leaping around the
stage with Cunningham and in class, so, in my world, it was a spectrum.
That was…I was trying to make maybe a little bit broader. But for
somebody in the audience who is used to coming to a dance, they wouldn't
have had my — at that point — fairly long term questions about what is
my body doing when I'm not aware of it? And how does it get around the
world? And, you know, these kinds of basic, again, simple, simplistic
even, questions that lead to observations about what's going on in the
pedestrian and in the habitual realm.

MVI: I have two or three more issues. One issue is this calling; whether
you call something a dance or not, or activating that question. Is it a
dance? There seems to be various strategies. One is, to Simone Forti -
we talked about it yesterday - taking one medium and then calling it a
dance: reading a poem, calling it a dance, and so on. And in by doing
that, you activate the question more, even, like, well, “What is it then
if you can call it this?” and so on. But there's also another strategy,
and that is saying like, “It doesn't matter.”

SP: I took the “It doesn't matter,” or else I don't know how to…

MVI: Yeah whether it’s dance or visual art or…

SP: I don't know how to understand what matters.

MVI: Whether it’s dance, or theater or whether it's dance or…

SP: See, at this…at that point, I called it dance because it simply was
the paradox of language. And the obvious, at that point in aesthetic
circles, the breaking down of different clearly defined realms was
happening. So, I couldn't quite see how to decide what the right
language should be, and there didn't seem to be a handy word to describe
it outside of the word “dance.” I mean, I was in the process of
admitting that I was a dancer or taking on that role. I was dancing all
day, and in several different modes, and so, it seemed that I could just
assert that as a dancer, I was doing this and therefore it was dance,
you know. That seemed like, it seemed even at that time, to me, very
simple-minded, but I just couldn't cope with the memetic complications
of what trope I was actually presenting. And since there wasn't a word,
you see, since there wasn't language to describe what I was doing to
myself, I was at a loss. Now, I would think that it would be better to
not call them dances and to leave the word “dance” as an art to forms
which are choreographed and have been technically worked on, so that we
have a way to distinguish that kind of work from other kinds of work. I
mean, that's the point of words, really, is this distinguishing between
elements.

MVI: But if we speak of *Proxy* or *Flat*, because we’ve been speaking
about those pieces, they are choreographed.

SP: Mmhmm. [No.]

MVI: They are…why wouldn't they be choreographed? There is a set of
decisions…

SP: Once again, I had stepped beyond the limits, you see, that language
usually distinguishes. When we talk about choreography, what we meant in
those days and what is largely still meant, is the laborious building up
of movement sequences and fixing them in time, so that we can see more
or less the same dance one time after another. This is a major cultural
invention. This is something that is critical to our ability to…excuse
us if we ignore you.

Cathy Weiss: I don’t think you will.

SP: [laughing]. You showing us your best side that's why.

Cathy Weiss: Yes, I am.

SP: I can't help but look.

Cathy Weiss: Please continue.

SP: I can't remember what I was gonna say because you interrupted
me…with that color.

MVI: We can briefly have a break just to say hello.

SP: Let's just, actually, can we play back?

MVI: Yeah.

SP: Let's play back, and I can then get back on track, and we can erase
Cathy's entrance.

MVI: Oh, I can’t…

[tape cuts]

--- SECTION: The idea of General Art --- [27:38]

SP: Where was I?

MVI: We were…I was interested in whether it really mattered to you if it
was considered a dance or anything else…the early works that you made.
There were several options. You could think it is exciting to call it
dance even if it resists assumptions or expectations, but insist on it
being a dance because that’s what gives friction. Or maybe because you
really think, you know, this is dance, and you know, we need to expand
the whole notion of dance. The other option was more that it doesn't
really matter because there's something as a general general art.

In visual arts, usually people were speaking about painting or
sculpture, and they were very delineated disciplines and media, and I
think that although the idea was older, that in the 60s, what became
very important that there was something like general art, which wasn't
necessarily painting or sculpture or anything else, but it was art, art,
art.

SP: So, then we're making the word “art” carry all these different…

MVI: And that's new.

SP: So, we've lost the ability to distinguish between painting and
sculpture.

MVI: And that’s new, historically speaking this is quite new. Conceptual
art will even work from this notion and even develop it further. Kosuth
will really resist everything that could still relate to a discipline or
a medium. He really wants to reside in the general art. Art as art.

SP: So, what does this do for the poor critic who has to write about
this material?

MVI: Well, that's my understanding, but I think that the idea of general
art has never really achieved, especially because the institutions
aren’t general. You can say it's art, it doesn't matter what it is in
terms of traditional disciplines or backgrounds. You could even make
like a performance, and say, it doesn't matter if it's a dance, theater
or maybe visual art. It’s art. That's what it is. Then, still, the
institutions underneath it that support it or that will distribute it,
or will commodify it are very disciplined. You know, it's like, if it's
a gallery circuit, it will be pulled back into maybe visual arts. If
it's a theater who…so, I think that the ‘general art’ idea was very
unknowing. You need general institutions to have general arts.

SP: Is it useful to lose those distinctions? See, that's my question
about my early stance, you know. I couldn't make a choice of a word, and
so I chose the word “dance” as a generality, and so, in my terms, I lost
the ability to distinguish between ballet and modern. I mean, modern has
had a continual fight about whether to be contemporary dance, modern
dance, new dance, experimental dance.

MVI: Post-modern dance.

SP: I mean, we have gone through all the possible words, and now that
we're reinventing them. Now, England has reinvented “new dance,” you
know, new dance is different than modern dance, but modern dance was a
reaction to new dance, which was, you know, prior to modern dance.

MVI: Neue Tanz.

SP: And Martha Graham decided she was contemporary dance, and so…

MVI: But they all thought of them as making dance, so that's still
within the discipline, so within the discipline, you have categorization
also.

SP: Yeah, categorization. And so, I came along, and I got outside the
categories that existed at that point, and there were no words left.
Plain dance, simple dance, basic dance, movement…the idea of just
calling it a movement art or a something like that had, you know…

MVI: Kinetic art.

SP: We had to work through these kind of semantics, and I wasn't capable
of making a choice. I didn't know how to make a choice very early on.

MVI: But would the idea of a general art have a…could that appeal to
someone with that struggle? Say like, I just don't bother. It’s art.

SP: I think I respect language a little bit more than that. I think
language and the way it structures the brain should be acknowledged. So,
if you say “general art,” I think you're actually losing the ability to
distinguish, or subsequent artists will not have the ability to make
distinctions that we consider important nowadays. And I think that was
the affront that I gave to people who had a very clear idea of what
dance should be. And now, I'm willing to say, “Okay, have it, you know,
have the word.” Now, I found some other ways to say it, or I don't even
care that it be an art. I don't care what it is. It’s my work, you know.
That's all it is, and it's really up to you to claim its…to distinguish
it from other things if you want to write about it. Yeah, I gave the
writers quite a problem. But anyway, I think it's important to be able
to distinguish and to be able to transmit from one medium to another,
remarks about other media because in that way, awareness is gained, you
know. We go from…well, as the painters did with the happenings, to go
from the ‘picture plane problem’ and take it into performance, you know,
into both space and time, and yet still consider themselves artists.
This is the “general art” kind of thing. That was very productive.

MVI: But that produces, somehow, like a meeting ground, if from that
side on, people are moving into performance, and from [the other
side], performers are moving into stillness or into installation, like
physical things. You could say, well, how will we rename that middle
ground? Because somewhere, you find yourselves together in what,
strictly speaking, wasn't a visual art.

SP: Well, in those days, it was fairly simple that the painters actually
did come up with a number of words. Rauschenberg invented the word
“combines” to clear up the problem, because before that, he was being
attacked for neither being painter. He was neither one nor the other. He
was neither a sculptor nor a painter. So, he decided to call them
“combines,” and then everybody shut up, you know. Oh, okay, you know?
It's almost as though we need a language, somebody in the cabinet who's
in charge of language to take care of our current English pronoun
problem, you know, because of the gender awareness that has arisen
without language to cover groups of people who are maybe of either
gender or, you know, both kind of thing, but we need bothies. We need,
you know, or, a-lot-of-us kind of word. So, somebody should just say it,
you know, have the right, have the responsibility to come up with new
constructs as new ideas come up. And because I think a lot of the
fighting is productive, but a lot of it is repetitive, too. I mean, who
cares, essentially, whether something is theater or dance? If dance
takes on the possibility of vocal sounds, you know, it's awfully close
to opera and theater. And what's the difference between theatre and
opera, you know? Is it really just melody? Or how much of speech
contains melody, anyway, and are we making an artificial distinction in
an effort to hold these two things apart when actually they're quite
similar? But I mean, it would just be useful if somebody had the
responsibility for coining words…

MVI: According to…

SP: The same way that we coin coins, you know?

MVI: Yeah.

SP: As needed, you know?

MVI: …according to Erica Abeel, a critic who wrote on the New York
Theater, first New York Theater Rally, you and Alan Solomon coined the
word “theater piece” for containing the work that was presented.

SP: I don't know that we did.

MVI: Yeah, she was also grasping…

SP: I think it was one of the terms that was around. I don't think we
coined it though. I mean, we probably, in her experience, put it forth
first or something, but I don't think it was…“theater piece.” What kind
of word is that?

MVI: It was like looking for a common denominator and…

SP: Yeah, to describe everything: the dance and the happenings and the
swimming pool event and the…yeah.

MVI: Well, anyway, some people really insist on a dance being called a
dance, or it being…

SP: But then, they have to define what a dance is and where are they
going to draw the line? What are they going to do with the things that
are outside the line they draw? I like that. I mean, I like that
theoretically, but it has practical…

MVI: …problems.

SP: …problems. Especially in the modern arts, in which the point is to
not do what the culture has already done. So, anything that's defined,
that's why we need somebody to coin new terms. But, you know,
Rauschenberg made combines. Painters called their work…

MVI: …happening.

SP: …happenings. So, once that's done, you know, in a way, we can
proceed although the word “happening” then starts to mean too many
things, as well. So, we need to distinguish between what Red Grooms was
doing at the beginning and what Arpege considers a happening by their
ads in magazines, you know, as they reach tens of thousands more people
than had ever heard of the artist Red Grooms.

--- SECTION: Happenings and naming art movements --- [38:28]

MVI: How did you relate to happenings?

SP: Well, I mean, you're talking about a period when everything was…all
the barriers were down in New York. Downtown, anyway.

MVI: And also, people were positioning themselves, still, whether the
barriers were there or not.

SP: So, the happenings looked to me like sort of primitive theater, and
I liked the primitive quality of it. I liked the fact that it wasn't all
glamorous and slick and high-flying but yeah, it seemed quite positive,
and positive in a way which didn't have to do with actually seeing a
happening, but positive in feeling, like the culture was developing new
materials, that new materials were rising, which they were in that arena
all the time. So, it was really quite normal, you know? Felt both
positive and normal that…and if I liked the person, I would be apt to
like their work, and if I didn't like the person, I would be apt to
think that their work was a bit suspect, you know? And some of the work
was definitely suspect in the sense that we didn't trust that it was
really art.

I can remember having conversation, with David Vaughan, as a matter of
fact, about La Monte Young, where David and I…he may have rethought this
in the interim, but he said La Monte Young is not the real thing, you
know, but John Cage is the real thing was sort of implied there, you
know? And so, what is the difference between John Cage and La Monte
Young, in terms of being able to describe or define their realness, you
know? I…what is? What were we talking about? I think David suspected La
Monte, you know, of somehow being a…and this is very common in the
general public to suspect the artist is being a poser.

MVI: Poseur.

SP: As opposed to being the authentic, genuine artist, you know? But if
you're in the position of making new art, you don’t actually know what
you are. In a way, the question can't be asked: “Are you a real artist
or just faking it?” You know, “Are you fooling the public?” It’s a very
delicate question because the artist has made this stuff, and so and,
you know, as in my [case]…I don't, I didn't know what it was. I didn't
know if I was posing as an artist in presenting material, or if I was an
original artist. I didn't, you know, I had no way of assessing myself,
and maybe it's not an assessment that actually is very interesting.
Self-assessment or public assessment, you know, of the work but rather,
what is the experience of the work, becomes the next question. Does it
provide an experience that you find useful in some way, and some of the
experiences that we provided, I don't even know if they were useful. I
mean, I can theorize that watching somebody stand still is useful in
terms of understanding physiology and energy and things like that. I can
make that theory, but I don't know whether that is what people get when
they watch it, or whether there, you know, is boredom. Is boredom good
to provoke? Do we need to get bored? You know, I mean, boredom is
something we run from, we hate, we look for ways out of. At the same
time, you know, back to the tea ceremony, anything that you look at
closely enough provides an incredible amount of information about many
things, not only about itself. So, what are we talking about? We're
talking about habits or perception, you know, and staying within the
norm of those habits, or are we talking about the values of going beyond
the habits that we've arrived at, you know, and acknowledging that
they’re just arrived at habits and that we can do other things.

So, the name of a thing, I don't know. I went on to name Contact
Improvisation. And I think it was with the consciousness of this problem
that I didn't want it to get stuck in whether it was dance or whether it
was, you know, what it was. I decided well, give it a name, and then,
they can struggle with the definition of what that name is.

MVI: You've been very successful at that maybe for some people, too
successful because the contact improvisation became nearly like an
equivalent to improvisation as such in dance, but that’s their problem.
It’s been a very successful…

SP: …it’s been a successful name. And quite a number of critics who have
looked at my solo work and called it Contact Improvisation, I think,
look a little bit foolish [laughing] in their assumption. So, in a
way, it's exposed that to me that these people who write very often are
not very thoughtful, you know? They have a job, and they're doing their
writing, and that’s, in a way, a relief because I put a lot of credence
in their intellectual power, and it turns out only to be, in fact,
positional power, you know? They have a column that they write in such
and such a newspaper, and that's the end of it, you know? They actually
are taking not much responsibility for what they're doing or have much
awareness of their power. That it’s good to say, “Okay, they're just
people, too, and we're all struggling.” [laughing]

MVI: Yeah. I go back to the happenings. Billy Klüver, he sort of
suggests that — and this is about Rauschenberg, actually — that
Rauschenberg, first of all, wasn't aware of happenings, or wasn't
interested, and that Billy really had to drag, you know, drag him to the
Lower East like, You come, you know? You need to see this. You will see
this. I drag you, you know, to those performances. Anyway, that
happened, but it shows that there wasn't really a natural affiliation or
a natural…he wasn’t a natural spectator of it. Someone dragged him over,
you know,

SP: I don't think that's true.

MVI: That's what Billy…

SP: That’s what Billy thinks is true. Yeah. Well, I remember
Rauschenberg talking about seeing Red Grooms’ *The Burning Building*,
and so that was probably the earliest example of a…

MVI: That’s very early.

SP: …painter in New York. Yeah. The earliest one that I know of, anyway,
and prior to that, of course, was the *Black Mountain Event* that Bob
was a part of, with John Cage.

MVI: True.

SP: So, maybe Bob felt like he already knew what they were, and Billy
thought he was educating Bob, and Bob was agreeable to go, but, you
know.

MVI: I think Billy likes to give him that role, also, like with the
Experiments in Art and Technology. It being quite special, because he
brought people together who would otherwise never have been in the same
program, like a happening family, versus the Judson family versus, you
know, like…

SP: It's true. He did.

MVI: He did?

SP: Yeah.

MVI: So, I was wondering whether…

SP: But I mean, we were sort of in the same family, but we weren't
working on the same program exactly. I mean, the dancers were well aware
of the happenings and the happenings people were, to some degree, aware
of the dance. We had a big painting and art gallery and museum following
in the early days.

MVI: Can you remember your first happening you saw?

SP: No, I can't. I didn't see very early ones. Maybe Oldenburg, *Store*
happenings were the first ones I can remember. I was dragged there.
[laughing]

MVI: [laughing] And because you said like some of it, I liked because
I liked the artist, and some of it, I didn’t like, because I didn’t like
the…

SP; Well, by artist, I was being generalized as the artist, but what art
was, or who was an artist, but yeah, in…yeah, I didn't understand Lucas
Samaras very well, so anything he performed in, I felt had this
untrustworthy edge. On the other hand, he was in Oldenburg happenings,
so…or maybe the first one, just to correct myself, maybe the first one
was a Robert Whitman happening.

Anyway, it depended on whether I liked the artist or not in that sense,
you know, like Samaras didn't convince me as a performer. Other people
did. It was sort of on the chemical level, whether it was art or not. It
wasn't on the theoretical level whether it was art or not.

MVI: Judson is so complex because there's so many styles and aesthetics
operating within that group but if we focus on people like you, Yvonne
Rainer, Lucinda Childs, Deborah Hay, those people, there's more an
austere style or more well…

SP: And different amounts of austerity within each of those artists.
Yeah.

MVI: Did that make it like irreconcilable with the mess of happenings
and, you know, the way they used material and images and…

SP: Not irreconcilable, no.

MVI: Yeah. Would that like provide a map or…

SP: It didn't even occur to me. It didn't even occur to me to contrast
the two. They were sort of all happening in the same milieu. And again,
not so many people involved, you know? I mean, how many people could get
to a happening? Maybe 30, 40 people maximum. Hundreds could get into the
Judson to see a performance there, but that's a very small number
compared to how many are seeing ballet or seeing a film or anything like
that. So, we knew that we were a small enclave in a very big world.

--- SECTION: Competition in the scene --- [50:15]

MVI: Even in a small group, there must have been a sense of competition
or a sense of, you know, rivalry…

SP: Well, it was minimized in our group because we tried to operate
within the Quaker method, and so we didn't reject work although if we
thought work was awful, you know, which, occasionally, something seemed
awful, you know, [laughing] to a number of people, then we would
advise or try to position it in such a way that it was less awful, or
advise them not to show it or, you know, whatever…you know, personally.
But as a group, we managed to minimize competition in the sense that we
would let our performances be three hours long rather than cut people
out and make a one hour performance, or one and a half hours.

MVI: But isn't there then like a natural selection if this may work for
the concerts, the Judson concerts, but the invitations to go to other
festivals, or…

SP: Then, then there was selection, and I think that selection had to do
with quality as we understood it. I mean, that was my choice, my choices
when I invited people, which I did quite a bit of it turns out, you
know, as I look back on the history of it. It had to do with the work
that I thought was the best, and was reasonable to show outside of our
own particular, more generous laboratory. But I mean, personal choice
versus group choice, you know, or group lack of choosing.

[singing in the background]

MVI: People like David Gordon, for example, they would always sort of
point to the implicit and explicit aesthetic, you know, credo? Creed.

SP: Yeah, yeah, some kind of belief…

MVI: …belief that affirms itself as it became more manifest towards
the people themselves who made it, to an audience, that there was like a
yeah, an aesthetical hierarchy, somehow. And also, if you look at the
White Oak projects, it reaffirms the aesthetical hierarchy that was
already becoming visible near the mid-60s. It’s like…

SP: Well, when you say hierarchy…

MVI: Well, hierarchy, I mean…

SP: What are we actually saying there? I mean, because it sounds as
though…

MVI: Everything should be level. [laughing]

SP: Well, yeah, it sounds as though everything should be level, which I
don't believe is possible. You know, I think it's, I think it's good to
have ground, sometimes, as we did in the workshops and was reflected in
many of the programs of the generality of what was happening at Judson.
At the same time, we saw work develop, you know? We saw work several
times, we heard the people talking about their ideas, and how things
were made, and all of that. So, I would say like, within my own work, I
can see an aesthetic hierarchy. I can see this piece I made where I used
a ball for a chance[^Ball] mechanism, you know, as being of a low level,
Cage-derived level of invention.

Whereas *Proxy*, which actually presented much less interesting movement
in terms of interesting, quote, “interesting movement” was a much higher
level of invention on my part, and I think we could see that in each
other, as well. People like Trisha and Yvonne seemed always to work at a
very high level. Simone seemed always, to me, to work at a very high
level. Deborah…when the painters got involved, they worked at a high
level of creation. Other people, some of them didn't, you know. The
level of creation seemed very derivative, especially of Cage, you know,
which was because Bob Dunn had used him as a compositional model, and
the class became, yeah, was used and overused as a conceptual framework.

--- SECTION: Scores and indeterminacy --- [54:57]

MVI: Apart from the assignments, you never really made a chance dance,
did you?

SP: No, no although I felt like I was working from that premise to some
degree with the scores[^Score]. But I was not interested in having an objective
mechanism that would make decisions so much as I was interested in
creating a situation where the performer themselves had - with whatever
mechanism they had developed in themselves - had to make choices and to
operate on the level of creation within a certain line through the
dance. So, I was opening it up not to indeterminacy, which is the
Cageian word for giving performers options in the structure, but…

MVI: …can you say that again the last thing?

SP: Well, I haven't finished the sentence yet. I'm still struggling to
find the next word…not “indeterminacy,” which is the Cageian term for
giving performers options, but not giving options. That is, not
pre-selected sounds or indications about what to do, and maybe leaving
it open as to when it occurs, but to make them make choices about how to
be within the structure that I provided. So, I provided photographic
moments on a page for them to have to move through or hold, but at the
same time, the choices about how to get from moment to moment were
entirely up to them. So, I was really trying to see well, what does this
this kind of structuring create? And if it’s…I think another word than
“indeterminacy” should be found maybe.

MVI: I understand it very basically, indeterminacy: something is left
open for…

SP: Well, I mean, I think you have to…because Cage invented it. I think
you have to look at it through his lens. Something is left open, and it
has to do with the time structure, usually, and maybe the description of
the sounds made. For instance, like he might describe a certain sound on
an instrument in very general terms, like it rises or falls, or it's
loud, or it’s soft. So, you could make any soft sound, and it could go
someplace within these two minutes or this, someplace within these 30
seconds. So, something is left undetermined. And so, what was left
undetermined in my scores was something to do with rationale, so it
was…had determined moments and then unrationalized spaces between these
determined moments. In order words, leaving to the rationale of the
performer as to what occurred there. So, anything could have occurred.
There could have been ten minutes between these two…you didn’t have to
go directly from one moment to the next moment, in the most efficient
and speediest way, but that was the choice that most people made. They
made as few decisions possible in between, just try to get from one to
the other. But I mean, they could have vocalized, stripped, they could
have gone off stage and come back on, they could have decided that every
moment between these photographs was an exit and an entrance for the
next one. They could have done that.

MVI: Have you ever?

SP: Have I ever what? Played with this in this way?

MVI: Yeah. You are talking about performers.

SP: Yes, I have. I’m talking from experience.

MVI: Did you take those liberties?

SP: Not very much in performance, nor did I perform them very much. Most
of the scores were given to other people to do, so I have not performed
that much myself. The whole point was communication to another mind, and
how to do that? In my case, my rationale was how to do it without
dictating what is done. So, in other words, to not influence or
contaminate the performer.

[^Ball]: Here, Steve refers to a piece he made in the workshop of Robert Dunn in the fall of 1961. Dunn had set the task to invent chance procedures that would determine the use of body parts. According to Yvonne Rainer, Steve made use of a “[…] diagrammed ball which he spun and stopped with his index finger[.]” <cite>Yvonne Rainer, *Work 1961-73*, The Presses of the Nova Scotia College of Art and Design and New York University, 1974, 7.</cite>

[^Score]: Steve Paxton's use of scores started as early as the beginning of the 1960s when he integrated elements that were picture-scored in the trio *Proxy* (1961) and the solo *Flat* (1964), both pieces created and presented in the frame of Judson Dance Theater. The photo sequences were to be interpreted by him or other performers and “took the choreographer out of designing a dance” and legitimized “something that dancers do all the time, which is to give the choreographer movement, suggestions really.” Photo scores were also used in *Jag Vill Gärna Telefonera* (1964), a duet with Robert Rauschenberg that premièred in Stockholm and later in the 1980s for the pieces *Ave Nue* (1985) and *Audible Scenery* (1986) with Extemporary Dance Theater, showing that the interest in photo-generated movement resurfaced as a compositional tool and interest throughout his work. 
