Title: 3: Visual Art Continued
Date: 03-10-2001
Location: Vermont
Image: images/steve-paxton/cluster-2/part-3.jpg
audio: audio/steve-paxton/cluster-2/part-3.ogg
       audio/steve-paxton/cluster-2/part-3.m4a
       audio/steve-paxton/cluster-2/part-3.mp3
Voices: Steve Paxton (SP)
    Myriam Van Imschoot (MVI)

--- SECTION: The Japanese tea ceremony --- [00:00]

SP: Events that we know about sort of suggest this, and maybe the most
formal one is the Japanese tea ceremony. Have you been to one?

MVI: Seen one? Never.

SP: In which the act of making and serving a drink is turned into a
ritual of extraordinary depth, I felt, anyway. And Magritte has
something of that same quality. The act of looking out a window is
subverted, or taken to a different pitch, or you're shown a different
level of reality. All of these things give us evidence that we can
inquire into the routine and find mystery. And find new stations in
there to depart from in different ways than we do. The routine means
that you leave from the same station and arrive at the next station, you
know, as you always do. You kind of…the journey is considered not worth,
not exciting, not worth examining, and the idea that we should examine,
or that there's something valuable in examining the routine, has been a
strong focus for me in dance and, as well, in those moments when I'm not
dancing, you know, officially, but I'm still carrying around with me,
the kind of eidetic imagery of the consciousness that sometimes arrives
when I'm dancing. Certainly, working in the garden, working outdoors on
the farm is filled with moments like that, where suddenly I'll become
aware, in a technical way of how I'm handling the tools, or where my
mind is at or how my body is reflecting my state. So, all of the people
that I've mentioned as influences I think carry that message… The
Muybridge, the Johns and Rauschenberg, the New York investigation in the
early 60s, Frank Stella, Andy Warhol, Liechtenstein. A host of painters
seemed to be referring to this kind of…the Duchamp, you know, that the
routine is not discredited. That, in fact, it's terribly important and,
in fact, it's sort of the foundation. Our habits are sort of the
foundation on which we move through life, and we need these, but at the
same time, we can examine them and see them as a revelation in
themselves that can take us to a different pitch of awareness.

MVI: How does that work for Rauschenberg’s work? Now, you say that all
these painters…

![]({attach}white_painting_small.jpg)
:   Robert Rauschenberg, *White Painting [two panel]*, 1951, House paint on canvas, 72 x 96 x 1 1/2 inches; overall (182.9 x 243.8 x 3.8 cm), Robert Rauschenberg Foundation, ©Robert Rauschenberg Foundation.

SP: Well, I mean, we talked about the *White Paintings*. Now, what is
this but an appeal to awareness? Okay. Here's the situation that you
bring your awareness to a gallery. Here is an object on the wall, which
is called a painting. As we noted yesterday, he paints it, he puts white
paint on this, probably already white canvas, and expects us to
interpret what we see on this surface that we call blank. You know, if
you call this a blank canvas because it doesn't have a cow on it, or it
doesn't have an apple on it or something. It doesn't have a, you know,
it doesn't do anything. It’s as neutral as possible. Even in that
neutrality though, he's suggesting that we make, or we have the ability
to make and to see an imagistic world in the same way that when we look
at Leonardo's quite ordinary woman, not quite smiling, we construct
something, you know. It's a reduction to a kind of simplicity, which,
when we examine it, we find completely not simple. We find it completely
complex in its implications, and all of this is dealing with mental
stuff. You know, the brain and how it works, finally, which is one of
the great frontiers that we will forever be exploring.

MVI: That’s for Rauschenberg. How would it apply for Duchamp?

SP: Well, I mean…

MVI: The same?

SP: Somebody…it’s a little bit the question of performance, you know.
For a painter, the gallery, the museum is the performance, you know.
That's the presentation and the statement. So, the state of performance
means something is being said. If Duchamp puts his urinal into that
situation of “this is a statement, but this is a statement, which is not
a statement, but by putting it here, I've made this non-statement into a
statement,” one is forced into a system of interpreting, which I think
has a little bit that characteristic that you talked about yesterday…in
discussing Magritte, Suzi Gablik’s observation that he could construct a
trap for the observation. This trap, this ambivalence between
interpretation — and, what would you say…ignoring the existence of
something that's right in front of you? — is a place where one achieves
a new attitude, and in that new attitude, you are separated from
yourself, and at the same time, integrated in a new way. And so, this is
brain stuff as far as I'm concerned. You know, this is like basic
sensorial and mental interactions that they're pointing out and that let
us see how we are. This wonderful phrase that’s current nowadays, “What
are you like?”

MVI: “What are you…?”

SP: “What are you like?”

MVI: Oh, I didn’t know that was a current.

SP: Oh, it's very strong. It's around. “What are you like?”, you say to
somebody who says something so typical of the themselves that you just
have to laugh, you know. Or, you know, when they reveal an attitude
unconsciously, and you want to point out that they have just revealed
something. “What are you like?” Yeah, it's an affectionate kind of put
down or observation by somebody. But it's the basic question. What are
we like? And, and when we use the word like, we're suggesting, a kind of
analog. What analog is there for what you've just done? We're asking.
It's a question. What are you like? As though there were no firm
personal reality. As though everything is analogous to something. As
though, by this means, you know, metaphorical process, we figure out
relationships, which is what we do as far as I can see. And so, when you
ask somebody “What are you like?”, you're asking them about their whole
being, and what is it analogous to? And so, we've slipped into analog
and metaphor, that kind of simile, you know, that kind of world, as
opposed to a kind of more blunt, one-to-one relationship with whatever
has just happened.

MVI: When you said about Duchamp…that the gallery is the performance
situation, so whatever you bring into it will say something, and it will
require awareness, attention, it struck me, when I read descriptions of
*Proxy*, and when I saw *Flat*, that somehow, even though it already
happens on the stage, that sometimes there's like a stage within the
stage, as if you, for the displacement of everyday movement really look
for…like a pedestal, of Duchamp, causes even more attention. Or
signals even more, that we should pay attention to just an ordinary
object on it. It seems as if spatially, you've done something similar
with the everyday found movements that you bring in. And in *Proxy,* I
was thinking of the use of the ordinary actions, eating and drinking,
confined within a small rectangle…

SP: Square, yeah, the small…

MVI: …taped to the floor, somehow magnifies and formalizes these
actions.

![]({attach}Flat_P50.jpeg)
:   

![]({attach}Flat_P51.jpeg)
:   

![]({attach}Flat_P53.jpeg)
:   Steve Paxton performing solo piece *Flat* (1964). Photograph Collection. Robert Rauschenberg Foundation Archives, New York. Photo: Unattributed, 1964.

SP: In a way, I'm not pointing out the actions, I'm pointing out the
square.

MVI: The frame.

SP: I’m putting something inside a frame. Yes, exactly. I'm pointing out
the framing. The framing points out the actions, the actions point out
the frame.

MVI: Yes, yeah. And like in *Flat*, the circle, the clear pattern of the
circle, is like a frame to me.

SP: Gosh, if we just look at the framing, because also there are, often,
you know, lately, *Flat* has been done not in open spaces, but you know,
with the audience and the performer on the same floor, but in theaters.
So, you have the frame of the proscenium, and then, the frame of the
constructs, you know, the frame of the chair, for instance, or the chair
being able to be seen as a frame, as opposed to a support.

MVI: And one of the things that I think is so intelligent about that, is
that you construct platforms or frames to even more highlight the
ordinary. I mean, the ordinary requires attention. Someone saying like,
you should watch this now, and of course, the stage in itself will be
that frame or will be that pedestal, but it's as if you've been adding,
like…

SP: Yeah, the first time the circle occurs, it's just somebody walking
in a circle, which is not very usual, actually. We don't walk in circles
that usually, unless it's for exercise or something. But the second time
it occurs, it is…

MVI: …it carves space.

SP: Yeah, it's a pattern that has been established. And, and then the
third and fourth and fifth and sixth time it occurs, you know, it's
like, “Oh my God, can’t this person do anything but walk in a circle?”
Or, “What is a circle? Why is this statement being made?” Yeah, so, you
ask questions. It provokes inquiry.

I think it would be useful to say that to really investigate the
ordinary requires the utmost concentration, because you start saying,
“Oh, yeah, sitting down is ordinary,” and then, you start going into the
moments of the sitting down, which is what's happened to me in
performing *Flat*. There are various aspects of the movement, you know,
that I see as important fractions of that movement, and then within
those fractions, there’re yet smaller fractions, and so, you're going
into…you're examining the units, what you consider a unit. Like
walking in a circle, right, is a unit as you first see it. Okay, that
construct arrives at a kind of unity, but within that, there are all the
steps and the shifts of gaze and the little fractional turns of the
torso and the swinging of the arms, and so each unit then breaks down
into a number of other units, which break down into even further units.

[phone rings]

And we are soon lost in the fractions, and…

[phone rings]

…that’s where the attention needs to be finessed, and I think that's the
tea ceremony situation.

[tape is stopped for Steve to pick up the phone]

---- SECTION: Breaking down in components ---- [14:43]

MVI: I was amazed to hear that, actually, in the beginning, you were in
*Flat* breaking down the sitting…

SP: I did a little bit. I did. There were fewer of them. I saw fewer
units within the units, as it were. But one of the first things I wrote
is, “What is a unit of movement?” You know, looking at exercises and
classroom stuff, I was trying to figure that out years and years ago.
So, now, I, one still wonders how to imagine movement. How does one
create an image of movement, and what is happening in these tiny little
fractions? There seems to be a level of consciousness, which is fairly
wholesale, you know, large units of that, but it's possible to use
consciousness to go into the…

MVI: …fiber…

SP: …the fiber.

MVI: Yeah.

SP: And, yet, none of that tells us anything objective about what a unit
is. It's more something that arrives in consciousness, and you accept.
You know, a trip to the store is a unit, you know, even though it
obviously involves hundreds of thousands of decisions and sensings, you
know, to do it. You ignore all of those things, so it's interesting to
look at all those subparts of every action.

MVI: Yeah, in fact, the advent of optical instruments - and photography
is one invention within a line of invention — has helped us in
decomposing.

SP: Yes, and seeing the…well, not decomposing but de-unifying. Oh, how
do you say it? Because decomposing sounds like rotting. It sounds like a
process. “Un-composing” maybe, would be better, a better image, anyway.

MVI: Well, people would say “deconstruct,” but I think that’s a word you
have to be careful with anyway…

SP: It’s a much better word, but it's overused these days, and it has…

MVI: Well, deconstruction could be like taking it apart and…

SP: Yes because we're now photographing atoms as opposed to
photographing objects. Or, the atom has become an object, instead of a
component of an object, you know. This kind of shift in consciousness.

MVI: And that's what Muybridge was doing. He was atomizing.

SP: Yes, going in that direction, breaking down the unit.

MVI: In general, I think photography has been so influential in how we
look at a thing, at a unit, at reality. Maybe even “the ordinary” as a
concept has emerged along with the photographic gaze, I mean, the more
mobile the cameras became, and the more instant the way of shooting, you
didn't have to wait for a long, long time before you could shoot
something. That meant you could take it out into the world and you have
a mobile frame that whatever it was framing would be composing
attention. So, the ordinary is something that somehow became visible
through this process, through the photographic gaze, and I think that's
a very important thing, actually, that maybe we need sometimes technical
utensils to refine perception. You can drop the camera now. Now we don't
need the camera to maybe do that, but it helped us realize. It helped
us.

SP: But doesn't it just reaffirm the tea ceremony?

MVI: How do you mean, like, in what sense?

SP: Well, the tea ceremony is a tool, a cultural tool for refining the
perception, and comes from a tradition in which refining the perception
is…has been taken to a very high state. Refining it as far as we know,
to its highest level. And it suggests, as well, that there has always
been a question about what was perceived, you know. So that any one
could say that any technical, technological development of any kind,
from fire on through like — what do you call it — electro micrography,
or whatever it's called, point the mind to new levels. In other words,
the mind is being directed by the memes and cultural accumulations of
one sort or another. There is behind you on that window ledge, some
pottery.

MVI: Yeah?

![]({attach}site_content_images_steve-paxton_cluster-2_part-3.jpg)
:   Steve Paxton arranging pieces of Hohokam pottery. Photo: Tom Engels, 2019. 

SP: These are pieces that come from my state of Arizona, from the
Hohokam people, and they're about, oh, I don't know, a couple of
thousand years old, so they were made before America was invented by
Europeans, and they have stripes on them. They have glazes and stripes.
Well, glazes make sense to me because they, you know, keep the clay from
absorbing unwanted materials, but the design…why did they put design on
it? What are they…? Of what use was that? Why does design, why does
painting essentially arise in a culture which was, as far as we
currently understand, uninfluenced by our own design fixations. And it
seems to me that there lies a clue to our investigation into the
ordinary, and how the ordinary is useful. And I would say that this clue
suggests that one of the basic things that's happening, is sorting and
assessing. So that you would want one design on one pot to tell you.
It's like a label. If you didn't have writing, design would serve as a
label to tell you that that's where the beans are and not where the corn
is, you know. So, if you want beans, you know where to go. Or, that's
the one that's used for water, and we don't use it, you know, for other
things. That one of the things we're trying to do is just sort things
out, and it goes right back to hunting and gathering and is a kind of
basic element of the mind.

MVI: Yeah. I would agree.

SP: Yeah, and that if you have the habitual, you know, if you are stuck
in a rut, as they say, you know, and your life for long periods consists
of just following a schedule, in which you notice small differences like
new headlines every morning or something like that, but that, again, is
still part of the newness, even, is part of the habit that they’re…that
you aren't sorting and assessing your day in that way because it has
already…it comes pre-sorted, and then, in a way, the brain goes to
sleep, and it's, you know, the tea ceremony seems to address that, and
other kinds of things that we do, which wake us up, which includes
theater, or TV even, if TV were doing a better job.

--- SECTION: Everyday life and critique --- [23:50]

MVI: Still, if I take Suzi Gablik’s observation, she compares a way that
surrealists and pop artists deal with the every day, she thinks that a
big difference between those two approaches is that the surrealists
really try to critique a bourgeois culture. They want to critique a
culture that — my English is bad today — redefined…or, in the routine
that…fossilized, or whatever, in the patterns and the routines. So, it's
a critique. If there's something that they don't want to settle in, it's
that; while the pop artists would be more ambivalent in that sense.
Whether it is a critique or not, it's hard to tell. They much more take
an everyday object, or icon, and use patterns without necessarily
criticizing any of that and just presenting it.

SP: But isn't that one of the traps? I mean, couldn’t we see the pop
art, particularly Warhol, because of the kind of rough quality of the
images, the silkscreened ones. You take the Campbell's soup can. Isn't
there a kind of perceptual trap there where you're stuck between whether
this is serious or not, whether this is critique or not, whether this
is…you don't know where he's at, and the mind’s…a vibration is set up in
the mind there, between possible alternative interpretations. So, you
can't say that it's critique, and there does seem to be more of a
critique in earlier surreal work, but…or pointing out, maybe not a
critique, even, but just a pointing out of other possibilities or
ambiguities within what we think of as normal or ordinary. And Andy's
thing seems to build on that, knowing that the ordinary is
critique-able, you know, when you are then presented with another
example of something ordinary but without the obvious mystery that
Magritte built into his work, for instance. You still suspect that
there's mystery there somehow.

MVI:  Yeah, that's right, because it's beautiful, Andy Warhol’s…

SP: It's not so beautiful. There's nothing…

MVI: It’s very aesthetical. No? Don’t you think so?

SP: Oh, man, that's a big question, you know.

MVI: The flowers…

SP: There are many people who disagree. The flowers were ugly flowers.
They were, they were just simple flower shapes. They couldn't have been
less intent on telling you that flowers are beautiful. In fact, if
anything, they tell you that flowers are stupid, you know, that these
are dumb flowers.

MVI: Sentimental, or…

SP: Sentimental to the max, you know? Yeah, Walt Disney flowers of the,
you know, cheapest reproduction of Walt Disney cartoons in comic books.

MVI: But there was Duchamp’s, for example, rejection, a little bit, of
pop art. He said: when we pulled in a found object, we were doing *this*
to a whole bourgeois culture while… [Myriam puts up her middle finger]

SP: She…that was a finger! The word “this” means a middle finger was
extended there in that remark. [laughing]

MVI:So, he reproaches pop art for its…he also says that the found object
wasn't about being “aesthetically pleasing,” or there's no
aesthetization of that object. There's nothing to think “it's beautiful
or not” or whatever, and he thinks that pop art is very, really, I don’t
know…

SP:…aestheticized.

MVI:Aestheticized, and…

SP: Well, de-aestheticized in a way, you know, the Campbell’s soup cans
are blotchy, badly reproduced, you know. The aestheticization happened
in the commercial project product in a way. This careful design element
to make it pleasing for the eye and make you buy the product, you know.
Then, when he reproduces it, it's not nearly so perfectly done. We can
clearly critique it negatively in a lot of ways, but he does set up this
ambivalence about what he's doing, which he maintained with his silence
and his mysterious aura that he projected around him. His sunglasses and
his wigs and his, yeah, this falseness, false front that he gave us.
Anyway, I don't know how to deal with Duchamp's criticism of pop art,
but my own sense of pop art was that it made us see ourselves, and
especially if there were multiple artists doing it, you know, that you
had not only Warhol doing it, but Liechtenstein and Rosenquist, and a
number of other people.

MVI: Fahlström.

SP: Yeah, and the ways that Johns' beer cans and Rauschenberg’s use of
the whole graphic world as his palette made us see things anew, or see
them from a different position. Maybe we didn't see the things anew, but
we, ourselves were changed by the position that they presented. We could
obtain a new relationship. You could go to the gallery and obtain a new
relationship. You didn't even have to buy the painting. You could see it
for free and get this new relationship.

--- SECTION: Attentional phenomena --- [30:10]

MVI: Well, art always is about drawing attention to an object, or to
something. It seemed as if the art in the 60s more and more drew
attention to perception, and to the relation with something that's not
necessarily the object of that attention.

SP: But it was all prefigured. I mean, with Duchamp and Albers and,
well, some of the earliest of the minimalists, people who were kind of
not paid attention to, until enough artists started following them that
they had to be acknowledged, but I mean, Rothko was there, and much paid
attention to. People who seem to be working with phenomena rather than
subject matter, and some of that phenomena was mental or attentional
phenomena.

MVI: That's beautiful as a category, “attentional phenomena.”

SP: Well, I mean, it’s what the improvisation is all leading to, as far
as I understand it, you know. What is possible? I mean, as a performer
of improvisation, what is possible for me to perform? And it has all to
do with where my attention is, and how refined that attention is. So,
it's the tea ceremony, you know [laughing], only I'm not doing it in
this calm, ritualistic way. I'm doing it in the more frantic,
full-bodied dance around way, but it does open up to my, or as a
possibility, virtually any kind of movement at this point, as something
that can become material in an improvisation, and that I figure will
have an effect. That I can't do anything in this laboratory, under the
microscope of the eyes of the audience, I can't do anything that won't
be both material or “un-causative.”

MVI: I’m sorry. “Un-causative?”

SP: I don’t know. “Un-causative.” It's not a word. It's a word I need to
invent right now. “Causative” would mean, I could say that everything I
do will be seen to have been caused, or will cause the next thing, you
know, cause and effect. But what I'm saying now is the opposite. I'm
saying that I can't do something which doesn't do that. I don't know why
I'm putting it in the negative, but I'm putting in the negative. I'm
saying that it looks like everything is causative, but that isn't enough
to say. I have to say there's nothing we can do now, which can be seen
as outside the spectrum of both material and phenomena that are being
observed. It is about attention. It is about focus of the mind, it is
about how these elements in the brain connect physically with the
senses, and physically with a body. And so, yeah, we're just looking at
that, overall attentional phenomena.

MVI: Attention is something different than concentration, for example,
or…

SP: Oh God. Oh God. Do we want to go into this? Because it’s…

MVI: Yeah. Maybe later. Maybe we should stick to the visual art, but…I
feel clearly, and I haven't been able to point my finger to it, that
there's more and more of an interest in attentional phenomena in art.
And not that it has never been there, but it’s…

SP: It was always there.

MVI: It was always there, but it’s…

SP: The readers of the art didn’t necessarily know that the artists
thought that, or could think it, or were feeling it, or, you know.

MVI: Yeah, and that the prominence of attentional phenomena has an
immediate link to improvisation…

SP: You don't think it was in abstract expressionism to go back to its
influence on the early days of Judson and all that? I mean, or its
prominence during those days?

MVI: What do you mean, like…?

SP: Attentional phenomena in de Kooning, for instance. What was he
attending? What was he thinking? How free was he being with his
prejudices, perceptions, assumptions, and all that, as he quickly
generated these canvases? How critical was he of what he had generated
the day before? How much did he change? You know, this kind of thing.
But don't you think, you know, just for one incredible moment there,
there was a lot of attention being paid to what we would now call, or
what I am now calling “attentional phenomena.”

MVI: Maybe then we have to be a bit more clear about…

SP: How about Albers?

MVI: Joseph Albers.

SP: Joseph Albers. What about Joseph Albers?

MVI: Into color composition and his whole color theory and his quest for
materials. He was very much into materials.

SP: What do you mean by “materials?” You mean, the quality of…

MVI: That he had assignments. That students would go out and collect
materials, maybe like a rock or a piece of wood or…and compose something
with that.

SP: But I mean, in the work of his which remains strongest in my mind,
it's squares within squares within squares. Each had their slightly
different color, and he seems to be saying that color is quite relative,
depending on its circumstances. Is this not…and color is certainly one
of the critical elements of seeing…

MVI: Sure.

SP: …of our vision. So, is he not just calling attention to this
element? How relative, something that we say…we say the red car, but
it's going to be a different red if it's in the garage than it is if
it's in the sunlight. These are attentional phenomena as far as I can
say. In other words, it's maybe an attempt to get the mind to see
relationships in time on a lot of different levels. So, it's the old
problem of time: how do we conceive of it? How do we know it? The mind
seems to have a difficult experience of “the moment” which it is in. It
seems not to be able to objectify it, at the same time that it's having
it in a way. You know, we objectify it afterwards, in memory, even if
it's what happened a few minutes ago.

MVI: First level observation, second level observation, you observe your
own observation.

SP: Yeah, yeah. So, maybe he's suggesting something about that problem
as perhaps Duchamp's *Bride Descending a Staircase* was, you know, in
which he chose a kind of photographic presentation of time, like
multiple exposure of one element, and so we didn't have, we didn't have
a *Nude Descending a Staircase* in the way that we would have with
Rembrandt, you know? We had a *Nude Descending a Staircase* the way that
we might have with Eakins or Muybridge.

MVI: I mean, many works of art see our way of looking as nearly
transparent, you know, you just look in order to assess something, while
there’s works of art that throw you back on that action; they kind of
make you question that looking.

SP: Like *Zero Through Nine*.

MVI: Yeah, *Zero Through Nine*. So, attentional phenomena will always
like recast attention on, you know, the process of paying attention. I
see.

SP: *Zero Through Nine*, to see *Zero Through Nine*, or, one way to see
*Zero Through Nine* is to say, “Okay, I can see the zero. Okay, now,
with a little shift of focus, with the image of one in my mind, I can
make out how the one is constructed. Yes, I can see the two. I can also
see, yes, I can see a three and a four and a five and a six and a seven
and an eight and a nine.”

MVI:Yeah, infinity.

SP: Each one of them is preceded by having the image in your mind, and
constructing it on this quite complex canvas, but I think we should
maybe for a second leave the art world, and say that when…that in the
world of dance, we don't have anything like the assumption of a moment,
of a frozen moment. We don't have the assumption of a moment stopped the
way we do in photography and painting or sculpture.
