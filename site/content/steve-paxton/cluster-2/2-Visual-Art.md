Title: 2: Visual Art
Date: 03-10-2001
Location: Vermont
Image: images/steve-paxton/cluster-2/P654.jpg
Color: #efefef
audio: audio/steve-paxton/cluster-2/part-2.ogg
       audio/steve-paxton/cluster-2/part-2.m4a
       audio/steve-paxton/cluster-2/part-2.mp3
Voices: Steve Paxton (SP)
    Myriam Van Imschoot (MVI)

--- SECTION: First connections with visual art in NYC --- [00:00]

MVI: Visual art. When did that connection happen? You’re nearly two
years in New York now.

SP: And I've only been to museums. I haven't been to a gallery yet. It
happened obviously around the Cunningham connections. Because there, in
the studios, you saw Jasper Johns and Bob Rauschenberg, and loads of
other artists. Also in that building was The Living Theatre, and there
were loads of artists in and around that.

MVI: Was that the building on 14th Street?

SP: Do you know do you know the artist Ray John? Also Remy’s
connections. Remy [Charlip] was also an artist.

MVI: Yeah, I saw paintings of his.

SP: So, yeah, so he had lots of friends who were artists. Yeah, 14th
Street and Sixth Avenue studio.

MVI: What did they do there? Did they watch rehearsals? The artists you
mentioned?

SP: I don’t know.

MVI: They hung around?

SP: I mean, you have in your archives, artists making music there, so
they were there presumably to rehearse their musical evenings, and they
were just connected. It was a very connected scene. Poets, playwrights,
musicians, dancers, painters all know each other and knew where each
other drank and hung out, knew who each other was going with, you know.
It was a very, very small number of people involved, in the sense of
maybe 50 or 75 or 100 people all told, who made the activity that I
observed.

MVI: Seeing those people, they're part of a building, they’re part of a
biotope. Does that make you go to a gallery?

SP: Not yet.

MVI: [laughing] Not yet. Does that make you want to see their work?
Does that make you want to go to their studio?

SP: Yes, it made me want to see their work. It made me get some idea of
what they were doing. The first artists I can remember are people like
Ray Johnson[^Mail], who were doing mail art.

So, there was this radical element. Then, I started getting on mailing
lists, and so I was invited to a Red Grooms happening. I was invited to
various other openings. But I didn't really start going, I think,
until…I think, as a member of the Cunningham Company, when to support
Jasper and Bob, we would go to their openings and various other artists
that Cunningham and Cage sort of mentioned or approved of, you know. I’d
go to see what they were talking about. Then, I started feeling
comfortable in the gallery. I realized it was just another browsing
land, you know, go and see what was there. So, then I started going
more. Still, to a limited number of galleries, you know.

MVI: Probably uptown galleries.

SP: Yeah, mostly. There weren’t so many downtown at that time.

MVI: When did the scholarship shift into becoming a member?

![]({attach}61.P005.jpg)
:   Robert Rauschenberg, *Untitled [Cunningham dancers]*, 1961, Contact print, 2 1/4 x 2 1/4 inches (5.7 x 5.7 cm), ©Robert Rauschenberg Foundation.

SP: It happened via a work called *Aeon*[^Aeon], so I think that's ’61. And I was in that one work, and then that, and Remy was still in the
company, and then that autumn, Remy left, and so, I started learning his roles.

MVI: Did he teach it to you?

SP: Mmhmm [no], Cunningham did.

MVI: Because very often the dancers teaches his role.

SP: Not, not in the world where, you know, people leave…

MVI: Yeah in that way.

SP: In bad…with bad feelings.

MVI: With bad feelings.

--- SECTION: A Cook’s Quadrille --- [04:47]

MVI: So, before you were a member of a company, you had choreographed *A
Cook's Quadrille*, and Cunningham had contributed a dance to that.

SP: That was very generous. That was incredible. It wasn’t a very good
dance.

MVI: Yeah, you told me. Why not?

SP: Because he was actually trying to choreograph it!

MVI: Wait a minute. [laughing]

SP: You know what I mean? He was constructing it on my girlfriend and I,
bit by bit. He wasn't doing it by chance procedures. He was trying to
make a children's dance. This is actually a specialized and not
well-appreciated skill, and so, it was a little bit overly generalized,
you know, children's kind of stuff.

MVI: If you say he contributed a dance, what does that mean in terms of
the entire piece?

SP: Well, the entire piece wasn't a piece. It was a series of works,
essentially, you know, about an hour long, so we could just not do
something else and do the Cunningham. But it was very generous because I
think he knew he wasn't very good at it, and I think he knew it wasn't
very good. And it was just…he was very fond of us, and we had done
things, I think, I don't remember how I got into that position, but
Juliet and I helped paint his studio on 14th Street when he first got
it. So, yeah, he was fond of us for that reason. We were helpful young
people, and he was helping us out.

MVI: What happened to Julia up to that point? Was she still also a
dancer with Limón, and did she stay a dancer there?

SP: Juliet was a great dancer, but she was Chinese.

MVI: Is her family name Hurd?

SP: It’s Waung.

MVI: Juliet Waung.

SP: Juliet Waung. W-a-u-n-g. She was Chinese, so she aspired to the
Limón Company. She wasn’t…she liked Cunningham, but she didn't aspire to
be in that company. She wanted to be a Limón dancer for some reason, and
because she was Chinese, it was a little bit difficult because the works
were all based on…

MVI: American history or…

SP: Different, well, different ethnicities. I mean, they had certainly
accommodated José's Latin, Mexican, Indian looks, you know, and they had
found ways to do that, and they had found…and when he choreographed, he
choreographed mainly for his company, which was essentially white,
WASPy-looking people. Maybe a little Jewish, you know, now and then or,
I mean, Pauline Comer could look Spanish. She had dyed black hair, so
she could braid it and make it look…become an Indian, or she could, you
know…she had a kind of, an ambiguous ethnicity, but yeah, it had a kind
of ethnic bent that Juliet didn't fit into. So, however good she was and
however hard she worked, she wasn't really…

MVI: The typecasting of it.

SP: The typecasting. The repertory was based on different ethnicities.
There was no Chinese role.

MVI: So, did that, what happened then? Did she…?

SP: I don't know.

MVI: Continue dancing, or?

SP: I don't know. We drifted apart, and I never found out what happened,
you know, with her dancing.

MVI: You didn’t see her dancing anymore, some place, or…?

SP: The next thing I heard, she was designing costumes for ads. She was
a very skillful craftsperson, and she ended up making money that way. I
don't know what happened with her dancing.

MVI: Did you make *A Cook’s Quadrille* together?

SP: Hmmhmm. [Yes.]

MVI: The two of you? Or…?

SP: And with at least one other person.

MVI: Shall I say the names? [reading from her computer screen] There’s
like, Julia Hurd and Bernice Mendelsohn.

SP: Bernice Mendelsohn. Julia Hurd was…I don't think she was in it. I
don't know what her name is doing there. I don't remember that we worked
together. She was in The Merry-Go-Rounders. So, she would have been a
connection from there, but I don’t think she was in *A Cook’s
Quadrille*.

MVI: The information I got was from the Merce Cunningham “50 Years”
book, with the chronology of all these works and dances. So, since he
contributed, there are credits. It says a show for children by Juliet
Waung, Steve Paxton, Julia Hurd, Bernice Mendelssohn. Duet choreography
by Merce Cunningham, music by Merce Cunningham tape, and dance by Juliet
Waung and Steve Paxton. So, maybe…

SP: So, maybe Julia was part of it. I can't remember. It was a very
complex and difficult event between us, all trying to make this thing
and thinking what a good idea, but not actually knowing—any of us—how to
make it work, which was summer touring to summer camps, what we were
trying to do. So, it meant getting a car, and I was the only driver, and
so I had to take care of the car, and Bernice Mendelssohn was very
inspired to do it, but never contributed any ideas and was a very
difficult performer. She also was in The Merry-Go-Rounders. So, we were
setting off to make a children's company but not having any real skills.

MVI: So, you didn't produce this piece under the wings of The
Merry-Go-Rounders?

SP: No.

MVI: That was like another…

SP: It was a separate…

MVI: Separate. Trying to make a company.

SP: Yes, trying to make a little company that would tour in the summer,
going to summer camps.

MVI: Was it the first piece you made for, well, the first piece you made
since the workshop piece, *Power*?

SP: Yes. I believe.

MVI: Prior to that, did you feel you were interested in making dances,
or…?

SP: It was sort of expected, but I didn't feel that drawn to it. I
really wanted…I really felt I was just a dancer at that point. Barely
that, you know.

[tape is stopped for Steve to make a phone call to Boris Charmatz and
the conversation resumes]

SP: I mean, the only thing that's interesting is that we tried to do it,
that we actually tried to make something happen. That was a good sign, I
guess, but the work was not interesting.

MVI: Doesn’t matter.

SP: Probably does matter. I…my philosophy now is that students are meant
to fail, and teachers are meant to say, “try again.” And so, it's
important to try and even if you fail, you know, it's important to try.

MVI: Does it feel like, that was clear to you then already?

SP: Yes. It was very clear.

MVI: Or are you saying this in retrospect, with other aesthetic
standards?

SP: No no, these aren't really retrospect. I haven’t…it’s an opinion I
formed at the time. There were, I mean, it was very, it was full of
difficult logistics and getting ourselves in this old bad car around to
the summer camps that we wanted to perform at, and there wasn't a lot of
interest, you know, probably five or six performances.

MVI: In that summer?

SP: Yeah.

MVI: That’s not enough, uh?

SP: No, you know, we thought it was going to be a summer living. And I
think it might have had a chance of success if it had been good, but it
was clear to me that it wasn't very good.

MVI: Was it a shared opinion amongst…?

SP: Amongst us? Well, I don’t remember.

MVI: And would that be, like the pieces you have been dancing for
children, was it modern-dance based?

SP: Yes. But also ethnic dance-based and had actually several styles
that you had to…

MVI: Accommodate.

SP: Yeah, accommodate.

MVI: But for children, I can imagine that you're looking for variety or
diversity.

SP: Yes, and education.

MVI: And education, like an introduction into…

SP: …the Indian dance, you know, an introduction to Hindu mythology and
Indian dance styles.

MVI: Would you link it up with a theme, like an overall theme, like…

SP: In *Cook’s Quadrille*?

MVI: Yeah.

SP: I don't recall. I don't think so, particularly. I don’t think we
were so sophisticated as to have a theme. We thought cooks’ costumes
would be funny, you know, and cooking references would be funny, and
children would enjoy it. I don't know why we thought that, but…

MVI: “Quadrille”[^Quad] is a dance form, isn’t it?

SP: It’s a dance form.

MVI: Yeah, what sort of dance form is that?

SP: Well, it's for four people.

MVI: Quadrille.

SP: Yeah.

MVI: Is it a square dance?

SP: I think it's the original square dance.

MVI: Yeah, that's an American…

SP: No, it's a British…or quadrille…maybe it's French.

MVI: Yeah, but a square dance still.

SP: Form.

MVI: Form, yeah, so you would.

SP: Four sides.

MVI: Yeah, and…

SP: Crossing, yeah, corners, but we didn't do a quadrille.

MVI: You didn’t do it?

SP: There was not a quadrille in it, no.

MVI: So, how would Cunningham adjust to that format? I mean, how did he…

SP: He made a dan…he made a duet. So, the duet would just be performed
in the piece.

MVI: I can imagine that humor is important.

SP: That's what everybody assumes. […] Kids, I don't think…I think
they get obvious. I think they know… They need to know when a joke is
being told, and if, yeah, any more subtle kind of thing, they're not
sure whether it's a joke or not, they don't know what, you know, they
get confused as quickly as they get amused.

MVI: When you were setting up, making this first piece of…must have been
like a test case to see if, indeed, a company could get started and
other pieces would…

SP: I think we just didn't have any idea how complex it was to actually
have a company. Inter-socially, artistically, you know, to actually…the
craft of making dances. We weren’t skilled at any of these things.

MVI: So your first experience is one of struggle.

SP: Struggle?

MVI: Yeah, that's that's what making a dance…

SP: Yeah, yeah.

MVI: …would have been linked to…

SP: Yes, definitely. When you're, when you actually come to the studio,
what do you do? What mechanisms are there? What do you make, you know?
How do you know if it's any good? All those kinds of questions.

MVI: Did you ask any feedback of people who were more…

SP: Not really. No, not really.

MVI: Inviting them and saying, “Just give us feedback.”

SP: I don't think we really did that. No.

MVI: Why did you say…

SP: That’s, again, a process that you have to learn. You know, it's not
something you intuit necessarily. I wasn't into that much, anyway. You
know, as I mentioned earlier, I didn't think of asking for…

MVI: But Merce proposed!

SP: Information.

MVI: He proposed to contribute.

SP: Yeah, yeah.

MVI: Like, I’ll make a dance for you.

SP: I’ll help out. Yeah, I think that's the spirit in which he did it. I
mean, his time is so valuable. I can't even imagine what fiddling around
with these…we didn't rehearse that much. It wasn't that long. It wasn't
that difficult. But he…still, he was doing it. You know, he was giving
time.

MVI: Yeah. Yeah, he didn't do that very often. That's like one of the
rare occasions when he did that in his overall career.

SP: Yeah.

MVI: So, he must’ve indeed been very fond of you, or grateful.

SP: Well, he was fond of us. We had helped him, you know? We had pitched
in, and I don't know, I was sort of raised that way. It never occurred
to me that it was unusual, but now, I realize it is unusual for people
to help. There's something about it. Maybe it puts somebody in your debt
or, you know, it's like an assumption, somehow, of closeness that people
socially often don't do.

MVI: Why did you say last time I was here…you said like, all the dances
before *Proxy*, never-mind about them, but there's not that many dances.
I mean, it's like, the only dance I know of is *A Cook’s Quadrille*.

SP: There aren't many dances. There were some other dances for that
course that I made.

MVI: The Louis Horst?

SP: No, the Robert Dunn course in which I made *Proxy*.

MVI: Before *Proxy*, like assignments prior to *Proxy*?

SP: Yeah, yeah.

MVI: I see. Although there’s very few references to any of your…

SP: ...contributions.

MVI: ...contributions. So I wouldn’t have a clue…

SP: Yeah, that's quite right. [laughing] There’s not much to know.

--- SECTION: Back to the visual artists --- [20:09]

![]({attach}64.P003.jpg)
:   Cunningham Dance Company rehearsal, 1964. Photo: Attributed to Robert Rauschenberg. ©Robert Rauschenberg Foundation

MVI: Yeah. Okay, so I…so, we are [sound of Steve trying to hit a fly]
now in the Merce Cunningham Company and…

SP: Okay.

MVI: As we said, there's people hanging around and going to bars and
finally, you're on the mailing list, and you get the invitations, and
you feel like you're supporting artists by…

SP: I’m seeing artists, yeah.

MVI: …by attending their…

SP: …their openings and their…going to parties and…

MVI: I wonder, like, how that works because for many versions and
reports and interviews I read or did, the classes themselves have a very
formal social pattern in terms of the relationship between Merce
Cunningham and his dancers.

SP: Right.

MVI: So, it's hard for me to imagine the shift from that social relation
to one in a bar.

SP: I didn't go with Merce.

MVI: Well, he would…

SP: I occasionally saw him in those situations. But I went with younger
people. I wasn't hanging around with that crowd. No, not in that sense.

MVI: With the other dancers?

SP: Yeah.

MVI: The younger people, the younger folks.

SP: Younger folks.

MVI: Yeah. The artists were older?

SP: Yes.

MVI: How did that…?

SP: They were sort of paternal, you know. You're talking about the
abstract expressionist crowd at this point, de Kooning and Klein.

MVI: Well, depending on what parties you were going to and what…

SP: Depending, but I mean, they were the main artists who were, you
know, that one saw at The Cedar Bar, for instance.

MVI: Yeah, like de Kooning?

SP: Yeah, de Kooning, Klein, who else? Well, and younger artists around
them.

MVI: Would they still be part of that scene? I mean, I know they were
like, in the mid fifties, they were like…

SP: They were still there.

MVI: For example, John Cage and Merce Cunningham were close.

SP: Yeah.

MVI: But they were moving away from that sort of art and maybe also
those artists.

SP: They didn't move so far away.

MVI: Because very often, it seems as if the abstract expressionism was
not rejected because there’s always been like a complex relation to
it…one of respect, admiration.

SP: I think the artists themselves started making really a lot of money
and started living out of Manhattan.

MVI: That was the point where their work became very expensive. Late-50s
was like expressionism, high market value.

SP: Yeah, they started traveling more, they started…yeah.

MVI: But still, these would have been artists that were part of your…of
that scene?

SP: Yeah.

MVI: So interesting.

SP: Barney Newman. Larry Rivers. There was…yeah, they were quite
congenial. They hung out a lot together. They were all, you know, into
booze and drinking and needed mutual support. I mean, they were also in
galleries, and galleries also means that the artist has to fight for
their rights and things like that. So, they needed mutual support and…

MVI: The abstract expressionist gallery circuit I read, was more located
on 10th Street, 9th Street…

SP: And uptown.

MVI: And uptown.

SP: Sydney Janice, 57th Street, sort of…

MVI: How did you look at those works? What sort of…

SP: With complete incomprehension.[laughing]

MVI: Yeah, it’s…it was your first…

SP: I mean, there were these currents in the society, that was… that,
there were the beatniks, there was this new literature and new music,
that's, you know, jazz. That all seemed to be inventing themselves
completely anew, and one, just, you know, it was a new event in the
culture, and this is post-war, right? So there's a lot of money. There's
a lot of pride, and whatever people want to do was of great interest.
You know, in terms of abstract expressionism, for instance, you know, a
completely different way of doing, thinking, painting.

MVI: You looked at it with incomprehension. What did you get out of it?

SP: Kind of an invitation to the moment, to mysticism, to the kind
of…yeah, like, to the work of Jackson Pollock, you know. What do you do
with this if your, if your mind is based on paintings and etchings of a
more classical sort, and suddenly you come upon Jackson Pollock? You're
invited in to a way of thinking about art that is quite different, quite
a different level of thinking…into quite different ways of evaluating
it, you know, assessing whether it's something remarkable or something
stupid, you know? All of those questions are very strong, and they still
exist in the modern arts, you know. Some people will not trust this now
quite established trend in the culture, but…

MVI: Did it make you want to read about it, like art criticism?

SP: There was a lot of art criticism available. There were magazines,
and critics were quite strong in the scene. So, it was clear that
painting was developing a vocabulary and a way of assessing itself that
was much more developed than dance, for instance. What else about that
moment? I liked to read. I was still reading a lot. Mostly nothing to do
with New York or the art scene.

MVI: With what?

SP: Oh, semi-classics, you know.

MVI: Literature?

SP: Literature. I guess I was beginning to ask myself some basic
questions about how art gets made, you know. What is ok to do? You know,
a kind of sense of…that you get when you're from the edges of a culture,
as Arizona is. You get these questions of what is the proper…? What is
what is proper to do? What is, you know, what is considered the right
artistic behavior?

MVI: What are the implicit codes?

SP: Yeah, that kind of thing. What exists here? Why is this a famous
city for its art? You know, what…I found out a lot more about that in
the next 12 years.

MVI: I'm really struck by the fact that abstract expressionism had still
this attraction.

SP: It was very powerful then.

MVI: The later estrangement from abstract expressionism, maybe people
have kind of projected upon a time that was much more ambivalent, or
much more…

SP: What do you mean?

MVI: In the 60s, more and more, you will have criticism of the abstract
expressionist aesthetics, and since that happened, probably people dated
a little bit too soon…

SP: Its demise you mean…I think so. Because nobody knew that
Rauschenberg and Johns were going to take off, you know. I mean, they
were groomed for that role, but nobody knew that it would become so…

MVI: Different?

SP: Strong. And then pop art came almost immediately. Something like
’64, it was clearly there, and nobody could’ve predicted what that would
become this kind of ‘reverse high art’ into the popular culture thing,
that happened, and yeah, by the middle '60s, minimalism had appeared.
There were all these new genres to be criticized and to be compared and
all that, and I think that had a lot to do with the kind of
disappearance of abstract expressionism from the press. But the artists
were still strong and commanding enormous prices. In a way, they had
escalated to the point where they were no longer public, or as public as
they had been when they were establishing. So, I got there at the end of
the period when they were establishing, I think, but they were still,
you know, behaving as they always had.

MVI: Sometimes, they say that people like John Cage and Merce
Cunningham, although they would find themselves with the abstract
expressionists, they were sharing the scene, you could say…

SP: Yeah.

MVI: But they always felt removed from a particular…like the figure of
the artist that abstract expressionists somehow cultivates…a little bit
macho, a little bit existentialist, in pain or in torment, the brave
solitary figure who faces the world as he faces his canvas, and you
know, the struggle with the forces, elements, material…

SP: Colors.

MVI: Colors, the gestural temperaments, the girlfriends, the booze, the…

SP: Yeah, yeah.

MVI: So, it has a…it not only has products, artworks, but it has a
behavior. It has a philosophy. It has an attitude to it.

SP: But they weren't painters, so they didn't have to do that. They were
something else. But I mean, it wasn't just sharing the scene, they
played with these guys. There are famous stories about John going to
play poker with the painters, so that they, Merce and John, could have
enough money for supper.

MVI: I never heard that one!

SP: Yeah, yeah, Merce would wait out on the front step, and John would
go in and join the poker game and earn enough money for supper, and then
leave, but I mean, they were part of the social…

MVI: Yeah, That’s…if you can rip someone off, then you’re
close. [laughing]

SP: You’re definitely right there.

MVI: Yeah.

SP: It also suggests they weren't such great poker players, too, doesn’t
it?

MVI: The alcohol was already in the…game…

SP: Exactly.

MVI: Yeah. So, you saw those works—paintings—incomprehensible, yet,
intrigued…

SP: Well, have you ever seen a Franz Klein from that period?

MVI: Yeah.

SP: That's one of the few times I was in a gallery down in that area,
you know, down on 9th Street, was for his show. And I walked in and
there are these black and white things…I think one of them was for
Merce. I think that's why I was there.

MVI: Yeah.

SP: To see this painting for a dancer. But I mean, they're just
calligraphies, so what do they mean in American terms? How can an
American kid from the cultural periphery assess this thing at all? See,
yeah, I had an aesthetic blank. I had a big aesthetic blank looking at
all their works, and I'm sure that was good for me. You know? To have an
aesthetic blank instead of feeling like there's some answer, a
philosophical void to fill. I quite like Franz’s work.

MVI: I think it’s very strong work.

SP: I think it's great work. But I mean, we say, with hindsight, I can
say that. But at the time, I mean, it filled me with a lot more positive
feelings than de Kooning did, for instance, with his acid colors and his
distorted figures. Barney Newman, I liked very much. He seemed to be
painting essays about the space between buildings. I definitely felt it
was a cityscape that I was looking at. It wasn't anything abstract at
all. It was a representation of something urban and real. Anyway…

MVI: Just thinking, this is round about ’61, isn’t it?

SP: Yeah.

MVI: Does the…did the name Clement Greenberg…

SP: The critic?

MVI: Yeah, did that…

SP: I knew who he was. I didn't read his work.

MVI: Yeah. Have you ever?

SP: I feel like I have. I can't think of a title to give you, but yeah,
there were times later when I was more, when I knew the artists better
and heard them talking about different writings that I would follow
their leads. And Greenberg was, of course, a major theoretician.

MVI: Of the modernist…

SP: Yeah, and particularly of the abstract expressionist…

MVI: And he kind of valued it for pushing a limit and looking for the
essence of a medium. So, if the medium is painting, he thought that the
essence, and if you reduce it to that essence, it’s flatness.

SP: I think he was very interested in dropping the Renaissance
perspective tricks from painting.

MVI: And in moving all the optical illusions until you really go to the
medium.

SP: The paint, the flatness, the surface, and the paint…

MVI: Yeah. Was “flatness” a concept?

--- SECTION: White Paintings --- [36:05]

SP: Flatness was a preoccupation of so many painters.

MVI: Yeah.

SP: They called it “the picture plane.” And they were all preoccupied
with…the main riddle seemed to be, what to do with this new flatness
that they had invented or re-found?

MVI: How flat can you get? Or how…

![]({attach}white_painting_small2.jpg)
:   Robert Rauschenberg, White Painting [two panel], 1951, House paint on canvas, 72 x 96 x 1 1/2 inches; overall (182.9 x 243.8 x 3.8 cm), Robert Rauschenberg Foundation, ©Robert Rauschenberg Foundation.

SP: Yeah. So, for instance, Rauschenberg’s *White Paintings*, you know.
For instance, the joke that Jasper Jones said about putting paintings on
an airplane and flying it around, and calling it “The Picture Plane.”
[laughing] They…yeah, it was a big preoccupation. This idea of the
canvas and what it was for this generation.

MVI: The paintings of Rauschenberg, *The White Paintings*. Have you ever
seen them?

SP: Mmhmmm. [Yes.]

MVI: Because what I wonder is, whether they were…whether it was just a
blank canvas, or whether it was covered…?

SP: It was covered with white paint.

MVI: By paint. I see. Interesting. I hoped it was the white, just the
canvas.

SP: The canvas. Canvases are not necessarily white. I mean, they can
come pre-primed, but, anyway, he painted them.

MVI: He painted them. There were several of them, weren’t there?

SP: There were seven of them.

MVI: You would probably have seen it in the studio.

SP: No, I saw it in, at Castelli gallery.

MVI: Let’s see if I can…because they were older. They were from ’52 or
’51.

SP: Yeah.

MVI: He had even used them already in the first theater event at Black
Mountain College, so I was just wondering whether it…they were
re-exhibited or…

SP: Yeah, they must have been, or they must have been there in the back
room.

MVI: Yeah.

SP: Unhung. But I seem to remember seeing a show. Now, there wasn’t…what
was it? ’63. The retro…the Rauschenberg retrospective. So I certainly
saw them there.

MVI: Yeah. In one of the original exhibitions, they were exhibited, in
the ‘50s, that must have been, along with black paintings.

SP: Yes.

MVI: Black paintings were black paint on top of newspapers.

SP: Yes.

MVI: *The White Paintings*, then…

SP: …were just…

MVI: They must have been together in the same space somehow.

SP: I don't know.

MVI: What did you think of this piece? *The White Paintings*?

SP: I guess I just saw it as the perfect answer to this “picture plane
problem,” you know, and false illusion and philosophical statement, in
paint, a number. There are seven paintings, and they go from one panel
to two panels to three panels up to seven panels.

MVI: They build up.

SP: Yeah, they build up numbers of *White Paintings* per white painting.
And so, that seemed like some kind of numerical statement.

MVI: Ameri…?

SP: A numerical. And I asked Rauschenberg later, why seven? And he said,
that's the biggest number you need to do to suggest…

MVI: [simultaneousely] …infinity.

SP: [simultaneously] …infinity.

SP: Infinity. So, I mean, those are the kinds of things I knew about
these paintings, and I like them very much. I found them very troubling
because of the lack of imagery. On the other hand, there was imagery
there because there were shadows. There was reflection and shadow, so
yeah, it struck me as being another Eastern influence on Western
[art]…with this “picture plane problem” thrown in and solved.

MVI: The thing with the “picture plane problem” is that there's like
several tendencies happening at the same time. There’s the expansion of
the canvas, or the…

SP: The size, you mean.

MVI: Sorry?

SP: The size.

MVI: Yeah, and then from the size, even leaving the wall…protruding…you
get assemblages, and then suddenly, it even expands into the room.

SP: It loses flatness.

MVI: And it loses flatness, so there’s objects, and there’s
semi-sculpture.

SP: Yeah and in the Rauschenberg “combine thing”…where all kinds of
things were attached.

MVI: There's like either, nearly, like going to the ultimate flatness,
and yet also going to leaving flatness altogether.

SP: Yeah, yeah. I mean, they're all dealing with the same

problem: how to reduce illusion. And how to create a new medium at the
same time, cause so much of painting had relied on the illusion of
perspective. So, I mean, you saw paintings with paint like cake
frosting, you know, really thick, squirted right from the tube, kinds of
elements, and the *White Paintings* and sculptural paintings, where the
surface was torn. From Europe, Fontana was slashing painting canvases.
Yeah, the canvas was being treated as a material. Not just a kind of
surface to put the painting on to. It was being treated as part of the
material.

MVI: It's good to say, to me, that it's actually not a contradiction
that those things happen at the same time, or that they deal with the
same…

SP: Issue.

MVI: Issue.

SP: I think they do. I think. Or they can be grouped into that. Yeah, I
think so. I don't see why not. There are probably other issues, as well,
but getting some grip on a reality that had been reconfigured, a new
paradigm had come, and they were trying to figure out what, you know, it
suggested.

MVI: How flat can one get?

--- SECTION: Literalist art --- [42:38]

MVI: I had another question, but it’s gone. Ramsay Burt, he wrote like a
paper-in-progress, and he really makes a connection between the work of
Jasper Johns and your work. Maybe because…Jasper Johns, in terms of
imagery…he found a way to both include imagery, and yet be literal.

SP: Yes, yes. I think that's true.

MVI: And a flag. Painting or writing a word is not painting a word, it's
writing a word. You actually…

SP: Yeah.

MVI: Painting a flag with the same proportions is not representing a
flag, but it is a flag.

SP: Yeah.

MVI: So, this…

SP: Yes. That would have appealed to me. I mean, it did appeal to me.

MVI: Can you tell something about that appeal?

SP: Oh, I don't know, but I think it exists in my mind to this day, as a
place of real balance between what's represented and what the
representation is. When you represent something, you do change it. That
is the nature of the thing. And maybe this is a particularly dance issue
of this century, but a lot of dance was reproduced. So, to find out if
there was a possible way to not reproduce dance, but to make dance.
Maybe that's the question that his aesthetic raised.

MVI: Yeah. So what would be like an equivalent to…

SP: To a Jasper Johns?

MVI: Yeah.

SP: [laughing] Oh, perhaps *Flat* is a good example since it's a
well-known, fairly well-known example from my early days. But the whole
problem of pedestrian movement is a bit based on that question: how can
you be both real, and performing at the same time?

MVI: Yeah. You can’t.

SP: How can you not…

MVI: Represent walking without walking.

SP: Yeah. Exactly. You walk, although it's changed by being in
performance, I have to say. We have to look at the issue of performance
as being different than showing a painting or even playing music, you
know. To have your entire body scanned for all it’s messages at once,
you know, and quite intensely, is something that…

[phone rings]

…sort of makes everything unnatural about you, because you don’t
normally…

[phone rings]

…work under those, or exist under those circumstances. Only a few hours
a day, at most, you know, is it like that.

[beep for voicemail recording]

MVI: Someone hung up? I think the other connection he saw with Johns was
maybe in the titles, the self-reflexivity.

SP: Johns’s titles aren’t self-reflexive, are they?

MVI: No the flag, you know, it being what it…you know, this kind of
self-reflexivity in the imagery.

SP: There was one title that I very much admired of Jasper’s, which was
*Zero through Nine*.

MVI: Yeah. *Zero to Nine*.

SP: *Zero through Nine*.

MVI: Is that…what…the painting with the numbers?

SP: It's a painting with numbers all…it’s a small painting, and all the
numbers are done one on top of another, so *Zero through Nine* is both
the way you can describe a sequence and the way you described viewing a
situation where you have nine. What would you call them? Nine separate
images…that you compress the view into just one square, you know, for
all nine. So, the complexities, the graphic complexities between the
numbers, create the spaces of the painting. I like the painting, too,
but I very much like the title because of the double…

MVI: The double…?

SP: Yeah, it's reflexive in a way. The difference between the
reflexivities, maybe I should talk about, because it seems like there is
some kind of difference being made here, that we haven't been very
clear. Jasper’s titles referred to the paintings. My titles refer only
to themselves, of that kind of title. There are other categories and
titles, but *that* kind of title.

MVI: Within that series of titles.

SP: Yeah.

MVI: But the title is a funny thing because it's like the frame of a
canvas. It's already part of it and yet stands outside of it, and so, it
can play both ways.

SP: Yes, yes.

MVI: It can say like, “I'm not the piece and yet you are.”

SP: It can be the piece. It can sum up the piece in some way.

MVI: Yeah.

--- SECTION: The flatness of Flat --- [48:42]

![]({attach}P54.jpg)
:  

![]({attach}P52.jpg)
:   Steve Paxton performing solo piece *Flat* (1964). Photograph Collection. Robert Rauschenberg Foundation Archives, New York. Photo: Unattributed, 1964.

SP: So, *Flat* is a good example of a title which I consider
self-reflexive in that the print is flat on the page, and so, the word
itself was flat. It is, by having been printed, you know…that it was on
a program meant that it would be part of the flatness of the paper. At
the same time, in the performance, I think it very much influenced the
nature of performing that work because, in fact, that's one way you
would describe the behavior of that dance, is that there’re no highs and
lows of personality display. So…

MVI: Since we talked about the flatness in the pictorial plane… […]

SP: It was not described as flatness so much as it was described as the
problem of the “picture plane.”

MVI: Yeah.

SP: Or, dropping illusion or dropping perspective.

MVI: Yeah.

SP: So, the word flat was not…that. It wasn't that current. I, the…when
I first got the word, I just felt like it was a wonderful word to
describe a kind of performance style that was quite used by all of us,
this blank face kind of style. And then I realized that it had this…that
I could use it as a title in that series because it…like “English” is an
English word, it was self-reflexive. So, eventually, I found…first, I
thought of the title, and then the dance…

MVI: Came.

SP: Came, and then I applied the title to the dance, you know.

MVI: I think that if you would make like a history of the concept
“flat,” “flatness,” like in art history, at a certain time, it would
just really have this Clement Greenbergian ring. It’s about the
reduction of a medium to its proper self and that sort of culminates in
the flatness of the canvas, but that as the ‘60s moved on, it more and
more was attached to an attitude more than just to being a property…

SP: To the physical canvas.

MVI: And then attitude then was called a “sensibility.” Or “an
attitude,” as you say, performance style reflects attitudes, and that's
an important shift somehow in terms of…

SP: …flatness…

MVI: …flatness.

SP: …what it meant. Just, I mean, it's important to note that these
ideas change. That we don't hold on to a certain definition for very
long. We keep it while useful although it can be centuries, too, you
know…centuries of undeviating allegiance to a property.

MVI: But when *Flat* was made…though that already had…that shift had
already materialized a little bit in writings about a new sensibility,
and just referring to the abstract expressionist caricature I gave, like
the macho and gestural and temperamental, you could say that the new
attitude was very opposite to that the blankness, lack of effects or
don’t reveal the effect. So, people were trying to find ways of
describing that or naming it, and then you would have like something
like, “cool,” the “cool” came out, or “cool arts.” So, it’s interesting
that you, that I think your piece is very much a piece of its time, as
well, and that it really is a very good example of a new sensibility or
a new attitude, but I definitely wouldn't compare anymore with abstract
expr…temperament, or whatever.

SP: No, not at all. I'm trying to figure out how to phrase this exactly,
but yeah, it seems to me like it…I just wrote to somebody in an email
that artists either have to have someplace to go, or they have to have
something to show off from, and I think that *Flat* is one of those
pieces where I use the prevailing aesthetic as I understood it to, to
push away from and find out what I had to do to maintain that position,
which basically, since I had no goal to make something different, I
had…I employed very little. I employed as little as possible. What… How
little could you do? How little could it be, you know? Well, it was
walking, and it was standing and sitting. It was a few almost like
little islands, you know, in a very quiet sea, images, and the clothing
event…the undressing and dressing. Putting it on my body made it into a
slightly surreal event, but I was aware of the strip-tease aspect of
actually disrobing on stage, and I was also aware that, you know,
having, at that point, danced in just a G-string, you know, and
remembering the kind of humiliation that felt like afterwards, and it
just felt so, exposed as one is [laughter]. So, I realized that the
hanging on the body was going to solve that problem, that I wouldn't
feel exposed, and it would be a kind of commentary. It would be a kind
of practical solution to what to do with the clothes when you take off
your clothes. I mean, I saw all that work as humorous and nobody else
did, and that was [laughter]…caused a great division between myself
and my public, I have to say.

MVI: Yeah, well, you’re getting closer. The gap is getting…

SP: The public is finally getting my sense of humor.

--- SECTION: Minimalism --- [56:33]

MVI: Minimalism. When did you first become aware of the fact that it
could also be like a new aesthetic or…

SP: It didn't apply to dance. It didn’t apply to dance.

MVI: Maybe not even thinking about dance now, but like in visual arts,
when did it strike as…

SP: With Robert Morris and Donald Judd and a few other people, a few
other artists…

MVI: Robert Morris. You knew him from really early ‘60s, yeah?

SP: Yes. I don't recall when I met him, but I met him when they were
married.

MVI: They were married when they came to New York.

SP: Mhmm. [Yes.]

MVI: Simone said that Robert Morris was a friend of Robert Rauschenberg.

SP: Yeah, they did become friends.

MVI: Yeah and that, that's how she met you, so if…

SP: I don't think that's the case.

MVI: You’d come together…

SP: I don't think that's the case. I think we met at the Cunningham
studio.

MVI: Yeah, in a class.

SP: Mhmm. [Yes.]

MVI: Robert Morris…it took a long time, like until 1963 before he could
contribute into an exhibition, and that wasn't a solo show. It was a
group exhibition, and prior to 1963, he showed his work in more like a
fluxus event of Yoko Ono[^Ono], or more like a performance[^Ono2] context in which he
would contribute an installation or something. Have you seen those early
works?

SP: The first works that I was aware of were *Gray Paintings*. So, they
were a lot like the *White Paintings*, but they were gray paintings.

MVI: *Gray Paintings?* Yeah.

SP: They were canvases painted gray.

MVI: Yeah, like monochrome?

SP: Mmm…

MVI: Monochrome?

SP: Yes.

MVI: Well, that’s good. I can look for them. I know he has boxes and
stuff and a lot of those things.

SP: But, yeah, so he didn't strike me since there were the *White
Paintings*, already. He didn't strike me as such a strong figure. But
then, he started making felt pieces, which were the same color. They
were gray and sliced up in various ways and made into marvelous, fluid
fabric.

MVI: They hang.

SP: Yeah, they hang. Sculptures.

MVI: That’s late 60s, that’s like 1968 that he, after his minimalist
period, actually…

SP: And that's not considered minimalist anymore?

MVI: Well, for him, it was already like a critique because he thought
that maybe minimalism still had like, a-too-strong-a-formalism in it, so
he was looking for what he called ‘anti-form’, and by focusing really on
the materials as such, and materials that you just don't have any
control of because they…like felt, you know, it hangs. It’s not that it
doesn’t have a form, but it’s not composing that form to such an extent.

SP: It can have a recon…like a…

MVI: It really became more and more like putting dirt in the gallery
and, and that changes every day, and so it would become more and more
abject and really away from the pure aesthetics of the minimal objects,
and he also wanted to remove, to move away from the objects’ solidity,
and the…so, yeah, so that's why I was thinking it was…it’s later, later
on. But is that what you're saying? That you remember these gray
monochromes? And then you go to later — really later on — and you go to
the felts? Because in between, you have all these pure, minimal boxes
and cubes and L-beams and…

SP: Which I saw, but I mean, I didn't find them that stimulating. I
mean, they're not meant to be stimulating in the normal way, anyway.
They're stimulating for the lack of stimulation, you know, kind of
thing.

MVI: Isn’t that similar to the little, when you say like that was…

SP: How little could I…

MVI: Yeah.

SP: Yeah, it is.

MVI: Because that was so troubling to many people, like it’s…

SP: It still is!

MVI: Near to nothing. So how am I, how I'm supposed to look if there’s
no…

SP: That's the problem.

MVI: But still that didn’t…even though you had like a similar interest
in that proposition…

SP: I can't remember ever discussing with him.

--- SECTION: Rainer and minimalism --- [01:02:06]

SP: I thought one of Yvonne’s works, *Parts of Some Sextets*[^Parts], was very
much influenced. She was at that point, living with Morris, very much
influenced by his theories, and I thought that was a little bit too
loose, you know, in terms of her own aesthetic that…why was she taking
on these…

MVI: Why would she apply it or why would she take?

SP: Yeah, why would she take somebody else's work and apply it to dance?

MVI: How did you feel about her manifesto? Could you relate to that?

SP: I thought it was a big mistake.

MVI: You thought at the time?

SP: Only because it was a manifesto. Yes. At the time. I thought, boy,
she's gonna have to live with this for the rest of her life
[laughter]. Because, you know, manifestos are known to hang around.
They don't just disappear, you know. You make a manifesto, then 50 years
down the road, people are still saying, “And you made this manifesto!”
Yeah, it was a manifesto, of hers. And, yeah, I thought it was a
mistake.

MVI: Could you relate to some of the ideas that were expressed in it?
Maybe you didn’t like the manifesto but you would, you would…

SP: I saw that she was trying to talk about what she thought, you know,
she did try to think those thoughts and, you know. Thing is, that, all
the things that she says “no” to at the end, I thought, were really
quite strong in her work. […]

I mean, I said I didn't really approve of her doing it, you know, which
is none of my business, and that I thought that it was a mistake. And at
the same time, now that I'm older and wiser, and I've seen a bit of the
art world and a bit of the dance world, it seems very clear that things
kind of flow from one thing to another, and that great leaps are made by
one art form because they absorb the ideas from another medium, you
know, and apply them, and it gets more and more clear when we get into
multimedia. As though, as I’ve said, as though, we didn't already have
multimedia in opera and theater, of various kinds.

Or, the Experiments in Art and Technology Organization, you know. It was
deliberately trying to bring ideas from one medium into and to another,
indeed, from the arts into the sciences, and the sciences into the arts.
They are deliberately trying to mix them, the two things. So, I see now
that that's really pretty much normally the way things work. Yes, Yvonne
is, and was dogmatic, I mean, she has very clear views. That was
actually quite refreshing that somebody had views and was stable enough
to get them out. I didn't have any such clear views.

MVI: No?

SP: No, and I don't think many of us did, at least not things we were
ready to make public and, you know, considered part of our career. Hopes
and ambitions, but everybody had those in our circle, you know. Mystical
leanings, you know, toward one way of making art or another but not
anything that you could proclaim.

--- SECTION: Mystical leanings, surrealism and Magritte’s puzzles --- [01:06:00]

But I was going to say about *Flat* and about other aspects of my early
work, that I did have mystical leanings, and I was interested in ideas
of the void and the kind of emptiness that we assume exists in some
people's philosophical or religious discipline, or in space itself, some
place or, you know, emptiness, void, the ultimate mystery, the ultimate
ambiguity kind of thing, ultimate, you know, a youthful kind of
philosophical leaning, which made me want to make my pieces in a certain
way, and *Flat* has somehow more to do with that. How, I mean, it says a
lot about what I thought the other, the normal dancing was like. That it
was incredibly three-dimensional, heightened, performed to a ridiculous
degree. Nowadays, I mean, overboard, it would be…most of the modern
dance performing was ultra-pushed, dramatized. So, for instance, the
Cunningham performance mode seemed quite refreshing, that they didn't
try to make the audience feel things and think things or notice things.
They sort of just did things, and let the audience watch. It was a big
shift in audience protocol, and that was going on as well with the
painting and as well with the music, and a lot of it based on Cage’s
writings and lectures and, you know, just what he was doing. That's the
kind of thing though that I think I would tend to avoid. I tended to
avoid other people's aesthetic. I was trying to forge one of my own. 

The person that I felt a strange affinity with was this anthropologist,
Birdwhistell. He was at the Museum of Natural History in New York, and he’d
written about walking, and there was something there. I also felt a lot
of affinity when I was introduced to Muybridge, and the photography of
walking and running and ordinary actions, and how it somehow seemed to
turn into a kind of little performance there, just in the photographic
plates of things that were both real and performed. You know, again,
that interest in that dichotomy. I was interested early on by
conversation that I heard painters having, and Duchamp, and again, this
real art quandary and…

MVI: The found object.

SP: The found objects, yeah, that kind of thing and the, the work of…

MVI: Magritte.

SP: Magritte. How did you know? I must’ve told you already.

MVI: Well.

SP: I was gonna say, who is that Belgian painter? [laughing] You know,
with his apples and his ordinary objects and unusual juxtapositions. So,
if you talk about a painter that would have influenced me…Jasper Johns
definitely, but Jasper just was somebody alive and around who was
articulating something that was already quite strong in the art world
through the century, which had to do with…maybe it has to do with
surrealism. So, I feel like *Flat* is more a surreal piece than it is a
minimal piece. It's that figure that walks around in a bowler hat in
Magritte paintings, you know, and what he does in time as opposed to
just the one slice that a painting gives you.

MVI: Yeah. I read Suzi Gablik’s book on Magritte[^Mag] some time ago, and I
was very struck by these ideas, also. I didn't know that much about…I
mean, I know his paintings, of course, as a Belgian… [laughing]

SP: You must. Yeah, it’s obligatory.

MVI: He thought of his paintings as philosophical riddles. He said he
wasn’t so much into enjoying the paint and the smell of the brushes and,
you know, the whole thing. He wasn't also interested in painting that
much. He waited for something to paint and to have a certain kind of
juxtaposition that would be like a puzzlement, or maybe visual poetry.

SP: Visual puzzles.

MVI: Or visual paradoxes.

SP: There's some kind of a relationship with Escher, which I can't quite
pin down, but it’s something. Again, it's about the plane, isn't it? In
a way, Magritte is playing with perspective, but he's playing with it.
And Escher is doing two-dimensional graphics of three-dimensional
objects that, because they're actually only two-dimensional
representations, can be tiled or twisted in some way.

MVI: True. Magritte has many of the paintings that fit into the
landscape, so you don't know whether…

SP: …a painting in front of a window of the scene, outside the window
kind of…

MVI: Yeah, yeah, so, really, questioning representation.

SP: Yeah, yeah. So this was all part of what Greenberg was on about,
too.

MVI:  How would you have had access to Magritte? Through books?

SP: Books and posters at first, I think. Then, I found out that Jasper
and Rauschenberg were interested in him and Cage.

MVI: I didn’t know about Cage…that he had an interest in Magritte.

SP: Yeah, he…well, he was interested. I don’t know how deep it went, but
I mean, yeah, there's a kind of Satie/Magritte side to Cage that isn’t
particularly strong but that he felt some sympathy for. Cage just liked
composition, you know, and he - I'm sure - saw what Magritte was up to
as something different than, and aside from normal art. Anyway, and then
Susie, whom I also knew…

MVI: You knew her?

SP: Yeah, and she had lived with Magritte and all that. She had
Magrittes.

MVI: No, really?

SP Yes. Yeah.

MVI: So, you saw real…

SP: …Magrittes, yeah, hanging in somebody's house kind of thing.

MVI: That's a great way of seeing a painting, actually.

SP: Oh, boy, I mean, then you see what a painting really does because
there it is. Even the Morris *Gray Paintings*, you know, you put them
into a formal house or anybody's house, and it…they say something very
powerful, than in the studio or in the gallery, you might wonder if
there's anything to be said, but they say something very powerful.

MVI: Do you remember what paintings she had?

SP: I do remember that she had a triptych of a nude.[^Trip] I think it was a
torso. So, she had breasts and hips and legs or something…basically, the
knees. I don't know if she had others. I can't remember, but she would
have had pictures and things like that, and she would have discussed…

MVI: Did you ever talk to her about Magritte, since she had lived
there, or about writing the book? The book appeared like, 1971, or
something.

SP: Oh, really? That late?

MVI: She must’ve been in the process of [writing the book].

SP: Perhaps. I don't know when she started the book. No, she just
described, you know, having shown up on his doorstep, and lived there
for a while, and how nice he was and how much he’d helped her and all of
that…not so much about the theory of, you know, what he was painting.
But the work was interesting. I like the puzzles, the paradoxes that he
proposed.

Now, of that kind of thing, I never found an equivalent in dance and
didn't feel like…but I did feel like, in a way, I was stealing that
figure, or that figure related to the figure in *Flat* and *Proxy.* I
guess that performance mode came from several directions because it
definitely came from Cunningham as well, but Cunningham, of course, had
me leaping all over the place, and that wasn't the movement I was into.
Still, the blank persona, or the unknown persona…something about that
interested me.

MVI: Something very intriguing or enigmatic about it…opaque.

SP: Yes. My assumption in the early days was that everything was
perfectly obvious, so you didn't have to say very much to have said a
lot. My assumption nowadays is that things are not very obvious and that
people are all incredibly different and to make an assumption that we
all know the same thing is a big mistake, you know. It's very rare that
you actually have that kind of communication with somebody. But I knew
fewer people then than I know now.

[^Mail]: Ray Johnson (1927- 1995) used mail art as a medium, since the early 1960s. By way of personally sending small scale artworks via mail, he proposed a radical democratization of art making and spectatorship. He was the founder of the New York Correspondence School, one of the most famous mail art structures and figurehead of alternative social networks at large. “By concentrating on mail art, with its invitation to alter the work by whomever receives it, he refused to create a body of work that exhibited a narrative arc of his own “development.” That productive repudiation carries forward the only motive of the avant-garde that really matters anymore: undoing Western culture’s ever-widening separation of one’s life (living) from one’s art (making).” <cite>Tim Keane, *I Is an Other: The Mail Art of Ray Johnson*, Hyperallergic.com, August 22, 2015.</cite> 

[^Aeon]: *Aeon* by Merce Cunningham premiered on August 5, 1961, in Montreal, Canada. Its music, *Atlas Eclipticalis with Winter Music (electronic version)*, was composed by John Cage. Robert Rauschenberg was responsible for the set, light, costumes, and small performative events that occurred during the performance. Its original cast included Shareen Blair, Carolyn Brown, Remy Charlip, Merce Cunningham, Judith Dunn, Viola Farber, Steve Paxton, Valda Setterfield, and Marilyn Wood. 

[^Quad]: The quadrille is a square dance, traditionally performed by four couples. The five sections of the dance each had a specific set of prescribed figures. It was particularly popular at the end of the 18th and the beginning of the 19th Century in France and England.  

[^Ono]: Yvonne Rainer describes Robert Morris’ evening at 112 Chambers Street as follows: “The final evening at Yoko’s loft was announced as “an event” by Robert Morris. George Sugarman and I traipsed downtown and up the five flights expecting some kind of performance, only to be met, on opening the door, by a three-foot wide curving corridor with seven-foot 197 high ceiling that ended in a pointed cul-de-sac. I was so outraged that 1 wrote on the wall “Fuck you too, Bob Morris.’” <cite>Yvonne Rainer, *Feelings Are Facts: a life*, The MIT Press, 2006, 200.</cite>

[^Ono2]: “I make a 50-foot long plywood *Passageway*,which narrows as it curves. Two arcs of a circle converging. I wedge my body between the narrowing walls, which curve ahead and out of sight. I am suspended, embraced and held by my world. I listen to the faint sound of the hidden mechanical heartbeat I have installed over the ceiling of *Passageway*. There is nothing to look at here in this curving space which diminishes to zero. In this blind space whatever constitutes the ‘I’ of my subjectivity evaporates and I think of that other remark of Wittgenstein: ‘The subject does not belong to the world: rather, it is a limit of the world.’ Others who visit *Passageway* leave messages written on the walls such as ‘Fuck you too.’” <cite>Robert Morris in “Simon Grant interviews Robert Morris” in *Tate Etc.*, issue 14, Autumn 2008.</cite> 

[^Parts]: *Parts of Some Sextets* by Yvonne Rainer premiered at the Wadsworth Atheneum, Hartfort, Connecticut on March 6 in 1965. The piece was made for 10 performers and 12 mattresses and was divided in 31 acts. Its cast members were Lucinda Childs, Judith Dunn, Sally Gross, Deborah Hay, Tony Holder, Robert Morris, Steve Paxton, Yvonne Rainer, Robert Rauschenberg and Joseph Schlichter. Rainer describes the score of her piece as follows: “The chart, reading down, lists 31 choice of material; reading across, numbers consecutively thirty-second intervals 1 thru 84. The piece is as long as two sheets of 22 x 17 inch graph paper allow with one-half inch of ruled space equivalent to thirty seconds. The chart is divided into squares, each indicating the juncture of a given piece of material with a given interval in time. The physical space of the dance – where the material would take place – was to be decided by necessity and whim as rehearsals progressed.” <cite>Yvonne Rainer, *Work 1961-73*, The Presses of the Nova Scotia College of Art and Design and New York University, 1974, 47.</cite> 

[^Mag]: Suzi Gablik in an American art historian, art critic and artist. The book being discussed here, in the interview, is: Suzi Gablik, *Magritte*, Thames & Hudson Ltd, London, 1985. About her work she says the following: “My work over many years has been to articulate the vital role of artists in society, and along the way, to create a new paradigm as well. […] I guess I’ve always been trying to deconstruct the cultural narrative which pins art to careerism, self-promotion, and the creation of “art stars,” and which is linked to the obvious gallery and museum nexus. I wanted to validate and expand the possibilities for art making, so that they could include many more options than that. This was not meant to negate the former, but rather to help those artists who found careerism a deadening path break out of the mold. I wanted to encourage them to feel validated if they work outside the familiar contexts.” <cite>Russ Volckman, “Art and the Future: An Interview with Suzi Gablik” in *Integral Review*, 5, 2007, 266.</cite> 

[^Trip]: René Magritte, *The Eternally Obvious*, 1948. 

