Title: The Inflatables in the Age of Plastic, 2019
Date: 24-07-2019
Voices: Steve Paxton (SP)

![]({attach}P1383.jpeg)
:    

![]({attach}P1386.jpg)
:   

![]({attach}P1382.jpeg)
:  

![]({attach}P1384.jpg)
:   Robert McElroy, Photos of *Music for Word Words* (1963) by Steve Paxton, Judson Dance Theater, Judson
Memorial Church, NY, 30 January 1963. © 2020 Estate of Robert R. McElroy / Licensed by VAGA at Artists Rights Society (ARS), NY. Courtesy Robert Rauschenberg Foundation, New York
     
SP: It is very interesting what in a lifetime one sees develop. When I was
born all food was organic, most things were fresh, and there was no
plastic, except for a few plastics that commercially were available like
Bakelite, a plastic made into radios. So many problems that we have now,
all happened in one lifetime. It's a very short time to bring the earth
to a halt. But it wasn't my fault. I was just there and tried not to
participate.

Admittedly, I did make these plastic pieces, inflatables, or you may
call them “erections,” “balloon art”…and that involved plastic. But is
an artist not allowed to make art to bring out the nature of what's
going on?

Polythene plastic came in roles of 8 feet wide and hundred feet long. I
was drawn to the inflatability of the material and that you could make
shapes with it and change your perception of the space. The first
inflatable was the piece *Music for Word Words*. During the piece a big
sheet of plastic transformed from the size of a room to a wearable robe.

 That's all what happened in the piece, except that it required some
business to make sure that the vacuum cleaner that was sucking the air
out of the shape did not inhale the plastic as well. So I was busy in
that room, and gradually it became small enough so that I could leave
the gymnasium through a normal sized door.

*Music for Word Words* was to make the music for *Word Words*, which
was a duet by Yvonne Rainer and me. *Word Words *and *Music for Words
Words *never met. The music was never played for the dance, still the
intent was there. The affiliation was announced: This is the sound for
the dance we did last night. It got here late, but here it is anyway.

 When the audience came in, the piece was already inflated with me
inside. Actually, I was a bit late, as the idea was that when the
announcement started of the performance, I was already a deflated thing
and left as one does when one rents a place until 8 o'clock and at 8
o'clock the performance starts.
    
What I particularly liked about the inflatables came out the strongest
in *Earth Interior*, which I presented in Washington DC in a skating
rink. Fancy new radio technologies can now identify voids inside of the
great pyramids where no one has been since it was first built.
Similarly, in *Earth Interior *the audience in the normal strata of the
alt seeing earth itself was able to see a tunnel inside the earth. It
would have been a horizontal tunnel about a hundred feet long, because
that was how long a role of plastic was; it had a tower at the end.

![]({attach}Peter_Moore_Earth_Interior_Ann_Arbor_1965small.jpg)
:   Peter Moore, Untitled [Steve Paxton's *Earth Interior*, Once Again Festival, Ann Arbor, 1965], 1965. Gelatin silver print (lifetime print). 11 x 14 in (27.94 x 35.56 cm). © 2020 Barbara Moore /Artists Rights Society (ARS), New York. Courtesy Paula Cooper Gallery, New York

My thinking is that you don't really think about the space in the same
way when you see a translucent, almost transparent shape through which
you can see objects. The shape is there, the shape is an aspect of it,
so it very definitely affects the environment and it very definitely
defines space and then disappears. I remember Carolyn Brown being inside
the tunnel, in a modest bathing suit, sunbathing on a bath towel in 50s
style. At one point, Trisha Brown and I went in and undid the tower,
which was essentially a tube with its end gathered and tied on the
inside. So she stood on my shoulders and reached up and unended that tie
and all the air came wooshing out at the other end with the plastic
flapping in the wind. There was enough air and air pressure for the
structure to keep its equilibrium and stay inflated. Other actions would
take place outside like Trisha and I were balancing stovepipes on our
shoulders like pencils, that basic game with the law of physics. I think
some *deposits* happened too, like batting a big inflated armchair in the
air with huge sticks and paddles.

![]({attach}Peter_Moore_Country_Club_Paxton_Hay2.jpg)
:   Peter Moore, Steve Paxton and Deborah Hay performing *Deposit* by Steve Paxton, golf course at Kutcher's Country Club, 1965. Photograph Collection. Robert Rauschenberg Foundation Archives, New York. © 2020 Barbara Moore /Artists Rights Society (ARS), New York. Courtesy Robert Rauschenberg Foundation, New York

The deposits was a concept. I was thinking I would make a number of
little units of materials, some of dance, some of costume, some of set
or prop, you know and combine them in different ways so that I would not
have to think of a new title \[every time\], I don't know why I thought
that was such a burden, but anyway, for a while I wanted to call
everything a deposit. The batting of the inflated arm chair, which I
used a couple of more times in other sequences, came from the first
deposit at the Kutcher’s Country Club with Deborah and I walking 20 feet
apart down a lawn with an invisible nylon fishing line attached to our
shoes. As we walked, the grass between us was trimmed by the string.
Deborah's was wearing a dress which had wire in the hem so that it
looked flounced or wouldn't blow at all times. I had painted a green
grass mat so that it would match the green of the lawn. In the beginning
I would lie under it. I cannot remember other elements.

I don't know, there must have been 10 or 12 different deposits. The one
in Ann Arbor, The Ann Arbor Deposit, happened on a small stage in a tiny
little disco. The inflatables had that adaptability too. Think how
convenient! In those days I could bring in a suitcase the set to fill
the whole space of skating rinks, armories… It was a whole capability
I was glad to have.

By then it was pretty much accepted that a work of art was a concrete
thing. Cunningham made *Events*, pieces consisting of sections that he
claimed could be reordered into new pieces, but in fact it was too much
rehearsing and work… It was a more casual Judson thing to tell my
companions that they could come and be part of it as a way of populating
the space in the way a park might be offered to people to come and do
what they want and you don't know what they will do. Anyway, I think I
was a bit preoccupied with trying not to do what I knew as the
conventions, which is often a young artist's preoccupation.

All the artists I liked - like Duchamp or some of the other painters -
seemed to me pretty radical rule-benders and I admired that and I wanted
to bend them as well. New York had an avant-garde self-aware, proud of
itself: the painters were famous and making money, the poets had the
Living Theater for instance available to read their poems at, people
were public with their art, plays were written, music was written, music
was performed, all of it quite unlike anything that had happened before,
like a whole new world. It was almost that our art of those times was
anything that did not look like any art that had happened. Art for the
future if not for the present. Definitely not from the past.

![]({attach}Peter_Moore_Physical_Things.jpg)
:   Peter Moore, Overall exterior shot of Steve Paxton in “Physical Things”, 1966. Vintage gelatin silver print. Paper: 10 x 8 in. (25.4 x 20.3 cm). © 2020 Barbara Moore /Artists Rights Society (ARS), New York. Courtesy Paula Cooper Gallery, New York

I stopped the exploration of the inflatables at the *9 Evenings* with a
piece called *Physical Things*. It was the biggest structure of all. The
audience could go in. I had been exposed to the off gassing of the
plastic to such an extent that even I noticed it and couldn't discard it
as “that funny smell.” I began to feel the effect of the toxicity and
the off gassing.

Many artistic materials in those days were poisonous. There were paints
that painters were using that were attacking their nerves. That's what I
thought I was getting into with playing with this plastic material. But
until that time, I had not noticed it particularly, or I had not
identified it as a problem, I just identified it as a characteristic
smell of plastic. Yeah, how long can you maintain your innocence if you
want to use a material that is actually toxic or incredibly pollutive?

So if you ask me when we started to become aware that plastic was a
cause for pollution… Obviously the scientists had some idea. At first,
they were considering it as a product, a very shiny new thing. They did
know the lifespan of the chemistry and how long plastic was going to
last in the environment, although I don't know if they were considering
it *in *the environment. The public maintained its innocence as long as
it could until pollution was apparent and they had to spend money to
clean up the trash in the cities, rivers and trees, and all that. Then
they felt guilty. Guilt came later than it should have.


