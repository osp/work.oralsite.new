Title: Score, 2001
Date: 06-05-2001
Location: Madbrook Farm, Vermont
audio: audio/steve-paxton/keywords/score.ogg
       audio/steve-paxton/keywords/score.m4a
       audio/steve-paxton/keywords/score.mp3
Voices: Steve Paxton (SP)
Order: 2

![]({attach}Paxton_Score_.jpg)
:   Steve Paxton, *Jag vill gärna telefonera (I Would Like to Make a Phone Call)*, 1964, printed paper, stickers, ink, gouache collage on paper, dimensions 45-3/4 x 24 x 4-1/4", Collection Walker Art Center, Minneapolis, Gift of the Robert Rauschenberg Foundation, 2013.

SP: So what am I…I’m just to bring up all thoughts that pertain…is that?
Score. I wonder about its source. I wonder what…I have no idea what
language it originated in. But I look at that “o-r-e” in it, and I think
of gold and I think of…I think it might be a word that comes from
minting coin and something to do with striking an image. I mean, to
score also then…if you scratched something, you have scored it. So it
has all to do with image making, and maybe something of iconography,
something of the icons that we use for graffiti, or for coin…

Well really, dance still doesn’t have a written procedure, although we
have a couple of great obscure notations. But we have become a literate
art in this century and also we have of course the help of the
[inaudible] and other image making things. But it occurred to me that
there could be a dance score, and I think people have worked especially
with video to make that happen.

When I was making the photo scores, I was trying to leave spaces in
between the poses. So the dancers had short bursts of events they had to
do very precisely, and in between those were sections that they could
either… I generally asked them to do it efficiently, to go efficiently
from one position to another. So I was not looking for how much of
themselves they could put into the space between events. But nonetheless
it was themselves. As opposed to what? As opposed to a self that they
were assuming, I guess. So clicking back and forth between kinds of
self. And when I’ve done the scores, I can feel that “clickage.” And
when you hit one of those poses it’s very much an icon in your mind,
it’s very much like the profile on the coin or the recapitulation, the
rescoring of… I guess in the arts we mostly use scoring for music, don’t
we? What an enviable thing they’ve arranged. Cause there…it
achieves…they achieve the printing capability through a true voice,
except for a few pieces that have to do without. I used to worry that
video and film were going to make dance obsolete, because it’s actually
quite a chore to go to the theatre and see a piece. And space is
expensive. But the more I think about an art using space, the more I see
it can’t be replaced. It can only happen in space. And if the other
scores are two-dimensional and ours can only really happen four
dimensionally, really…three dimensionally in space.

When you hit those iconographic moments in the photos, you have this
question of how do you face them. So here is a spatial question. It has
to do where the audience is. But also, where you are viewing it from.
So, are you going to reproduce it as you have seen it as a photograph or
are you going to put yourself into that person’s body and reverse the
image in a way? Or are you trying to get the audience…are you trying to
position the things so that the audience sees it as you saw it when you
first looked at the photograph? I don’t know if that’s clear. Anyway,
it’s a question, which disconnects the dance from the architecture, and
connects it to being seen from a point of view. And so it’s…say you have
several of those photographs to hit and you’re moving in space as you do
it and you are facing different directions… How does it work? From your
own point of view, there is always the question – and I think it comes
up with every photo – of whether you are reading it as a mirror image or
whether you’re reversing that image and being inside it and presenting
your front in that way. When you…oh, I know what it is. When you look at
the score, the score is up on the wall and you’re doing the movement and
you look at the score, you got your back to the audience. If the picture
in the score is facing out, presumably toward the audience, you’re
looking at that score and mimicking it, right, that means reversing the
actual position and you got your back to the audience. So you learn it
all that way, and then you turn the whole thing around, the whole dance
around, all the positions, which – as I understand it at this moment –
reverses all the imagery that you have learned in terms of what the
audience sees. The relative sizes of the images indicate how close or
far to the audience you are. So there…sometimes a small photograph next
to a large one means that you travel quite a bit to make the change, to
get from one moment to the next.

Somehow in that is the kind of nut of the dance. Somehow in those
choices, whether you reverse, whether you mirror the dance, whether you
decide to learn it the opposite of what you looking at frontally and
then turn it around, how you decide to handle the spacing forward and
back, how the different steps just by an efficient change work into one
another, it determines the space of the dance. It has a very odd look in
the space, I think. I think all of this creates an odd patterning to the
movement, and an odd sense of who the performer is, cause your mind is
going back to the original performer, and coming back to yourself and
back to another original performer, you know, in the photograph all the
time. So it’s *personation, impersonation,* and somewhere in that is the
central unit of the…of what is interesting to me about the work. The way
people choose and how those… And one person may do one and one another
of these possible choices, so that the dance… I can have dance movements
reverse themselves for what I have seen before of what I thought it was.
So my preconception of the dance is… I’ve got one dance that’s flowing
along as I envisioned it, and in the other one all the wrong arms and
legs are being used. I thought it went the other way around, my reading
of the score. And then I just look and see if it’s worth keeping or I
should make somebody reverse, or what, you know. But in those choices
amazing things happen, a kind of slippage, just at the point where the
choreographer begins to see the point where things can be revealed about
the materials. I think of course as much about the score that I’m making
as I can, but it’s so interesting to see there is always more. Just in
the simple kind of symmetry that there are for each reader to flip.
Always more possibilities coming out of that.

It was an attempt in making the scores, over the years, to just find a
way to think about movement. I mean, I’m proposing on the stage
definitely a dance, but I’m proposing in my mind’s eye as I’m creating
the score all kinds of my own transitions between one thing and another
and it’s like a mental dancing to work with the photos. One gets very
intimate with the photos. I mean, I don’t know how long it takes to…when
I do the cut-out photos, I don’t know how long it takes me to do it, but
it’s like fractals. It gets ever more intricate as you try to follow,
you know, the difference between one shade of grey and another around a
curve of a finger or a piece of a costume. That seems to be about that.
