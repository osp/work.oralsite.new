Title: Gardening, 2001
Date: 06-05-2001
audio: audio/steve-paxton/keywords/gardening.ogg
       audio/steve-paxton/keywords/gardening.m4a
       audio/steve-paxton/keywords/gardening.mp3
Voices: Steve Paxton (SP)
Order: 1

![]({attach}gardening.jpg)
:   Mad Brook Farm. Photo: Tom Engels, 2019.

SP: The word “guard” is in garden. One of the Anasazi sites has an
incredible number of old tower bases, built on the rim of a small canyon
and away from the main village, which was at the source of the water.
There was a spring where they had the main village. This was the place
where two watercourses in the desert met so there would be extra amount
of water there. And they gardened down in the canyon and they had all
these guardhouses, presumably to watch the gardens, that’s the first
thought one has, when they weren’t in them. In the garden.

There is a sense of a space where you are doing something very special.
It might need guarding. We have a fence around ours to keep the dogs
from the parking lot from dashing madly right into the middle of it, as
they always do. Somehow some animal always comes along at seeding time,
just when the plants have just started, and goes crashing into the beds.
I think the satisfaction that I find there is very similar to the
satisfaction I found in dance, for a lot of the same reasons. A special
place to investigate a natural phenomenon and design with it. All the
ways you have to design a garden, like rotation in crops, you know how
it works, what its principles are.

Of all the gardening elements, one of the things I produce a lot of is
compost. And you can’t find out from the books what exactly compost
does. Why they recommend putting it on gardens and lawns and everywhere.
But, I mean, my garden is in the middle of a hay field. So there are no
trees around. Not too far away there is a forest and in that forest is
topsoil, being made by the leaves. So the trees are shedding their
nutrients in their leaves, and they fall down by the roots, and then
they leech, the water leeches the minerals and everything down to the
roots again and it recycles. It’s a recycling system. In the garden,
since you are working for just one kind of crop, you eliminate all the
other more successful plants from the area, putting these whims that
require your completion attention, watering and all that. You help them
compete all summer by removing the natural plants, the weeds, each one
of which is a chemical bactery of some sophistication producing
concentrates of different kinds of minerals and molecules. When you
return all of this material, plus the plants themselves, to the soil in
the form of compost, what you are doing is working very hard to do what
the forest does naturally. To…it’s an artificial topsoil. Not artificial
in any sense except that it’s made through the intervention of a person.
So I thought a lot about that since nobody will come out and say, “What
is this soil conditioner?” It’s not a manure, it’s not a… So I can
imagine that it has a lot of stuff in it that plants want because it
comes from plants. But at the same time, and maybe in a form that is
easy to get, but it’s not recommended that way. It isn’t like lime or
some kind of chemical that you would add, or fertilizer that you would
add. It’s not treated like a fertilizer quite. And I realize that the
earth has to breathe first of all. That when there is a crust over the
earth…like right now we do a lot of raking here, that when there is a
crust of plant, you know, dead plant skeletons as the top layer, that
what is happening underneath is an increase of temperature and humidity.
And it becomes a good place for moles and other lovers of wetness and
darkness to grow. And it forms a barrier to air moving in and out of the
earth. The surface of the earth becomes impermeable and is covered by,
you know a kind of covering of dead plant material. And it’s not at all
like the forest where…because it’s the grasses, it’s a whole different
kind of growth. In the forest the bits are all shaped and sized so that
they make discrete… Well, you do get mats of leaves, I guess you do get
mats of leaves, but they curl, they react, they are not so adamant as
the dead grass that you are…we are raking up right now. It will stay
like that and once it’s protected by the growing grass around it, which
it limits the growth of. It means many fewer shoots come through. Then
it’s kind of stuck there and becomes a mat that over the years inhibits
the ability for grass to grow at all. So in a way, without the
intervention of animals like grazing and pawing and manuring and all
that, the grasses have a much harder time. They interact more than the
forest does with animals in that way. Grasses require herds \[laughing\]
to stay healthy.

The compost, when it goes back to the garden, is in a way a provider of
atmosphere. It enlightens the soil so that air can move up and down
easily with different air pressure changes. The very movement of that
area’s cleansing, it’s literally an exhalation of decayed stuff, the
gasses from decay, and it’s also a way to take in fresh oxygen and get
oxygen down into the earth so that that process can continue. It’s in
those chemical changes that the roots seem to find nutrients. So it
seems as though compost makes roots work easier and makes it more
profitable, without directly doing anything other than kind of being a
little bit of lighter space in the soil.

When I started this it all related to dance, but now it no longer does
and I don’t care. I got interested in just the compost and how it’s
working. Anyway, so there’s the roots. They are out of sight. We don’t
know how they work. There is the worms, down there already, living
there. I am interested in that they have a good life. I’m interested in
that the soil is right for them. And I think composting helps the worms
and the worms leave an incredible amount of manure, just at the right
level as they go under the surface. To be available to the plants. It’s
a system, isn’t it? Describing a system. And how one intervention works,
in a way replacing that top layer of soil, which we want to be so
barren, which is so contrary to the way the earth normally dresses
herself, which is with as many plants possible. You know, let them fight
it out.

I could go back to dance. Shall I do that? It’s up to me, you know. In
saying that I think there is something in dance that is a system as well
and that the way you intervene in it’s a choice that you make. Whether
you expand it or intensify it or just instigate it or try to do it via
instruction, however you intervene to make a choreography happen is a
choice that you make. The scores were my kind of choice, where I did not
have to dictate or influence or even be present during the learning of a
dance that I had instigated. But I had provided something, you know,
something very much like compost, when I think of it, or maybe
fertilizer, or maybe seed too. I provided a lot of material, but… I
didn’t then directly and with my personality and my particular slant on
things, intervene in a process that led to the performance. Period.
