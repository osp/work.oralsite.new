Title: Lecture, 2019
Date: 24-07-2019
Voices: Steve Paxton (SP)
Order: 4

SP: *Beautiful Lecture*. There was a figure, me, in between two images
projected on the wall. I was against the wall too and I was a
three-dimensional object. My action during the piece was using my spine
to worm my way down to squat on my heels. And then worm my way back up
to standing. Up and down, as I saw fit. It was an undulation of the
spine, not a big one. Pretty subtle, up and down, very programmatic.
Meanwhile, to my right one could see *Swan Lake* danced by Ulanova at
the Mariinsky Ballet, or whatever it was: big, white, glorious ballet
and Tchaikovsky. To my left you had a kind of really sleazy pornographic
film, the kind of where a delivery man shows up and the woman is
enticing. And before you know it, they’re naked and going at it. There’s
one beautiful shot: the two bodies just rippling together, a duet. In
the moment of choosing the film, I didn’t think at all of pornography or
sex. I just thought of the movement. But it was dirty sex. It was meant
to be pornographic, which I think is naturally a dirty look at sex or a
look at something we’re forbidden to see. I was just interested in that
shot of the clapping bodies. I don’t know if I’ve ever seen it again. It
is a very interesting movement. Where his penis was at the moment, I’m
not sure, but you know, it wasn’t the issue. So, those two films were
going on. A black and white and a black and white film. Two black and
white films. One more black, one more white, one high art, one the
lowest. And me in between, kind of in the middle. A middle figure.
Scrunching along the wall.

When I wanted to perform *Beautiful Lecture* at the New School in New
York City, I was not allowed to show the pornographic film. I made an
alternative version, called *Untitled Lecture*. At the time there was a
famine in Biafra. So, I called up the people who were running the
information program, went up there and got a film of starving Biafrans,
babies, mothers, and substituted that for the pornography. I told the
audience what I had had to do and what I had chosen to do. They totally
got the idea that sex was natural or humane, and that starvation was
not. They understood that the school had made me substitute something
that was good for something that was bad, or at least the
representations thereof. It all gets a little bit murky when you try to
decide how these images exist in life. Mentioning there was supposed to
be pornography did the job. Famine and high art. It sort of changed the
parameters of the piece a bit, but it was still an incredible scope of
the best from the most classic dance to the most tragic of racialized,
economically corrupt circumstances. I still did the undulating movement.
I didn’t have to change myself. Nobody censored me. I was allowed to put
my back on the wall.

I once more chose to use the lecture as a response to another instance
of censorship. I had planned to do three versions of *Satisfyin Lover*,
my walking dance with 42 people, at New York University: a mixed
version, a redheaded version and a nude one. I found the redheads by an
advertisement in the Village Voice for “redheaded people who could
walk." I had four weeks of people coming up to me asking “Am I redheaded
enough?” And then there was the nude version, just a way to let people
see it as a different form of walking. And that one was forbidden by the
NYU. Instead, I did *Intravenous Lecture* in which I am getting
intravenous fluid needled into my body as a kind of metaphor for
sponsorship in the arts and control of the arts. Just to make a big old
complaint about being censored. I was dancing. I was moving. And I had a
doctor, my friend Bill Summer, who put the needle in. I stood still for
the actual insert, but then it was possible to move around and dance and
speak adlib about how I had wanted to put my dance on stage and hadn’t
been allowed to. And how sponsors shape what the arts become by their
control.

The act of naming it, that it seemed to be in a kind of category in my
mind, like the chemistry lecture, the psychological lecture or the
lecture on punctuation and English. Delivering the attitude of
delivering information. Even though in *Beautiful Lecture*, who’s to
know what is meant by the title and if it is a lecture what has been
delivered. It’s subverting the form. I’m not doing my best to educate an
audience. It’s an event. It’s an experience. I never considered to call
those pieces “lectures” because I performed them in schools, but isn’t
it interesting that *Beautiful Lecture* was presented at the New School
and *Intravenous Lecture* at New York University? There was quite some
support coming from educational institutions in New York City. Not all
of them, but the more liberal institutions were supportive. They were
liberal, but there were still the bounds of good taste. I was willing to
go beyond good taste.

I rely on historians for the sequence of my life, but *Lecture on
Walking* that appears in *Earth’s Interior* in 1966 might be the first
expression of what one could call a lecture. Trisha and I held hands and
I lectured on walking as I walked. Trisha was to not cooperate with what
I was trying to do by improvised dancing. If I set off to cross the
stage, she could drag behind or she could go ahead or she could role on
the floor. But we had to hold hands. She had to be interruptive. To be
an impediment to my delivery. That same reflexive movement like *Word
Words*. Now talking on walking while walking. Art made me do it.
