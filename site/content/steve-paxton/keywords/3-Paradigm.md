Title: Paradigm, 2001
Date: 06-10-2001
Voices: Steve Paxton (SP)
Order: 3

SP: One does make assumptions about students, and I've written an article,
which you probably read, about assumptions I made and the ways I tried
to operate once I had pushed away from the paradigm of modern dance and
got into a more complex movement thought. At Dartington, after the
Schräber work, we started a new project. We decided to work on how
Contact Improvisation would be received by people who had no sight, and
so, we started working with people who were blind and found a way to
train them, which required a lot of adjustment of the teaching paradigm
that I had started with sighted students. My assumption was that if
somebody was blind, they would be aware of their body. And so, I was
quite surprised to discover that they were no more aware of their body
than people with sight. I had assumed that sight was such a compelling
sense that it overrode sensations or gravity or touch; the normal kinds
of things that one is constantly doing in the world, so that therefore
we don't notice it. It’s a habitual input that is disregarded by the
consciousness. Only to find out that people without sight also didn't
notice the habitual input. I'm telling you, it's an axiom. It's a
training axiom.

They claimed that they didn't have a more developed touch skill. The
people without sight that I talked to said that was just nonsense, that
touch was more developed. That it was very difficult to read braille. It
required a lot of training. I don't know how true it is because there's
also the possibility that they didn't notice that it developed, because
it developed slowly enough or without traumatic incident, so their sight
might be taken away one day, the next day, they didn't suddenly…things
didn't, you know…touch didn't lead to great sensitivity. That's a
possibility.

But overall, I found that it was as mysterious to them how their bodies
felt, as in a conversation that we had this morning where Danny
\[Lepkoff\] said it took some time for him to find himself in Contact
Improvisation, to find his center, the right awareness. It took some
time to develop that. I thought this was a problem of people being aware
of the outside of their bodies because of their sight and therefore
without sight, you would be inside your body. But in fact, I find that
the body, as the most continuously inputting to the brain element of our
existence, is by large ignored, and so, we come back in a way to the
process illustrated by the tea ceremony, where awareness is brought to
something ordinary. That's the point of doing the ordinary. It is the
process of developing the awareness of obvious material, which one is
overlooking and is not, therefore, aware of consciously. So, the problem
is how to bring the vast amount of material that exists in life and in
sensing to consciousness in the mind.

And what is surprising when you look back at Contact Improvisation is
that it wasn't recognized as a form. It was recognized as behavior
amongst children, amongst babies, between babies and parents, and that
it gets trained out of us because it becomes inappropriate, because of
the encroaching sexuality, which we have to control so that people don't
get into trouble with their sexuality in later years. It has to be
trained out. So, it's a good example of paradigm. There is a controlling
paradigm over the body, and that paradigm controls what you think of as
appropriate or even possible. You might not think it's possible to do
some things, which then after a little bit of training, you can do. It's
just that you were trained not to think in that direction. It has all to
do with the kind of an aspect which I'm still very curious about in the
theory of paranoia, about the ability of a person to hide their
experience or their thoughts or their desires from themselves.

And in the book *Soul Murder* [by Leonhard Shengold], there was an
especially delicious paragraph, which I couldn't quote to you, but it
exists in that book, about the nature of the mechanism of having a
thought, recognizing the thought as negative, being able to put that
thought away, or conceal it from yourself, forget that you have done
this action, and carry on living with this kind of still water of
incestuous longing as part of you, but not any longer giving the
consciousness access to it as though the fact that it was out of the
consciousness suggests that it doesn't exist anymore to the operator of
the body. But other people might say, well, it's still operative on a
subconscious level, and therefore, you have these problems in life.
Therefore, you react to people in the way you do. Therefore, you don't
get along with men, or you don't get along with women, or you don't get
along with dogs, or you don't get along with society, or you don't get
along with law in the way that you do, you know. It's something that you
have forgotten that you ever knew and forgotten that you've forgotten
it.

I don't believe in polarities, and I don't believe in integrities and
that everything is integral finally. We don't have senses that operate
so totally that we're aware of the final integrity. That we’re always
going to be in a situation of achieving whatever we achieve as
awareness. And the next generation won't even have that because they'll
dismiss it or think of it as history and go on to achieve new
awarenesses. And this must be a sort of operating project or not
paradigm in the race that keeps this able to change, able to adapt
because it does seem to me that we are in a world which in which change
occurs, and therefore, we can't be too fixed in our understanding of it.
We have constantly to have this slippage, and the ability to deny a
paradigm is very important, however strongly it seems true in one’s
societal group.

We say sometimes, truth can only be told in a story that illustrates the
truth even if the event never happened or the people never existed. And
people who try to write only the truth very often aren't able to put it
in a way that anybody's interested in reading it. It doesn't even
penetrate. The idea that there is a truth is very interesting. You know,
there definitely is a fiction. We know that, but what is its opposite
pole? And so, in this way, one could say the paradigm of polarization
doesn't work for me. The idea that we can talk about one thing or the
other. Including my own work. I have to accept my own work this way.
What I know about my own work, I have to accept as a partial answer. I
can just know the part of it that I have become aware of, so
pronouncements are a little suspect, including my own pronouncements.
Including what I just said.
