Title: Practices of Inclusion<br>Vermont, 2001, 2019
Supertitle: Cluster 3
HasPage: True
Template: cluster
Image: images/steve-paxton/cluster-3/cluster.jpg
Imageposition: center top
Order: 3

### Introduction

The third cluster of this publication zooms in on the crux of the
research project that formed the impetus for conducting the interviews
in 2001: improvisation. Improvisation here is approached in a twofold
way; first tracing the concrete origins of that very notion within the
work of Steve Paxton, but secondly as a more general notion,
regarding it almost as a way of living, a life practice, a practice of
inclusion. The jump between the former and the latter is a jump made
between two different interviews, two distinct periods, and two
different interlocutors. The first interview dating back to 2001 and
conducted by Myriam Van Imschoot, the second having taken place in the
summer of 2019 and conducted by Tom Engels.

To go from improvisation as a dance form to improvisation as a way of
life seems, at first sight, a big leap, an exaggeration almost. But
stretching our understanding of this notion, turning it almost into
hyperbole, allowed us to think how art and life were able to resonate,
how they could feed each other, or how they could cross-fertilize,
without having to define the dual temporal order of chicken and egg.

In *1: On Improvisation*, conducted in 2001 in
Vermont, Steve Paxton and Myriam Van Imschoot trace back the very
origins of Steve’s interest in improvisation as a tool for dance and
composition. Against the backdrop of having worked with John Cage and
Merce Cunningham, Paxton recalls his earliest memories of seeing
improvisation in the work of Simone Forti and Trisha Brown made in the
context of Judson Dance Theatre. We’re amid a laboratory, a testing
ground, where the notion of improvisation was not yet conceptualized. It
is a notion in full emergence, which would come to define and radically
shift the practices of contemporary dance ever since. However, in the
early years, improvisation was not free from judgment and often resulted
in a flippant push and pull between discarding and embracing this new
form of practice. What one can witness is the slow emerging of an
understanding of what that thing called improvisation could be, or what
it could be capable of.

*2: Practices of Inclusion* is an attempt to continue the aforementioned
conversation on improvisation between Van Imschoot and Paxton from 2001. Some
things remained unclear. Did the tape cut off? Was there another tape
that went missing? If so, where would it be, who would have it, which
life would it live now? 18 years later, we took the lost tape as an
impetus for a new conversation. This interview picks up the thread and
speculates on improvisation in relation to ecologies of inclusion.
Inclusion and ecology being interpreted in a broad sense and being
present in a variety of life expressions: the practice of Buddhism, yoga
and aikido, performance as a field of permissions and openness, the pigs
playing in the garden of Mad Brook farm or an 8-year-old boy climbing
granite rocks in Arizona. One element keeps on returning, namely that
these expressions always embody the oneness of duality. Or, as
Zen-master Suzuki once said, “The body and mind are not two and not
one.”

<cite>Image left:</cite>   
<cite>- Simone Forti, *See Saw* from *Dance Constructions*, 1960. Performance with plywood seesaw. Duration variable. The Museum of Modern Art, New York. Committee on Media and Performance Art Funds. © 2020 The Museum of Modern Art, New York. Performed by Simone Forti and Steve Paxton at Galleria L'Attico, Rome, 1969. Image: © 2020 Claudio Abate. Courtesy of Simone Forti and The Box, LA.</cite>   
<cite>Images right starting from the top:</cite>     
<cite>- Video still from Grand Union performing at the University of Iowa, March 7, 1974.</cite>     
<cite>- Tom Engels, The Mad Brook running close to the lands of Mad Brook Farm, 2019.</cite>     
