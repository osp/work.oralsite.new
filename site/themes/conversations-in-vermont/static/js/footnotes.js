(function (document) {
  'use strict';

  var footnoteSelector = '.inline-footnote',
      footnotes = document.querySelectorAll(footnoteSelector);



  for (var i = 0; i < footnotes.length; i++) {
    footnotes[i].addEventListener('click', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
    
      
      if (!('expanded' in this.dataset)) {
        var note = this;
        var closeButton = document.createElement('section');
        closeButton.classList.add('lightbox--close');
        closeButton.addEventListener('click', function (e) {
          e.preventDefault();
          e.stopImmediatePropagation();
          delete note.dataset.expanded;
          note.scrollTo(0, 0);
          this.remove();
        });
        
        this.dataset.expanded = 'true';
        this.appendChild(closeButton);
        this.scrollTo(0, 0);
      }
    });
  }
})(document);