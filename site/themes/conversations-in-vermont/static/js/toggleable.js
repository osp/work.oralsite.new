(function (document) {
  'use strict';

  var panelSelector = '.panel--toggleable',
      panels = document.querySelectorAll(panelSelector);



  for (var i = 0; i < panels.length; i++) {
    panels[i].addEventListener('click', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
    
      
      if (!('expanded' in this.dataset)) {
        var note = this;
        var closeButton = document.createElement('section');
        closeButton.classList.add('lightbox--close');
        closeButton.addEventListener('click', function (e) {
          e.preventDefault();
          e.stopImmediatePropagation();
          delete note.dataset.expanded;
          note.scrollTo(0, 0);
          this.remove();
        });
        
        this.dataset.expanded = 'true';
        this.appendChild(closeButton);
        this.scrollTo(0, 0);
      }
    });
  }
})(document);