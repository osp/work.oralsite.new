(function (document) {
  'use strict';

  var Lightbox = function (src, caption, onClose) {
    this.src = src;
    this.captionHTML = caption.innerHTML;
    this.buildHTML();
    this.handleKey = this.handleKey.bind(this);
    document.addEventListener('keydown', this.handleKey);
    this.onClose = onClose;
  }

  Lightbox.prototype.buildHTML = function () {
    this.el = document.createElement('section');
    this.el.classList.add('lightbox');

    var closeButton = document.createElement('section'),
        figure = document.createElement('figure'),
        caption = document.createElement('figcaption'),
        image = document.createElement('img');
        
        
    figure.classList.add('lightbox--figure');
    caption.classList.add('lightbox--caption');
    closeButton.classList.add('lightbox--close');
        
    image.src = this.src;
    caption.innerHTML = this.captionHTML;

    figure.appendChild(image);
    figure.appendChild(caption);

    this.el.appendChild(closeButton);
    this.el.appendChild(figure);

    closeButton.addEventListener('click', this.close.bind(this));
  }

  Lightbox.prototype.handleKey = function (e) {
    if (e.code === 'Escape') {
      e.preventDefault();
      this.close();
    } 
  }

  Lightbox.prototype.close = function () {
    document.removeEventListener('keydown', this.handleKey);
    this.el.parentElement.removeChild(this.el);
    if (this.onClose) {
      this.onClose();
    }
  };

  function openLightbox (image, caption) {
    var lightbox = new Lightbox(image, caption, function () { document.body.classList.remove('has-lightbox'); });
    document.body.appendChild(lightbox.el);
    document.body.classList.add('has-lightbox');
  }

  var imageSelector = 'figure img',
      images = document.querySelectorAll(imageSelector);

  for (var i = 0; i < images.length; i++) {
    images[i].addEventListener('click', function (e) {
      var caption = this.parentElement.querySelector('figcaption');
      e.stopPropagation();
      e.preventDefault();
      openLightbox(this.src, caption);
    });
    images[i].dataset.lightboxLink = 'true';
  }
})(document);