(function () {
  var clickPrevented = false;

  function preventClick () {
    clickPrevented = true;
    window.setTimeout(function () {
      clickPrevented = false;
    }, 500);
  }

  function smoothScroll (distance) {
    var scrollEl = (arguments.length > 1) ? arguments[1] : window; 

    if ('scrollBehavior' in document.documentElement.style) {
      scrollEl.scrollBy({
        top: distance,
        behavior: 'smooth'
      });
    } else {
      var start = new Date().getTime(),
          startY = (scrollEl.hasOwnProperty('scrollY')) ? scrollEl.scrollY : scrollEl.scrollTop,
          duration = 200;
  
      function doScroll () {
        var now = new Date().getTime(),
            t = now - start;
  
        if (t < duration) {
          scrollEl.scrollTo(0, startY + distance * Math.min(1, t / duration));
          window.requestAnimationFrame(doScroll);
        } else {
          scrollEl.scrollTo(0, startY + distance);
        }
      } 
  
      doScroll();
    }
  }

  /**
   * Transform time in seconds to timecode: (hh:?)mm:ss
   * @param {number} t time
   */
  function toTimecode (t) {
    t = Math.round(t)
    var hours = Math.floor(t / 3600),
        minutes = Math.floor((t % 3600) / 60),
        seconds = t % 60,
        timecode = '';

    if (hours  > 0) {
        timecode += hours.toString(10)
        timecode += ':';
    }
    
    timecode += (minutes > 9) ? minutes.toString(10) : '0' + minutes.toString(10)
    timecode += ':' + ((seconds > 9) ? seconds.toString(10) : '0' + seconds.toString(10))

    return timecode;
  }

  function alignBookmarks (trackBookmark, textBookmark, textContainer) {
    var trackPosition = trackBookmark.getBoundingClientRect().top,
        textPosition = textBookmark.getBoundingClientRect().top,
        distance = textPosition - trackPosition;
    smoothScroll(distance, textContainer);
  }


  function initPlayer(audio, player, article) {
    var trackEl = player.querySelector('[data-role="track"]'),
        trackProgressEl = player.querySelector('[data-role="progress"]'),
        currentTimeEl = player.querySelector('[data-role="current-time"]'),
        durationEl = player.querySelector('[data-role="duration"]'),
        playPauseEl = player.querySelector('[data-role="play--pause"]'),
        scrubberEl = player.querySelector('[data-role="scrubber"]'),
        scrubberLabelEl = player.querySelector('[data-role="scrubber--label"]'),
        timeMarkerStartEl = player.querySelector('[data-role="time-marker--start"]'),
        timeMarkerEndEl = player.querySelector('[data-role="time-marker--end"]'),
        expanded = false,
        scrubbing = false,
        trackRect = null,
        scrubbingStart = -1,
        lastScrubbingPosition = -1;

    audio.volume = 1.0; // for now force volume.

    function updateTrackRect () {
      trackRect = trackEl.getBoundingClientRect();
    }

    function play () {
      audio.play();
    }

    function pause () {
      audio.pause();
    }

    function setPlayState () {
      article.dataset.playing = (audio.paused) ? 'false' : 'true';
      player.dataset.playing = (audio.paused) ? 'false' : 'true';
    };
      
    function setSeekingState () {
      article.dataset.seeking = (audio.seeking) ? 'true' : 'false';
      player.dataset.seeking = (audio.seeking) ? 'true' : 'false';
    };

    function setExpandedState () {
      if (expanded) {
        player.dataset.expanded = 'true';
      }
      else {
        delete player.dataset.expanded;
      }
    }

    function setProgress () {
      var timecode = toTimecode(audio.currentTime);
      currentTimeEl.innerText = timecode;
      trackProgressEl.style.height = getPositionForTime(audio.currentTime).toFixed(2) + 'px';
      if (!scrubbing) {
        scrubberLabelEl.innerText = timecode;
      }
    }

    function setDuration() {
      timeMarkerStartEl.innerText = toTimecode(0);
      timeMarkerEndEl.innerText = toTimecode(audio.duration);
      updateAudioBookmarks();
    }

    /**
     * returns a a time for the given y position
     * @param {int} y Position relative to the top of the track element
     */
    function getTimeForPos (y) {
      return Math.max(0, Math.min(1, y / (trackRect.height - 30))) * audio.duration;
    }

    /**
     * returns a height for given t
     * @param {int} t time in seconds
     * returns a 
     */
    function getPositionForTime (t) {
      return Math.max(0, Math.min(1, t / audio.duration)) * (trackRect.height - 30);
    }

    /**
     * Seek in the player
     * @param {int} t 
     */
    function seek (t) {
      audio.currentTime = t;
    }

    /*
      Creates a following HTML structure and attaches event listeners
    
      <section data-role="track--bookmark" class="track--bookmark"`>
        <span class="track--bookmark--timecode"></span>
        <span class="track--bookmark--label"></span>
      </section>
    */
    function makeAudioBookmark (t, timecode, label, marker) {
      var bookmark = document.createElement('section'),
          bookmarkTimecode = document.createElement('span'),
          bookmarkLabel = document.createElement('span');

      bookmarkTimecode.classList.add('track--bookmark--timecode');
      bookmarkTimecode.appendChild(document.createTextNode(timecode));

      bookmarkLabel.classList.add('track--bookmark--label');
      bookmarkLabel.innerHTML = label;

      bookmark.dataset.role = 'track--bookmark';
      bookmark.dataset.t = t;
      bookmark.classList.add('track--bookmark');
      if (marker.classList.contains('inline')) {
        bookmark.classList.add('inline');
      }
      
      bookmark.appendChild(bookmarkTimecode);
      bookmark.appendChild(bookmarkLabel);

      var activateBookmark = function (e) {
        if (! clickPrevented) {
          preventClick();
          e.preventDefault();
          e.stopImmediatePropagation();
          alignBookmarks(bookmark, marker, (window.innerWidth <= 600) ? window : document.querySelector('.panel.article'));
          seek(t);
        }
      }

      bookmark.addEventListener('mousedown', activateBookmark);
      bookmark.addEventListener('touchstart', activateBookmark);
      marker.addEventListener('mousedown', activateBookmark);
      marker.addEventListener('touchstart', activateBookmark);

      return bookmark;
    }

    /***
     * Finds audio bookmarks in the htmls document and places them on the player's timeline.
     * 
     * Audio bookmarks are expected to have this syntax:
     * <span class="audiobookmark" data-time="{t:int}">{label:string}</span>
     */
    function collectAudioBookmarks () {
      var markers = document.querySelectorAll('.audiobookmark');
      
      for (var i = 0; i < markers.length; i++) {
        var marker = markers[i],
          label = marker.querySelector('.bookmark-label').innerHTML,
          timecode = marker.querySelector('.bookmark-timecode').innerHTML,
          t = marker.dataset.time;
        
        var bookmark = makeAudioBookmark(t, timecode, label, marker);
        trackEl.appendChild(bookmark);
      }

      // for (var i=markers.length-1; i > -1; i--) {
      //   markers[i].remove();
      // }
    }

    /**
     * Update position of audiobookmarks on duration changes in the audio file.
     */
    function updateAudioBookmarks () {
      var bookmarks = trackEl.querySelectorAll('.track--bookmark');
      for (var i=0; i < bookmarks.length; i++) {
        // pos = parseFloat(bookmarks[i].dataset.t) / audio.duration;
        bookmarks[i].style.top = getPositionForTime(bookmarks[i].dataset.t) + 'px';
      }
    }

    function resetScrubberTransform () {
      scrubberEl.style.transform = '';
    }

    audio.addEventListener('play', setPlayState);
    audio.addEventListener('pause', setPlayState);
    audio.addEventListener('ended', setPlayState);
    audio.addEventListener('seeking', setSeekingState);
    audio.addEventListener('seeked', setSeekingState);
    audio.addEventListener('seeked', setPlayState);
    audio.addEventListener('seeked', resetScrubberTransform);
    audio.addEventListener('timeupdate', setProgress);
    audio.addEventListener('durationchange', setDuration);

    trackEl.addEventListener('mousedown', function (e) {
      if (!clickPrevented) {
        console.log('Seek event through mousedown on track');
        e.stopImmediatePropagation();
        e.preventDefault();
        seek(getTimeForPos(e.clientY - trackRect.top));
      }
    });

    trackEl.addEventListener('touchstart', function (e) {
      if (window.innerWidth <= 600) {
        preventClick();
        e.stopImmediatePropagation();
        e.preventDefault();

        if (!expanded) {
          expanded = true;
          setExpandedState();
        } else {
          seek(getTimeForPos(e.targetTouches[0].clientY - trackRect.top));
        }
      }
    });

    player.addEventListener('touchstart', function (e) {
      if (window.innerWidth <= 600 && expanded) {
        preventClick();
        e.stopImmediatePropagation();
        e.preventDefault();
        expanded = false;
        setExpandedState();
      }
    });

    // mouse down
    scrubberEl.addEventListener('mousedown', function (e) {
      if (!clickPrevented) {
        console.log('Scrubbing started');
        e.stopImmediatePropagation();
        e.preventDefault();
        scrubbing = true;
        scrubbingStart = e.clientY;
        scrubberEl.dataset.scrubbing = 'true';
        scrubberLabelEl.innerText = toTimecode(getTimeForPos(e.clientY - trackRect.top));
      }
    });

    scrubberEl.addEventListener('touchstart', function (e) {
      e.stopImmediatePropagation();
      e.preventDefault();
      preventClick();
      scrubbing = true;
      scrubbingStart = e.targetTouches[0].clientY;
      scrubberEl.dataset.scrubbing = 'true';
      scrubberLabelEl.innerText = toTimecode(getTimeForPos(e.targetTouches[0].clientY - trackRect.top));
    });

    // mouse move
    document.body.addEventListener('mousemove', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if (scrubbing) {
        var t = getTimeForPos(e.clientY - trackRect.top);
        scrubberLabelEl.innerText = toTimecode(t);
        offset = Math.min(trackRect.bottom - 30, Math.max(trackRect.top, e.clientY)) - scrubbingStart;
        scrubberEl.style.transform = 'translateY(' + offset.toString() + 'px)';
        // scrubberEl.style.transform = 'translateY(' + (getPositionForTime(t) ) + 'px)';
      }

      // scrubberLabelEl.innerText = toTimecode(getTimeForPos(e.clientY - trackRect.top));

    });

    // mouse move
    document.body.addEventListener('touchmove', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if (scrubbing) {
        lastScrubbingPosition = Math.min(trackRect.bottom - 30, Math.max(trackRect.top, e.touches[0].clientY));
        offset = lastScrubbingPosition - scrubbingStart;
        scrubberEl.style.transform = 'translateY(' + offset + 'px)';
        scrubberLabelEl.innerText = toTimecode(getTimeForPos(lastScrubbingPosition - trackRect.top));
      }
    });

    // mouse up
    document.body.addEventListener('mouseup', function (e) {
      e.stopImmediatePropagation();
      if (scrubbing && !clickPrevented) {
        console.log('Seek event through mouse up.');
        e.preventDefault();
        scrubbing = false;
        seek(getTimeForPos(e.clientY - trackRect.top));
        scrubberEl.dataset.scrubbing = 'false';
      }
    });

    // mouse up
    document.body.addEventListener('touchend', function (e) {
      e.stopImmediatePropagation();
      if (scrubbing) {
        e.preventDefault();
        preventClick();
        scrubbing = false;
        seek(getTimeForPos(lastScrubbingPosition - trackRect.top));
        scrubberEl.dataset.scrubbing = 'false';
      }
    })

    // Create a drag thing, style a bit better
    // Introduce markers

    function togglePlaying () {
      if (audio.paused) {
        play();
      }
      else {
        pause();
      }
    }

    playPauseEl.addEventListener('click', function (e) {
      e.stopImmediatePropagation();
      togglePlaying();
    });

    window.addEventListener('resize', updateTrackRect);
    window.addEventListener('resize', setProgress);

    collectAudioBookmarks();
    updateTrackRect();
    setDuration();
    setProgress();
    setPlayState();

    return togglePlaying;
  }


  window.initPlayer = initPlayer;

  
})();